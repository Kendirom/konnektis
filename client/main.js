Template.registerHelper('Schemas', Schemas);

SimpleSchema.debug = true

Meteor.autorun(function () {
  if (!Meteor.userId()) {
    SessionAmplify.set("patientId", null);
  }
});

var logout = Meteor.logout;

Meteor.logout = function(forceLogout) {
  var doLogout = function(doCloseAppointments) {
    if(doCloseAppointments) {
      closeAppointments();
    }

    SessionAmplify.set("loggedIn", false);

    SessionAmplify.set("walkupAppointment", null);
    SessionAmplify.set("walkupCurrentDate", null);
    SessionAmplify.set("currentAppointment", null);

    logout();

    Router.go('walkUpView');
  };

  var closeAppointments = function() {
    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];
      Konnektis.callMethodIfConnected("completeAppointment", a._id, Konnektis.patientId(), function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
           SessionAmplify.set("currentAppointment", null);
        }
      });

      var commentSessionTextKey = Konnektis.getAppointmentCommentTextSessionKey(a._id);

      var savedComment = SessionAmplify.get(commentSessionTextKey);
      if (savedComment) {
        var comment = {
          patientId: Konnektis.patientId(),
          parentType: 'appointment',
          parentId: a._id,
          timestamp: new Date(),
          username: Meteor.user().username,
          userType: Konnektis.isFamily() ? "Family" : "Carer",
          commentText: savedComment,
          urgent: false
        };

        Konnektis.callMethodIfConnected("createComment", comment, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            SessionAmplify.set(commentSessionTextKey, null);
          }
        });
      }
    }
  };

  if(forceLogout == undefined) {
    forceLogout = false;
  }
  if(Konnektis.isCarer()) {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { carerUsername: Meteor.user().username },
        { scheduledStartDay: { $gte: Konnektis.getStartOfDay(new Date()) } },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date()) } },
        { actualEndDate: null },
        { actualStartDate: { $not: null  } },
      ]
    }, {sort: {scheduledStartTime: 1}}).fetch();

    if(appointments.length > 0) {
      if(forceLogout) {
        doLogout(true);
      }
      else {
        bootbox.confirm("Logging out will complete this appointment and save any un-saved comments. Do you want to log out?", function(result) {
          if (result === true) {
            doLogout(true);
          }
          else {
            // Cancel button
          }
        });

        // logout handled in callback, do nothing else here
        return;
      }
    }
    else {
      doLogout(false);
    }
  }
  else {
    doLogout(false);
  }

  bootbox.hideAll();  // just in case there is a message box up about
}

function bindEventTouch(element) {

    element.bind('touchstart', function(event, ui) {
        $(element).click();
        event.preventDefault();

    });
}

Template.layout.rendered = function () {
  jQuery('button').each(function() {
    bindEventTouch(jQuery(this));
  });
};

Template.layout_login.rendered = function () {
  jQuery('button').each(function() {
    bindEventTouch(jQuery(this));
  });
};
