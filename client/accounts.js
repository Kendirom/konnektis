Accounts.ui.config({
  passwordSignupFields: "USERNAME_ONLY"
});

Konnektis.continueOnLogin = function() {
  // Figure out whether to start appointments for the user
  console.log('in Accounts.continueOnLogin');
  var appointments = Appointments.find({
    $and: [
      { carerUsername: Meteor.user().username },
      { scheduledStartDay: { $gte: Konnektis.getStartOfDay(new Date()) } },
      { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date()) } },
      { actualEndDate: null },
      { actualStartDate:  null },
    ]
  },
  {sort: {scheduledStartTime: 1}}).fetch();

  function getDiffFromNow(appointment) {
    // appointment could be concrete or recurring
    var scheduledStart = moment(Konnektis.setTimeFromString(new Date(), appointment.scheduledStartTime || appointment.startTime));
    return Math.abs(moment.duration(scheduledStart.diff(moment())).asHours());
  }

  function getClosestAppointmentToNow(collection) {
    // appointments could be concrete or recurring
    var appointment = null;
    var currentClosestDiff = null;
    if (collection.length === 1) {
      appointment = collection[0];
    } else if (collection.length > 1) {
      appointment = collection[0];
      currentClosestDiff = getDiffFromNow(appointment);

      for (var i = 0; i < collection.length; i++) {
        var a = collection[i];

        var diff = getDiffFromNow(a);

        if (diff < currentClosestDiff) {
          appointment = a;
          currentClosestDiff = diff;
        }
      }
    }
    return appointment;
  }

  var todaysRecurring = Konnektis.getCarerRecurringAppointmentOccurrencesForDate(new Date());
  todaysRecurring = _.sortBy(todaysRecurring, 'scheduledStartTime');

  var appointment = getClosestAppointmentToNow(appointments);
  var recurringAppointment = getClosestAppointmentToNow(todaysRecurring);

console.log('appointment: ' + appointment);
console.log('recurringAppointment: ' + recurringAppointment);

  if (appointment && recurringAppointment) {
    var recurringDiff = getDiffFromNow(recurringAppointment);
    var appointmentDiff = getDiffFromNow(appointment);

    if (recurringDiff < appointmentDiff) {
      console.log('Accounts.continueOnLogin 1');
      Konnektis.convertConcreteAppointmentFromRecurring(recurringAppointment, new Date(), function(result) {
        recurringAppointment._id = result;
        Konnektis.loginStartAppointment(recurringAppointment);
        // Konnektis.loginStartAppointment(result);
      });
    } else {
      console.log('Accounts.continueOnLogin 2');
      Konnektis.loginStartAppointment(appointment);
    }
  } else if (appointment) {
      console.log('Accounts.continueOnLogin 3');
      Konnektis.loginStartAppointment(appointment);
  } else if (recurringAppointment) {
      console.log('Accounts.continueOnLogin 4');
    Konnektis.convertConcreteAppointmentFromRecurring(recurringAppointment, new Date(), function(result) {
      console.log(recurringAppointment);
        recurringAppointment._id = result;
        Konnektis.loginStartAppointment(recurringAppointment);
      // Konnektis.loginStartAppointment(result);
    });
  }
  else {
    console.log('accounts.continueOnLogin falling off bottom');
    // Konnektis.autoAppointment = false;
    Router.go('/');
  }
    console.log('accounts.continueOnLogin at bottom');
};

Accounts.onLogin(function() {
  var patientAutoSelected = false;

  $( document ).idleTimer("reset");
  $( document.documentElement ).idleTimer("reset");

  EncryptionUtils.onSignIn(SessionAmplify.get("password"));

  if (!SessionAmplify.get("loggedIn")) {
    SessionAmplify.set("loggedIn", true);

    // Setup analytics
    if ((Meteor.isClient || Meteor.isCordova) && window.mixpanel) {
      mixpanel.identify(Meteor.userId());
      var username = Meteor.user().username;
      mixpanel.people.set({
        "$first_name": username,
        "$username": username,
        "$last_login": moment.utc(),
      });
    }

    if (Meteor.isCordova && Meteor.settings) {
      //navigator.analytics.sendEvent('userActivity', 'Signed in');
    }

    SessionAmplify.set("currentAppointment", null);
    SessionAmplify.set("patientId", null);
    SessionAmplify.set("role", null);

    SessionAmplify.set("loggingInToAppointmentSummary", null);

    // set the patient for this carer if a walk-up appointment has been selected
    var walkupAppointmentId = SessionAmplify.get("walkupAppointment");
    var walkupPatientId = SessionAmplify.get("walkupPatientId");

    if(walkupAppointmentId && walkupPatientId) {
      if(Roles.userIsInRole(Meteor.user(), ['carer:' + walkupPatientId])) {
        SessionAmplify.set("currentAppointment", walkupAppointmentId);
        SessionAmplify.set("patientId", walkupPatientId);

        Konnektis.recordCarerLogin(walkupPatientId);

        Konnektis.setRole();
      }
    }
  }
});

Konnektis.loginStartAppointment = function (appointment) {
  var patient = Patients.findOne(appointment.patientId);
  if (!patient) {
    throw new Meteor.Error('Could not find a patient with the id on the appointment. Maybe the patient isn\'t accessible to you');
  }

  var label = (appointment.scheduledStartTime ? appointment.scheduledStartTime : '')
    + (appointment.scheduledEndTime ? ' - ' + appointment.scheduledEndTime : '')
    + (appointment.carerUsername ? ' ' + appointment.carerUsername : '')
    + ' - ' + Konnektis.getAppointmentTypeLabel(appointment.name)
    + ' for ' + patient.name;

  bootbox.confirm("Do you want to start " + label + "?", function(result) {
    // Konnektis.autoAppointment = false;
    if (result === true) {
      Konnektis.callMethodIfConnected("startAppointment", appointment._id, appointment.patientId , function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          SessionAmplify.set("currentAppointment", appointment._id);
          // SessionAmplify.set("patientId", appointment.patientId);
          // SessionAmplify.set("role", "carer");
          // Router.go('/');
          Router.go('/details/' + appointment._id + '?date=' + Konnektis.formatDateForLink(new Date()));
        }
      });
    }
    else {
      // Cancel button
      Router.go('/');
    }
  });
}
