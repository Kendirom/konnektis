'use strict';

Template.registerHelper('equals', function (a, b) {
  return (a === b);
});

Template.registerHelper('gt', function (a, b) {
  return (a > b);
});

Template.registerHelper('lt', function (a, b) {
  return (a < b);
});

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};
