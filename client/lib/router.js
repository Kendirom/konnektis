Notify = {
  subscribe: {
    onError: function(error) {
      bootbox.alert('An error occurred while trying to get data from the server: "' + error.message + '". Please check your internet connection and try again.');
    }
  }
};

if (Meteor.isClient) {
  EncryptionUtils.configure({
    enforceEmailVerification: false
  });
}

// Router.waitOn(function() {
//   var patientId = Konnektis.patientId();
//   var collections = ["patients", "carers"];
//   var patientCollections = ["tasks", "recurringTasks", "appointments", "recurringAppointments", "comments"];
//   var subscriptionArr = [];

//   if(patientId) {
//     collections = collections.concat(patientCollections);
//   }

//   var num_collections = collections.length;

//   for (var coll_index = 0; coll_index < num_collections; coll_index++) {
//     var args = [collections[coll_index], patientId];
//     subscriptionArr.push(Meteor.subscribe.apply(null, args));
//   }

//   return subscriptionArr;
// });

Router.onBeforeAction(function() {
  var self = this;

  var doNext = Tracker.nonreactive(function() {
  var userId = Meteor.userId();
  var routeName = Router.current().route.getName();

  // Object.keys(Session.keys).forEach(function(key) {
  //   Session.set(key, undefined);
  // });
  if(routeName == 'resetPassword'){
    self.next();
    return true;
  }

  // self.next();

console.log('in Router.onBeforeAction');
console.log('route name: ' + routeName);
  // make sure to scroll to the top of the page on a new route
  $('body,html').scrollTop(0);

  if (!userId) {
    SessionAmplify.set("patientId", null);
    SessionAmplify.set("role", null);
  }

  var patientId = Konnektis.patientId();

  var walkup_appointment;
  var walkup_appointment_started;

  if(SessionAmplify.get("walkupAppointment")) {
    walkup_appointment = Appointments.findOne(SessionAmplify.get("walkupAppointment"));
    walkup_appointment_started = walkup_appointment && walkup_appointment.actualStartDate;
  }

  Konnektis.clearBodyClass();
  Konnektis.setBodyClass();

  // Session.keys = {};
      // SessionAmplify.set("walkupAppointment", self._id);
      // SessionAmplify.set("walkupCurrentDate", currentDate);

  console.log('session walkupAppointment: ' + SessionAmplify.get("walkupAppointment"));
  console.log('session walkupCurrentDate: ' + SessionAmplify.get("walkupCurrentDate"));
  console.log('session currentAppointment: ' + SessionAmplify.get("currentAppointment"));

  

  if (!userId) {
    if(SessionAmplify.get("walkupAppointment")) {
console.log('Router.onBeforeAction case a');
      self.layout('layout_login');
      self.render('login');
    }
    else {
      var deviceId = localStorage.getItem('KonnektisDeviceId');
      if(deviceId) {
        SessionAmplify.set("walkupAppointment", null);
        SessionAmplify.set("walkingCurrentDate", null);
        SessionAmplify.set("currentAppointment", null);

        if(routeName == 'walkUpView') {
console.log('Router.onBeforeAction case b');
          self.next();
          return true;
        }
        else {
          // go to walk-up view
          if(routeName == 'walkUpView') {
console.log('Router.onBeforeAction case c1');
            self.next();
            return true;
          }
          else {
console.log('Router.onBeforeAction case c2');
            // self.redirect('walkUpView');
            Router.go('walkUpView');
            return;
          }
        }
      }
      else {
console.log('Router.onBeforeAction case d');
        self.layout('layout_login');
        self.render('login');
      }
    }
  } else {
    // in case we've browsed straight to a page whilst still being logged in, then start idle timers
    Konnektis.startTimers();

console.log('Router.onBeforeAction patientId:' + SessionAmplify.get("patientId"));

    if(routeName == 'selectPatientView') {
console.log('Router.onBeforeAction case e');
      self.next();
      return true;
    }
    else if (!patientId) {
console.log('Router.onBeforeAction case h');
      // Konnektis.doSubscriptions(function() {self.render('selectPatientView');});
      console.log('Router.onBeforeAction going to selectPatient');
      // self.redirect('selectPatientView');
      Router.go('selectPatientView');
      // self.go('/selectPatient');
      // self.next();
      // return;
      // self.render('selectPatientView');
    }
    else if(routeName == 'walkUpView') {
      var walkupAppointment = SessionAmplify.get("walkupAppointment");
      if(walkupAppointment) {
console.log('Router.onBeforeAction case f');
        var params = {};
        var currentDate = SessionAmplify.get("walkupCurrentDate");

        if(currentDate) {
          params['_currentDate'] = Konnektis.formatDateForLink(currentDate);
        }

        SessionAmplify.set("walkupAppointment", null);
        SessionAmplify.set("walkingCurrentDate", null);

        SessionAmplify.set("currentAppointment", walkupAppointment);
        Router.go('dashboardView', params);
        return true;
      }
      else {
console.log('Router.onBeforeAction case g');
        self.next();
        return true;
      }
    }
    else if(!SessionAmplify.get("role")) {
console.log('Router.onBeforeAction case i');
      console.log('Router.onBeforeAction setting role');
      Konnektis.setRole();
      self.next();
      return true;
    }
    else if(Konnektis.isCarer() && walkup_appointment && !walkup_appointment_started && routeName != 'autoAppointmentView') {
console.log('Router.onBeforeAction case k');
      // auto-start appointment
      var params = {};
      var currentDate = SessionAmplify.get("walkupCurrentDate");

      if(currentDate) {
        params['_currentDate'] = Konnektis.formatDateForLink(currentDate);
      }

      SessionAmplify.set("walkupAppointment", null);
      Router.go('autoAppointmentView', params);
      return true;
    }
    else {
console.log('Router.onBeforeAction case j');
      console.log('Router.onBeforeAction going to next');
      self.next();
      return true;
    }
  }
  // self.next();
console.log('finished Router.onBeforeAction');

});

// if(doNext) {
//   this.next();
// }

});

Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

Router.map(function() {
  this.route('home', {
    path: '/',
    template: 'dayView',
    waitOn: function() {
      var subsArr = [];
      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();
        var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('principals'));

        if(patientId) {
          subsArr.push(Meteor.subscribe('recurringMedications', patientId));
          subsArr.push(Meteor.subscribe('recurringTasks', patientId));
          subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
          subsArr.push(Meteor.subscribe('comments', patientId));
          subsArr.push(this.subscribe('medications', patientId, date));
          subsArr.push(this.subscribe('tasks', patientId, date));
          subsArr.push(this.subscribe('appointments', patientId, date));
          subsArr.push(this.subscribe('medicationTakenOptions'));
          subsArr.push(this.subscribe('medicationLocationOptions'));
        }
      }
      return subsArr;
    },
    onBeforeAction: function (pause) {
      if(!Konnektis.isFamily()) {
        this.redirect('/walkup');
      }
      else {
        this.next();
      }
      // var currentAppointment = SessionAmplify.get("currentAppointment");
      // if (currentAppointment) {
      //   // render the login template but keep the url in the browser the same
      //   this.redirect('/details/' + currentAppointment + '?date=' + Konnektis.formatDateForLink(new Date()));
      // } else {
      //   this.next();
      // }
    },
    data: function() {
      return {
        currentDate: moment.utc(new Date()).startOf('day').toDate()
      }
    },
  });

  this.route('dashboardView', {
    path: '/dashboard/:_currentDate?',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();
        var fromDate = moment.utc(date).startOf('day').subtract(1, 'months').toDate();
        var patientId = Konnektis.patientId();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(this.subscribe('appointments', patientId, null, null, fromDate, date));
        subsArr.push(this.subscribe('recurringAppointments', patientId));
        subsArr.push(this.subscribe('carerPatientLastLogins', patientId));
        subsArr.push(this.subscribe('carePlans', patientId));
        subsArr.push(this.subscribe('riskAssessments', patientId));
        subsArr.push(this.subscribe('patientInfo', patientId));
        subsArr.push(this.subscribe('carerPatientLastLogins', patientId));
      }

      return subsArr;
    },
    data: function() {
      return {
        currentDate: this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate(),
        hideDateNav: true
      }
    },
    onBeforeAction: function (pause) {
      if(!Konnektis.isCarer()) {
        Router.go('/day');
      }
      else {
        this.next();
      }
    }
  });

  this.route('carePlanView', {
    path: '/careplan',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        if(patientId) {
          subsArr.push(Meteor.subscribe('patients'));
          subsArr.push(Meteor.subscribe('carePlanSections'));
          subsArr.push(this.subscribe('carerPatientLastLogins', patientId));
          subsArr.push(this.subscribe('carePlans', patientId));
        }
      }

      return subsArr;
    },
    onAfterAction: function() {
      var firstCarePlanSection = CarePlanSections.findOne({}, {sort: {displayOrder: 1}});

      if(firstCarePlanSection) {
        SessionAmplify.set('carePlanTab', firstCarePlanSection.cssClass);
      }
      else {
        SessionAmplify.set('carePlanTab', null);
      }
    },
  });

  this.route('riskAssessmentView', {
    path: '/riskAssessment',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        if(patientId) {
          subsArr.push(Meteor.subscribe('patients'));
          subsArr.push(this.subscribe('carerPatientLastLogins', patientId));
          subsArr.push(this.subscribe('riskAssessments', patientId));
        }
      }

      return subsArr;
    },
  });

  this.route('patientInfoView', {
    path: '/patientInfo',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        if(patientId) {
          subsArr.push(Meteor.subscribe('patients'));
          subsArr.push(this.subscribe('patientInfo', patientId));
          subsArr.push(this.subscribe('carerPatientLastLogins', patientId));
          subsArr.push(this.subscribe('patientInfo', patientId));
        }
      }

      return subsArr;
    },
  });

  this.route('adminCarePlanView', {
    path: '/adminCarePlan',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(this.subscribe('carePlans', patientId));

        subsArr.push(this.subscribe('principals'));
      }

      return subsArr;
    },
    onStop: function() {
      tinymce.remove();
    },
  });

  this.route('adminRiskAssessmentView', {
    path: '/adminRiskAssessment',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(this.subscribe('riskAssessments', patientId));
      }

      return subsArr;
    },
    onStop: function() {
      tinymce.remove();
    },
  });

  this.route('adminClientInfoView', {
    path: '/adminClientInfo',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(this.subscribe('patientInfo', patientId));
      }

      return subsArr;
    },
    onStop: function() {
      tinymce.remove();
    },
  });

  this.route('walkUpView', {
    path: '/walkup/:_currentDate?',
    waitOn: function() {
      var subsArr = [];
      var deviceId = localStorage.getItem('KonnektisDeviceId');
      var patientId = Konnektis.patientId();

      // subsArr.push(Meteor.subscribe('principals'));

      if ((Meteor.userId() && patientId) || deviceId) {
        var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();

        subsArr.push(Meteor.subscribe('patients'));

        if(Meteor.userId() && patientId) {
          subsArr.push(this.subscribe('appointments', patientId, date));
          subsArr.push(this.subscribe('recurringAppointments', patientId));
        }
        else {
          subsArr.push(this.subscribe('appointmentSummaries', deviceId, date));
          subsArr.push(this.subscribe('recurringAppointmentSummaries', deviceId));
        }
      }
      return subsArr;
    },
    data: function() {
      console.log('Doing WalkUpView.data');
      return {
        currentDate: this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate(),
        dateNavLocation: 'walkUpView',
         bodyMainDivClasses: 'walkup'
      };
    },
  });

  this.route('autoAppointmentView', {
    path: '/autoAppointment/:_currentDate?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));

        if(patientId) {
          var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();

          subsArr.push(this.subscribe('recurringAppointments', patientId));
          subsArr.push(this.subscribe('appointments', patientId, date));
        }
      }

      return subsArr;
    },
    data: function() {
      return {
        currentDate: this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate()
      }
    },
  });

  this.route('dayView', {
    path: '/day/:_currentDate?',
    waitOn: function() {
      var subsArr = [];
      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();
        var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));

        if(patientId) {
          subsArr.push(Meteor.subscribe('recurringMedications', patientId));
          subsArr.push(Meteor.subscribe('recurringTasks', patientId));
          subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
          subsArr.push(Meteor.subscribe('comments', patientId));
          subsArr.push(this.subscribe('medications', patientId, date));
          subsArr.push(this.subscribe('tasks', patientId, date));
          subsArr.push(this.subscribe('appointments', patientId, date));
          subsArr.push(this.subscribe('medicationTakenOptions'));
          subsArr.push(this.subscribe('medicationLocationOptions'));
        }
      }
      return subsArr;
    },
    data: function() {
      return {
        currentDate: this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate()
      }
    },
  });

  this.route('selectPatientView', {
    path: '/selectPatient/',
    waitOn: function() {
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
      }

      return subsArr;
    },
    onBeforeAction: function (pause) {
      console.log('selectPatientView.onBeforeAction');
      var patients = Patients.find().fetch();

      // default to showing an auto-appointment, so that the dashboard behind is always hidden
      // Konnektis.autoAppointment = true;

      if (patients.length == 1) {
        var patientId = patients[0]._id;
        console.log('selectPatientView.onBeforeAction just one patient');
        // patientAutoSelected = true;
        SessionAmplify.set("patientId", patientId);
        // SessionAmplify.set("role", null);

       Konnektis.recordCarerLogin(patientId);
       // Konnektis.setRole();

        // Router.go('/autoAppointment');
        if(Konnektis.isFamily()) {
          Router.go('/day');
        }
        else {
          Router.go('/walkup');
        }


        // this.next();
        // Router.go('/');
      }
      else {
        this.next();
      }
    //   this.next();

    // // default to showing an auto-appointment, so that the dashboard behind is always hidden
    // Konnektis.autoAppointment = true;

    // var patients = Patients.find().fetch();
    // console.log('accounts.onlogin, patients.length: ' + patients.length);
    // if (patients.length == 1) {
    //   patientAutoSelected = true;
    //   SessionAmplify.set("patientId", patients[0]._id);
    //   SessionAmplify.set("role", null);
    //   Router.go('/autoAppointment');
    // }

    },
  });

  this.route('calendarView', {
    path: '/calendar/:_currentDate?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringTasks', patientId));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(Meteor.subscribe('comments', patientId));
        subsArr.push(this.subscribe('tasks', patientId));
        subsArr.push(this.subscribe('appointments', patientId));
      }

      return subsArr;
    },
    data: function() {
      return {
        currentDate: this.params._currentDate ? moment.utc(this.params._currentDate).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate()
      }
    },
  });

  this.route('commentsView', {
    path: '/comments/:_id?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = this.params.query.date ? moment.utc(this.params.query.date).startOf('day').toDate() : moment.utc(new Date()).startOf('day').toDate();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringMedications', patientId));
        subsArr.push(Meteor.subscribe('recurringTasks', patientId));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(Meteor.subscribe('comments', patientId));
        subsArr.push(this.subscribe('medications', patientId, date));
        subsArr.push(this.subscribe('tasks', patientId, date));
        subsArr.push(this.subscribe('appointments', patientId, date));
      }

      return subsArr;
    },
    data: function() {
      return {
        id: this.params._id,
        date: this.params.query.date,
        appointmentId: this.params.query.appointmentId
      }
    }
  });

  this.route('taskView', {
    path: '/task/:_id?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = moment.utc(this.params.query.date).toDate();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringTasks', patientId));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(this.subscribe('tasks', patientId, date));
        subsArr.push(this.subscribe('appointments', patientId, date));
      }

      return subsArr;
    },
    data: function() {
      return {
        id: this.params._id,
        date: this.params.query.date,
        appointmentId: this.params.query.appointmentId
      };
    }
  });

  this.route('medicationView', {
    path: '/medication/:_id?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = moment.utc(this.params.query.date).toDate();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(Meteor.subscribe('recurringMedications', patientId));
        subsArr.push(Meteor.subscribe('recurringTasks', patientId));
        subsArr.push(this.subscribe('appointments', patientId, date));
        subsArr.push(this.subscribe('medications', patientId, date));
        subsArr.push(this.subscribe('tasks', patientId, date));
      }

      return subsArr;
    },
    data: function() {
      return {
        id: this.params._id,
        date: this.params.query.date,
        appointmentId: this.params.query.appointmentId
      };
    }
  });

  this.route('appointmentView', {
    path: '/appointment/:_id?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = moment.utc(this.params.query.date).toDate();
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(this.subscribe('appointments', patientId, date));
      }

      return subsArr;
    },
    data: function() {
      return {
        id: this.params._id,
        date: this.params.query.date
      };
    }
  });

  this.route('detailsView', {
    path: '/details/:_id?',
    waitOn: function() {
      console.log('doing detailsView.waitOn');
      var subsArr = [];

      if(Meteor.userId()) {
        var patientId = Konnektis.patientId();
        var params = this.params;
        var date = moment.utc(params.query.date).toDate();
        var appointmentId = params._id;

        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(this.subscribe('recurringMedications', patientId));
        subsArr.push(this.subscribe('recurringTasks', patientId));
        subsArr.push(this.subscribe('recurringAppointments', patientId));
        subsArr.push(this.subscribe('comments', patientId));
        subsArr.push(this.subscribe('medications', patientId, date, appointmentId));
        subsArr.push(this.subscribe('medicationTakenOptions'));
        subsArr.push(this.subscribe('medicationLocationOptions'));
        subsArr.push(this.subscribe('tasks', patientId, date, appointmentId));
        subsArr.push(this.subscribe('appointments', patientId, date, appointmentId));
      }

      console.log('done detailsView.waitOn');
      return subsArr;
    },
    data: function() {
      console.log('doing detailsView.data');

      var id = this.params._id;
      var date = this.params.query.date;

      // var recurringAppointment = RecurringAppointments.findOne({_id: id});

      // if (recurringAppointment) {
      //   console.log('detailsView.data found recurring appointment: ' + id);
      //   if (!date) {
      //     throw new Meteor.Error('Date is required');
      //   }

      //   var startOfDay = moment.utc(date).startOf('day').toDate();
      //   var endOfDay = moment.utc(date).endOf('day').toDate();

      //   var concrete = Appointments.findOne({
      //     $and: [
      //       { patientId: Konnektis.patientId() },
      //       { scheduledStartDay: { $gte: startOfDay } },
      //       { scheduledEndDay: { $lte: endOfDay } },
      //       { recurringAppointmentId: recurringAppointment._id }
      //     ]
      //   });

      //   if (concrete) {
      //     id = concrete._id;
      //     console.log('detailsView.data found concrete appointment: ' + id);
      //   }
      // }

      console.log('done detailsView.data');
      return {
        id: id,
        date: date
      }
    },
  });

  this.route('medicationDetailsView', {
    path: '/medicationDetails/:_id?',
    waitOn: function() {
      var patientId = Konnektis.patientId();
      var date = moment.utc(this.params.query.date).toDate();
      var appointmentId = this.params._id;
      var subsArr = [];

      if(Meteor.userId()) {
        subsArr.push(Meteor.subscribe('patients'));
        subsArr.push(Meteor.subscribe('carers'));
        subsArr.push(Meteor.subscribe('recurringMedications', patientId));
        subsArr.push(Meteor.subscribe('recurringTasks', patientId));
        subsArr.push(Meteor.subscribe('recurringAppointments', patientId));
        subsArr.push(Meteor.subscribe('comments', patientId));
        subsArr.push(this.subscribe('medications', patientId, date, appointmentId));
        subsArr.push(this.subscribe('medicationTakenOptions'));
        subsArr.push(this.subscribe('medicationLocationOptions'));
        subsArr.push(this.subscribe('tasks', patientId, date, appointmentId));
        subsArr.push(this.subscribe('appointments', patientId, date, appointmentId));
      }

      return subsArr;
    },
    data: function() {
      console.log('doing medicationDetailsView.data');
      var recurringAppointment = RecurringAppointments.findOne({_id: this.params._id});

      var id = this.params._id;
      var date = this.params.query.date;

      if (recurringAppointment) {
        if (!date) {
          throw new Meteor.Error('Date is required');
        }

        var startOfDay = moment.utc(date).startOf('day').toDate();
        var endOfDay = moment.utc(date).endOf('day').toDate();

        var concrete = Appointments.findOne({
          $and: [
            { patientId: Konnektis.patientId() },
            { scheduledStartDay: { $gte: startOfDay } },
            { scheduledEndDay: { $lte: endOfDay } },
            { recurringAppointmentId: recurringAppointment._id }
          ]
        });

        if (concrete) {
          id = concrete._id;
        }
      }

      console.log('done medicationDetailsView.data');
      return {
        id: id,
        date: date
      }
    }
  });

  this.route('resetPassword', {
    path: '/reset-password/:token',
    action: function() {
      this.render('resetPassword');
    },
    data: function() {
      return {
        token: this.params.token,
      };
    }
  });
});
