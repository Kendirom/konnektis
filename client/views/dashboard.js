'use strict';

Template.dashboardView.created = function () {
  // console.log('currentAppointment:' + SessionAmplify.get("currentAppointment"));
  //     var recurringAppointment = RecurringAppointments.findOne(SessionAmplify.get("currentAppointment"));
  //     console.log('recurring appointment');
  //     console.log(recurringAppointment);

  this.clientInfoChanged = true;
  this.riskAssessmentChanged = true;
  this.carePlanChanged = true;

  var lastLogin = Konnektis.getCarerPatientLastLogin();

  if(lastLogin) {
    var carerId = Meteor.userId();
    var patientId = Konnektis.patientId();

    this.clientInfoChanged = Konnektis.hasPatientInfoChanged(PatientInfo, carerId, patientId, lastLogin, Konnektis.patientDocuments.patientInfo);
    this.riskAssessmentChanged = Konnektis.hasPatientInfoChanged(RiskAssessments, carerId, patientId, lastLogin, Konnektis.patientDocuments.riskAssessment);
    this.carePlanChanged = Konnektis.hasPatientInfoChanged(CarePlans, carerId, patientId, lastLogin, Konnektis.patientDocuments.carePlan);
  }
  else {
    // no last login for some reason, all client documents must be new
  }

  this.anythingChanged = this.clientInfoChanged || this.riskAssessmentChanged || this.carePlanChanged;
};

Template.dashboardView.helpers({
  isCarer: function() {
    return Konnektis.isCarer() && SessionAmplify.get("role") === 'carer';
  },
  currentDate: function() {
    return this.currentDate;
  },
  anythingChanged: function() {
    return Template.instance().anythingChanged;
  },
  clientInfoChanged: function() {
    return Template.instance().clientInfoChanged;
  },
  riskAssessmentChanged: function() {
    return Template.instance().riskAssessmentChanged;
  },
  carePlanChanged: function() {
    return Template.instance().carePlanChanged;
  },
  clientName: function() {
    return Konnektis.clientDisplayName();
  },
  appointmentTimes: function() {
    return Konnektis.appointmentTimes(SessionAmplify.get("currentAppointment"), this.currentDate, false, true);
  },
});

Template.dashboardCarePlan.events({
 'click li': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged && !template.data.changesHere) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }
    else {
      Router.go('/careplan');
    }
  },
});

Template.dashboardRiskAssessment.events({
 'click li': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged && !template.data.changesHere) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }
    else {
      Router.go('/riskAssessment');
    }
  },
});

Template.dashboardClientInfo.events({
 'click li': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged && !template.data.changesHere) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }
    else {
      Router.go('/patientInfo');
    }
  },
});

Template.dashboardItem.events({
 'click li.medication': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }

    bootbox.alert('Medication tasks are now found under the My Tasks button');
    return false;

    // var currentAppointment = SessionAmplify.get("currentAppointment");
    // if (currentAppointment) {
    //   Router.go('/medicationDetails/' + currentAppointment + '?date=' + Konnektis.formatDateForLink(Template.parentData().currentDate || new Date()));
    // }
    // else {
    //   Router.go('/walkup');
    // }
  },
 'click li.previous-carer-notes': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }

    var currentAppointment = SessionAmplify.get("currentAppointment");

    var appointment = Appointments.findOne(currentAppointment) || RecurringAppointments.findOne(currentAppointment);

    if (appointment) {
      var appointmentStart = null;

      if(appointment.scheduledStartDay) {
        // this is an appointment (concrete or not from a recurring appointment)
        appointmentStart = Konnektis.addStringTimeToMoment(appointment.scheduledStartTime, moment.utc(appointment.scheduledStartDay));
      }
      else {
        // this is a recurring appointment
        appointmentStart = Konnektis.addStringTimeToMoment(appointment.startTime, moment(Template.parentData().currentDate));
      }

      var prevAppointment = Appointments.findOne({
        $and: [
          {_id: {$ne: currentAppointment}},
          {actualEndDate: {$lt: appointmentStart.toDate()}}
          ]
      },
        {sort: {actualEndDate: -1}});

      if(prevAppointment) {
        Router.go('/details/' + prevAppointment._id + '?date=' + Konnektis.formatDateForLink(prevAppointment.scheduledStartDay));
      }
      else {
        bootbox.alert("Unable to find a previous appointment - is this the first ever appointment for this client?");
      }
    }
    else {
      Router.go('/walkup');
    }
  },
 'click li.my-tasks': function(event, template) {
    event.preventDefault();

    if(template.data.anythingChanged) {
      Konnektis.alertCarerReviewChanges();
      return false;
    }

    var currentAppointment = SessionAmplify.get("currentAppointment");
    if (currentAppointment) {
      Router.go('/details/' + currentAppointment + '?date=' + Konnektis.formatDateForLink(Template.parentData().currentDate || new Date()));
    }
    else {
      Router.go('/walkup');
    }
  },
});
