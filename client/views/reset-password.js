Template.resetPassword.onCreated(function(){
  this.password = new ReactiveVar('');
  this.passwordAgain = new ReactiveVar('');
});


Template.resetPassword.events({
  'input .new-password-input': function(e,tmpl){
    let password = $('.new-password-input').val();
    Template.instance().password.set(password);
  },
  'input .new-password-input-again': function(e,tmpl){
    let password = $('.new-password-input-again').val();
    Template.instance().passwordAgain.set(password);
  },
  'submit #passwordResetForm':function(e,tmpl){
  	e.preventDefault();
  	var newPass = $('.new-password-input').val();
  	var newPassAgain = $('.new-password-input-again').val();

  	if(newPass != newPassAgain){
  		console.error('Passwords are not the same')
  		return
  	}

  	if(!Konnektis.isPassValid(newPass)){
  		console.error('Rules doesnt match')
  		return
  	}

  	Accounts.resetPassword(tmpl.data.token, newPass, function(err){
  		if(err){
  			console.log(err);
  		} else {
  			Router.go('/')
  		}
  	})
  },
  'click .cancel':function(){
  	Router.go('/');
  }
});

Template.resetPassword.helpers({
  checkPassword: function(){
    let password = Template.instance().password.get();
    let notValidColor = 'red';
    let validColor = 'green';

    let result = {
      length: notValidColor,
      upperCase: notValidColor,
      lowerCase: notValidColor,
      numbers: notValidColor,
      special: notValidColor
    }
    if(password.length >= 8 ){                                                    //Checking password length
      result.length = validColor;
    }

    if(/[A-Z]/.test(password)){                                                 //Checking for upperCase
      result.upperCase = validColor;
    }

    if(/[a-z]/.test(password)){                                                  //Checking for lowerCase
      result.lowerCase = validColor;
    }

    if(/[0-9]/.test(password)){                                                     //Checking for number
      result.numbers = validColor;
    }

    if(/[@#\$%\^&\*\(\)_\+\|~\-=\\`{}\[\]:";'<>\/]/.test(password)){              //Checking for special chars
      result.special = validColor;
    }

    return result;
  },
  isAllValid: function(){
    var pass = Template.instance().password.get();
    var passAgain = Template.instance().passwordAgain.get();
    return Konnektis.isPassValid(pass) && (pass == passAgain);
  }
});