'use strict';

function areSameDay(a, b) {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false
  }

  return a.getFullYear() === b.getFullYear()
    && a.getMonth() === b.getMonth()
    && a.getDate() === b.getDate();
}

Template.calendarView.helpers({
  calendarOptions: {
      // Standard fullcalendar options
      height: 700,
      firstDay: 1,
      fixedWeekCount: false,
      hiddenDays: [  ],
      slotDuration: '01:00:00',
      minTime: '00:00:00',
      maxTime: '23:59:59',
      lang: 'en',
      header:{
        left: 'prev,next ',
        center: 'title',
        right: 'month,agendaWeek'
      },
      dayClick:function( date, allDay, jsEvent, view) {
        // Insert the day someone's clicked on
        Router.go('/day/' + Konnektis.formatDateForLink(Konnektis.getDateFromUtc(date)));
      },
      eventClick:function(calEvent, jsEvent, view){
        Router.go('/details/' + calEvent.id + '?date=' + Konnektis.formatDateForLink(calEvent.start.toDate()));
      },
      // Function providing events reactive computation for fullcalendar plugin
      events: function(start, end, timezone, callback) {
        //console.log(start);
        //console.log(end);
        //console.log(timezone);
        var events = [];
        // Get only events from one document of the Calendars collection
        // events is a field of the Calendars collection document

        var appointments = Appointments.find({
          $and: [
            { patientId: Konnektis.patientId() },
            { scheduledStartDay: { $gte: start.toDate() } },
            { scheduledEndDay: { $lte: end.toDate() } }
          ]
        }).fetch();

        for (var i = 0; i < appointments.length; i++) {
          var a = appointments[i];
          var startDateTime = Konnektis.getDateFromUtc(a.scheduledStartDay);
          var endDateTime = Konnektis.getDateFromUtc(a.scheduledEndDay);

          if (a.scheduledStartTime && a.scheduledEndTime) {
            var startTime = Konnektis.getTime(a.scheduledStartTime);
            startDateTime.setHours(startTime.hours);
            startDateTime.setMinutes(startTime.minutes);
            var endTime = Konnektis.getTime(a.scheduledEndTime);
            endDateTime.setHours(endTime.hours);
            endDateTime.setMinutes(endTime.minutes);
          } else {
            endDateTime.setHours(startDateTime.getHours()+1);
          }

          events.push({ id: a._id, title: a.name.toProperCase(), start: startDateTime, end: endDateTime});
        }

        var from = Konnektis.getDateFromUtc(start.toDate());
        var to = Konnektis.getDateFromUtc(end.toDate());
        for (var d = from; d <= to; d.setDate(d.getDate() + 1)) {
          var all = Konnektis.getRecurringAppointmentOccurrencesForDate(d);
          for (var i = 0; i < all.length; i++) {
            var occurrence = all[i]
            events.push({
              id: occurrence._id,
              title: occurrence.name.toProperCase(),
              start: Konnektis.setTimeFromString(d, occurrence.startTime),
              end: Konnektis.setTimeFromString(d, occurrence.endTime)
            });
          }
        }

        callback(events);
      },
      // Optional: id of the calendar
      id: "calendar1",
      // Optional: Additional classes to apply to the calendar
      addedClasses: "col-md-12",
      // Optional: Additional functions to apply after each reactive events computation
      autoruns: [
        function () {
          console.log("user defined autorun function executed!");
        }
      ],
      viewRender: function (view) {

        // Only add "drowdowns" if the current view is the weekly view
        if (view.name === 'agendaWeek') {

            // Add the "dropdowns" to the day headers
            //$('.fc-widget-header.fc-sun, .fc-widget-header.fc-mon, .fc-widget-header.fc-tue, .fc-widget-header.fc-wed, .fc-widget-header.fc-thu, .fc-widget-header.fc-fri, .fc-widget-header.fc-sat')
            //.append("<a class='btn new fright' href='/appointment' style='display: inline'><span class='icon-plus'></span> </a>");
        }
      },
      dayRender: function (date, cell) {


        if (this.calendar.getView().rowCnt === 1) {
          var queryPar = Konnektis.formatDateForLink(date);
          cell.append("<a class='btn new new_appointment' href='/appointment/?date=" + queryPar + "' style='z-index: 9999'><span class='icon-plus'></span> New</a>");
        }
        else {
          if(cell.hasClass('fc-past')) {
            var careStatus  = Konnektis.getCareStatus(date._d);
            if(careStatus != "neutral")
            {
              cell.append("<span class='icon-care-" + careStatus + "'></span>");
            }
            return true;
          }
        }
    }
  },
  patientName: function() {
    return Konnektis.patientName();
  },
  dateString: function() {
    return Konnektis.formatDate(this.currentDate);
  },
  previousDateString: function() {
    var previousDay = Konnektis.getDateFromUtc(this.currentDate);
    previousDay.setDate(this.currentDate.getDate() - 1);
    return Konnektis.formatDateForLink(previousDay);
  },
  nextDateString: function() {
    var nextDay = Konnektis.getDateFromUtc(this.currentDate);
    nextDay.setDate(this.currentDate.getDate() + 1);
    Konnektis.formatDateForLink(nextDay);
  },
  dateQueryParameters: function() {
    var currentDate = Template.parentData().currentDate;
    return {
      date: Konnektis.formatDateForLink(currentDate)
    };
  }
});


Template.calendarView.events({
  'click .new_appointment': function(e) {

  }

});
