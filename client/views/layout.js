'use strict';

Template.layout.onCreated(function(){
  this.changePassState = new ReactiveVar(false);
  this.password = new ReactiveVar('');
  this.passwordAgain = new ReactiveVar('');
  this.errorMsg = new ReactiveVar('');
  this.successMsg = new ReactiveVar('');
})

Template.layout.events({
  'click #login-dropdown-list a.dropdown-toggle': function(e) {
    // console.log('dropdown clicked');
    SessionAmplify.set("password", $('#login-password').val());
  },
  'click .btn-home': function(e) {
    Router.go('dashboardView');
  },
  'click .back-btn': function(e) {
    if (this.date) {
      Router.go('/day/' + this.date);
    } else {
      history.go(-1);
    }
  },
  'click .role_switch': function(e) {
    if (SessionAmplify.get("role") === 'family')
    {
      SessionAmplify.set("role", "carer");
    }
    else if (SessionAmplify.get("role") === 'carer'){
      SessionAmplify.set("role", "family");
    }
  },
  'click .assignPatientDevice': function(e) {
    e.stopPropagation();
    Konnektis.closeMenu();
    var patientId = Konnektis.patientId();
    if (patientId) {
      bootbox.confirm({
        className: 'assignPatientBox',
        message: 'Do you want to assign ' + Konnektis.patientName() + ' to this tablet? Doing so will automatically display their appointments on this tablet even when no one is logged in.',
        callback: function(result) {
          if (result) {
            var patientDeviceId = localStorage.getItem('KonnektisDeviceId');
            Konnektis.callMethodIfConnected("assignPatientDevice", patientId, patientDeviceId, function(error, result) {
              if (error) {
                localStorage.removeItem('KonnektisDeviceId');
                Errors.throw(error.message);
              } else {
                localStorage.setItem('KonnektisDeviceId', result);
              }
            });
          }
        }
      });
    }
    else {
      bootbox.alert('Sign in and select a service user beforing assigning them to this tablet.');
    }
  },
  'click .unassignPatientDevice': function(e) {
    e.stopPropagation();
    Konnektis.closeMenu();

    bootbox.confirm({
      className: 'assignPatientBox',
      message: 'Do you want to clear any service user who is already assigned to this tablet?',
      callback: function(result) {
        if (result) {
          var patientDeviceId = localStorage.getItem('KonnektisDeviceId');
          if(patientDeviceId) {
            Konnektis.callMethodIfConnected("clearPatientDevice", patientDeviceId, function(error, result) {
              if (error) {
                localStorage.removeItem('KonnektisDeviceId');
                Errors.throw(error.message);
              } else {
                localStorage.removeItem('KonnektisDeviceId');
              }
            });
          }
        }
      }
    });
  },
  'click .download-appointment': function(e) {
    e.stopPropagation();
    Konnektis.closeMenu();

    var appointmentId = SessionAmplify.get("currentAppointment");

    if(appointmentId) {
      Meteor.call('generate_appointment_pdf', appointmentId, function(err, res) {
        if (err) {
          console.error(err);
        } else if (res) {
          window.open("data:application/pdf;base64, " + res, "Appointment PDF");
        }
      });
    }
    else {
      bootbox.alert('Ensure you are viewing an appointment before downloading it.');
    }
  },
  'click .logout': function(e, tmpl){
    Meteor.logout();
  },
  'click .change-password': function(e,tmpl) {
    e.preventDefault();
    e.stopPropagation();
    tmpl.changePassState.set(true);
  },

  'click .cancel-pass-change': function(e,tmpl){
    e.preventDefault();
    e.stopPropagation();
    tmpl.changePassState.set(false);
  },
  'input .new-password-input': function(){
    let password = $('.new-password-input').val();
    Template.instance().password.set(password);
  },
  'input .new-password-again-input': function(){
    let password = $('.new-password-again-input').val();
    Template.instance().passwordAgain.set(password);
  },
  'submit #passwordChangeForm': function(e, tmpl){
    e.preventDefault();
    e.stopPropagation();
    var oldPass = $('.old-password-input').val();
    var newPass = $('.new-password-input').val();
    if(!Konnektis.isPassValid(newPass)){
      return
    }

    Accounts.changePassword(oldPass, newPass, function(err){
      if(err){
        tmpl.errorMsg.set(err.reason);
      } else {
        tmpl.password.set('');
        tmpl.passwordAgain.set('')
        tmpl.errorMsg.set('');
        tmpl.successMsg.set('Successfully changed');
        tmpl.changePassState.set(false);
      }
    })
  },
  'click .confirm-success': function(e, tmpl){
    e.preventDefault();
    e.stopPropagation();
    tmpl.successMsg.set('');
  }
});

Template.layout.helpers({
  appVersion: function() {
    return 'v. ' + Konnektis.appVersion();
  },
  loggedIn: function() {
    return !!Meteor.userId();
  },
  onlineStatus: function() {
    return Meteor.status().connected ? 'online' : 'offline';
  },
  connected: function() {
    return Meteor.status().connected;
  },
  viewSwitch: function() {
    return Konnektis.isFamily() && Konnektis.isCarer();
  },
  currentAppointment: function() {
    return SessionAmplify.get("currentAppointment");
  },
  havePatientName: function() {
    var patientName = Konnektis.patientName();
    return (patientName ? true : false);
  },
  patientName: function() {
    return Konnektis.patientName();
  },
  isFamily: function() {
    return Konnektis.isFamily() && SessionAmplify.get("role") === 'family';
  },
  isCarer: function() {
    return Konnektis.isCarer() && SessionAmplify.get("role") === 'carer';
  },
  patientIsSelected: function() {
    return Konnektis.isCarer() || Konnektis.isFamily();
  },
  home: function() {
    return Iron.Location.get().path === '/';
  },
  dateString: function() {
    if (this.currentDate) {
      return Konnektis.formatNiceDate(this.currentDate);
    }
    else {
      return null;
    }
  },
  previousDateString: function() {
    var previousDay = new Date(this.currentDate);
    previousDay.setDate(this.currentDate.getDate() - 1);
    return Konnektis.formatDateForLink(previousDay);
  },
  nextDateString: function() {
    var nextDay = new Date(this.currentDate);
    nextDay.setDate(this.currentDate.getDate() + 1);
    return Konnektis.formatDateForLink(nextDay);
  },
  currentDateString: function() {
    return Konnektis.formatDateForLink(this.currentDate);
  },
  showDateNav: function() {
    return this.currentDate && !this.hideDateNav;
  },
  dateNavDest: function() {
    return this.dateNavLocation || 'dayView';
  },
  bodyMainDivClasses: function() {
    return this.bodyMainDivClasses;
  },
  devicePaired: function() {
    return Konnektis.devicePaired();
  },
  user: function(){
    return Meteor.user();
  },
  changePassState: function(){
    return Template.instance().changePassState.get();
  },
  checkPassword: function(){
    Template.instance().changePassState.get();
    let password = Template.instance().password.get();
    let notValidColor = 'red';
    let validColor = 'green';

    let result = {
      length: notValidColor,
      upperCase: notValidColor,
      lowerCase: notValidColor,
      numbers: notValidColor,
      special: notValidColor
    }
    if(password.length >= 8 ){                                                    //Checking password length
      result.length = validColor;
    }

    if(/[A-Z]/.test(password)){                                                 //Checking for upperCase
      result.upperCase = validColor;
    }

    if(/[a-z]/.test(password)){                                                  //Checking for lowerCase
      result.lowerCase = validColor;
    }

    if(/[0-9]/.test(password)){                                                     //Checking for number
      result.numbers = validColor;
    }

    if(/[@#\$%\^&\*\(\)_\+\|~\-=\\`{}\[\]:";'<>\/]/.test(password)){              //Checking for special chars
      result.special = validColor;
    }

    return result;
  },
  isAllValid: function(){
    var pass = Template.instance().password.get();
    var passAgain = Template.instance().passwordAgain.get()
    return Konnektis.isPassValid(pass) && (pass == passAgain);
  },
  isError: function(){
    return Template.instance().errorMsg.get().trim().length>0
  },
  errorMsg: function(){
    return Template.instance().errorMsg.get()
  },
  isSuccess: function(){
    return Template.instance().successMsg.get().trim().length>0
  },
  successMsg: function(){
    return Template.instance().successMsg.get();
  },

});
