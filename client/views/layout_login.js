'use strict';

Template.layout_login.onCreated(function(){
    Konnektis.pauseTimers();
});

Template.layout_login.helpers({
  connected: function() {
    return Meteor.status().connected;
  }
});
