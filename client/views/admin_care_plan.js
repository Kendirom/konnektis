'use strict';

Template.adminCarePlanView.onRendered(function() {
  tinymce.init({
    // selector: '#txt_care_plan',
    selector: 'textarea',
    skin_url: '/packages/teamon_tinymce/skins/lightgray',
  });
});

var findTextEditor = function(id) {
  for (var edId in tinymce.editors) {
    var editor = tinymce.editors[edId];
    if(editor.id == id) {
      return editor;
    }
  }
  return null;
};

var getTextBlocks = function() {
  var documentEditor = findTextEditor('txt_care_plan');
  if(!documentEditor) {
    bootbox.alert('Unable to find document text, cannot save text, please contact support.');
    return false;
  }

  var changesEditor = findTextEditor('txt_changes');
  if(!changesEditor) {
    bootbox.alert('Unable to find changes text, cannot save text, please contact support.');
    return false;
  }

  return([documentEditor.getContent(), changesEditor.getContent()]);
}

var getCarePlanTextBlocks = function() {
  var carePlanEditor = findTextEditor('txt_care_plan');
  if(!carePlanEditor) {
    bootbox.alert('Unable to find care plan text, cannot save text, please contact support.');
    return false;
  }

  var medicationEditor = findTextEditor('txt_medication');
  if(!medicationEditor) {
    bootbox.alert('Unable to find medication text, cannot save text, please contact support.');
    return false;
  }

  var supportPlanEditor = findTextEditor('txt_support_plan');
  if(!supportPlanEditor) {
    bootbox.alert('Unable to find support plan text, cannot save text, please contact support.');
    return false;
  }

  var changesEditor = findTextEditor('txt_changes');
  if(!changesEditor) {
    bootbox.alert('Unable to find changes text, cannot save text, please contact support.');
    return false;
  }

  return([carePlanEditor.getContent(), medicationEditor.getContent(), supportPlanEditor.getContent(), changesEditor.getContent()]);
}

var adminCarePlanSaveWholeCarePlan = function(carePlanId) {
  var textBlocks = getCarePlanTextBlocks();
  if(!textBlocks) {
    return false;
  }

  // Konnektis.callMethodIfConnected('setWholeCarePlan', Konnektis.patientId(), textBlocks, function(error, result) {
  //   if (error) {
  //     Errors.throw(error.message);
  //   } else {
  //     bootbox.alert('Care plan has been saved');
  //   }
  // });
  var patientId = Konnektis.patientId();

    var criteria = {
      _id: carePlanId
    };

    var patientIdField = {
      patientId: patientId
    };

    var newFields = {
      updatedUsername: Meteor.user().username,
      updatedTime: moment().toDate()
    };

    var carePlanText = textBlocks;

    newFields['carePlan'] = carePlanText[0];
    newFields['medication'] = carePlanText[1];
    newFields['supportPlan'] = carePlanText[2];

    newFields['carePlanChanges'] = carePlanText[3];

    var insertFields = _.extend({}, patientIdField);
    _.extend(insertFields, newFields);

    var updateFields = _.extend({}, patientIdField);
    _.extend(updateFields, newFields);

    // var logResult = CarePlansLog.insert(insertFields);

    var result = false;

    if(carePlanId) {
      result = CarePlans.update(
        criteria,
        { $set: updateFields });
    }
    else {
      result = CarePlans.insert(updateFields);
    }

    if(result) {
      bootbox.alert('Care plan has been saved.');
    }
    else {
      bootbox.alert('There was a problem saving the care plan - changes have not been saved.');
    }
};

var adminCarePlanSaveCPSectionText = function(saveMethod, carePlanSection, saveMessage) {
  var textBlocks = getTextBlocks();
  if(!textBlocks) {
    return false;
  }

  Konnektis.callMethodIfConnected(saveMethod, Konnektis.patientId(), carePlanSection, textBlocks[0], textBlocks[1], function(error, result) {
    if (error) {
      Errors.throw(error.message);
    } else {
      bootbox.alert(saveMessage);
    }
  });
};

var adminCarePlanSaveText = function(saveMethod, saveMessage) {
  var textBlocks = getTextBlocks();
  if(!textBlocks) {
    return false;
  }

  Konnektis.callMethodIfConnected(saveMethod, Konnektis.patientId(), textBlock[0], textBlock[1], function(error, result) {
    if (error) {
      Errors.throw(error.message);
    } else {
      bootbox.alert(saveMessage);
    }
  });
};

Template.adminCarePlanButton.events({
  'click #save_client_info': function(e, t) {
    adminCarePlanSaveText('setClientInfo', 'Client info has been saved');
  },
  'click #save_care_plan': function(e, t) {
    adminCarePlanSaveWholeCarePlan(this.parent.carePlanObj);
    // adminCarePlanSaveCPSectionText('setCarePlan', 'carePlan', 'Care plan has been saved');
  },
  'click #save_medication': function(e, t) {
    adminCarePlanSaveCPSectionText('setCarePlan', 'medication', 'Medication care plan has been saved');
  },
  'click #save_support_plan': function(e, t) {
    adminCarePlanSaveCPSectionText('setCarePlan', 'supportPlan', 'Support plan has been saved');
  },
  'click #save_risk_assessment': function(e, t) {
    adminCarePlanSaveText('setRiskAssessment', 'Risk assessment has been saved');
  },
});

Template.adminCarePlanView.helpers({
  carePlan: function() {
    var patientId = Konnektis.patientId();
    var carePlan = CarePlans.findOne({patientId: patientId});
    return carePlan;
  }
});

Template.adminCarePlanTextBoxes.helpers({
    carePlanObj: function () {
        return CarePlans.findOne({
            _id: this._id
        });
    },
});

Template.adminCarePlanTextBoxes.events({
  'click #save_care_plan': function(e, t) {
    adminCarePlanSaveWholeCarePlan(this._id);
    // adminCarePlanSaveCPSectionText('setCarePlan', 'carePlan', 'Care plan has been saved');
  },
});
