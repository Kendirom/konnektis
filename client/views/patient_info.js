'use strict';

Template.patientInfoView.created = function () {
  this.patientInfoChanged = true;

  var lastLogin = Konnektis.getCarerPatientLastLogin();

  if(lastLogin) {
    var carerId = Meteor.userId();
    var patientId = Konnektis.patientId();

    this.patientInfoChanged = Konnektis.hasPatientInfoChanged(PatientInfo, carerId, patientId, lastLogin, Konnektis.patientDocuments.patientInfo);
  }
  else {
    // no last login for some reason
  }
};

Template.patientInfoView.rendered = function () {
  $('#patientInfoChanges').modal({
    backdrop: 'static',
    keyboard: false,
    show: true
  });
  $('#patientInfoChanges').off('hidden.bs.modal');
};

Template.patientInfoView.helpers({
  patientInfo: function() {
    var patientInfo = PatientInfo.findOne();
    return (patientInfo ? patientInfo.patientInfo : null);
  },
  patientInfoChanged: function() {
    return Template.instance().patientInfoChanged;
  },
  patientInfoChanges: function() {
    var patientId = Konnektis.patientId();
    var patientInfo = PatientInfo.findOne({patientId: patientId});
    return patientInfo.changeText;
  },
});

Template.patientInfoChangesView.events({
  'click .btn-cancel': function(event, template) {
    event.preventDefault();

    $('#patientInfoChanges').on('hidden.bs.modal', function (e) {
      Router.go('dashboardView');
    })
  },
  'click .btn-save': function(event, template) {
    event.preventDefault();

    var patientId = Konnektis.patientId();
    Konnektis.callMethodIfConnected('setCarerPatientInfoConfirm', patientId, Konnektis.patientDocuments.patientInfo, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        // all's fine, nothing to do, login has been recorded
        $('#patientInfoChanges').modal('hide');
      }
    });
  },
});
