'use strict';

Template.selectPatientView.helpers({
  patients: function() {
    var patients = Patients.find().fetch();

    var options = patients.map(function(patient) {
      return { label: patient.name, value: patient._id };
    });

    return options;
  }
});

Template.selectPatientView.continuePatientSelected = function() {
    // Router.go('/');
};

Template.selectPatientView.events({
  'change #patient-select': function(e) {
    e.preventDefault();

    if (!e.target.value) {
      return;
    }

    var patient = Patients.findOne(e.target.value);

    if (!patient) {
      Errors.throw("We were unable to find the patient you selected. Please contact an administrator.");
      return;
    }

    SessionAmplify.set("patientId", patient._id);
    SessionAmplify.set("role", null);

    Konnektis.recordCarerLogin(patient._id);

console.log('selectPatientView about to do autoAppointment');
    Konnektis.setRole();

    // Router.go('/autoAppointment');
    Router.go('/walkup');
console.log('selectPatientView done autoAppointment');
    // Router.go('/autoAppointment');
    // Konnektis.doSubscriptions(Template.selectPatientView.continuePatientSelected);
  }
});
