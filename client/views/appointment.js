'use strict';

var repeat = function(id) {
  var repeat = Session.get('repeat');

  if (repeat !== undefined) {
    return repeat;
  }

  appointment = RecurringAppointments.findOne(id);
  if (appointment) {
    return true;
  }

  var appointment = Appointments.findOne(id);
  if (appointment) {

    if (appointment.recurringAppointmentId) {
      return true;
    } else {
      return false;
    }
  }

  return false;
};

var isConcrete = function(id) {
  var appointment = Appointments.findOne(id);

  if (appointment) {
    return true;
  } else {
    return false;
  }
}

function createAppointment(form, date) {
  var appointment = {
    patientId: Konnektis.patientId(),
    recurringAppointmentId: null,
    name: form.name.value,
    description: form.description.value,
    scheduledStartDay: moment.utc(form.scheduledStartDay.value).toDate(),
    scheduledStartTime: form.scheduledStartTime.value,
    scheduledEndDay: moment.utc(form.scheduledEndDay.value).toDate(),
    scheduledEndTime: form.scheduledEndTime.value,
    carerUsername: form.carerUsername.value,
  };

  Konnektis.callMethodIfConnected("createAppointment", appointment, function(error, result) {
    if (error) {
      Errors.throw(error.message);
    } else {
      Router.go('/details/' + result + '?date=' + Konnektis.formatDateForLink(Konnektis.getDateFromUtc(date)));
      // Display success
    }
  });
}

Template.recurringAppointmentForm.helpers({
  recurringAppointment: function() {
    var appointment = Appointments.findOne(this.id);

    var recurringAppointment;
    if (appointment) {
      recurringAppointment = RecurringAppointments.findOne(appointment.recurringAppointmentId);
    }

    if (!recurringAppointment) {
      recurringAppointment = RecurringAppointments.findOne(this.id);
    }

    if (recurringAppointment) {
      // Because the date picker deals in UTC
      recurringAppointment.recurrenceStartDay = Konnektis.getDateFromUtc(recurringAppointment.recurrenceStartDay);
      recurringAppointment.recurrenceEndDay = Konnektis.getDateFromUtc(recurringAppointment.recurrenceEndDay);
    }

    return recurringAppointment || {
      recurrenceStartDay: Konnektis.getDateFromUtc(this.date),
      recurrenceEndDay: Konnektis.getDateFromUtc(this.date),
      startTime: appointment ? appointment.scheduledStartTime : null,
      endTime: appointment ? appointment.scheduledEndTime : null
    };
  }
});

Template.repeatOption.helpers({
  repeat: function() {
    return repeat(this.id);
  }
});

Template.repeatOption.events({
  'change .repeat': function(event) {
    Session.set('repeat', event.target.checked);
  }
});

Template.existingAppointmentView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
  isConcrete: function() {
    return isConcrete(this.id);
  }
});

Template.newAppointmentView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
});

Template.appointmentForm.events({
  'blur #start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Appointments.findOne(id) && !Appointments.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-field');
      var endField = $('#end-time-field');
      if (!endField) {
        console.log('Could not find the end time field, unable to auto set it\'s value from the start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'blur #start-day-field': function(e) {
    var startField = $('#start-day-field');
    var endField = $('#end-day-field');
    if (!endField) {
      console.log('Could not find the end day field, unable to auto set it\'s value from the start day field');
      return;
    }

    endField.val(startField.val());
  }
});

Template.recurringAppointmentForm.events({
  'blur #recurring-start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Appointments.findOne(id) && !Appointments.findOne(id).recurringAppointmentId)) {
      var startField = $('#recurring-start-time-field');
      var endField = $('#recurring-end-time-field');
      if (!endField) {
        console.log('Could not find the recurring end time field, unable to auto set it\'s value from the recurring start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'blur #recurring-start-day-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Appointments.findOne(id) && !Appointments.findOne(id).recurringAppointmentId)) {
      var startField = $('#recurring-start-day-field');
      var endField = $('#recurring-end-day-field');
      if (!endField) {
        console.log('Could not find the recurring end day field, unable to auto set it\'s value from the recurring start day field');
        return;
      }

      endField.val(startField.val());
    }
  }
});

Template.appointmentForm.helpers({
  displayConcreteDates: function() {
    return !repeat(this.id) || isConcrete(this.id)
  },
  displayConcreteTimes: function() {
    return !repeat(this.id);
  },
  appointment: function() {
    var appointment = Appointments.findOne(this.id);
    if (appointment) {
      // Because the date picker deals in UTC
      appointment.scheduledStartDay = Konnektis.getDateFromUtc(appointment.scheduledStartDay);
      appointment.scheduledEndDay = Konnektis.getDateFromUtc(appointment.scheduledEndDay);
      return appointment;
    }

    appointment = RecurringAppointments.findOne(this.id);
    if (appointment) {
      appointment.scheduledStartDay = Konnektis.getDateFromUtc(this.date);
      appointment.scheduledEndDay = Konnektis.getDateFromUtc(this.date);
      appointment.scheduledStartTime = appointment.startTime;
      appointment.scheduledEndTime = appointment.endTime;
      return appointment;
    }

    appointment = {
      scheduledStartDay: Konnektis.getDateFromUtc(this.date),
      scheduledEndDay: Konnektis.getDateFromUtc(this.date)
    };

    return appointment;
  },
  users: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } }
      ]
    }, {sort: {scheduledStartTime: -1}}).fetch();


    var patients = Patients.find().fetch();

    var options = patients.map(function(patient) {
      return { label: patient.name, value: patient._id };
    });

    return options;

  },
  carers: function() {
    var carers = Carers.find({ patients: Konnektis.patientId() }).fetch();

    var options = carers.map(function(value) {
       return { value: value.username, label: value.username };
     });

    options.push( { value: "external", label: "Not a System User" } )

     //return carers.map(function(value) {
       //return { value: value.username, label: value.username };
     //});

    return options;
  }
});

Template.appointmentView.helpers({
  id: function() {
    return this.id;
  }
});

Template.existingAppointmentView.events({
  // Update an appointment without the repeat box checked
  'submit #updateAppointmentForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);
    var appointment = Appointments.findOne(Router.current().data().id);

    if (appointment) {
      // if appointment exists already it's either already non recurring or is concrete AND we are canceling recurrence
      // if already non recurring save without hassle
      // if was recurring confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end date of recurrence to the last occurrence and set the recurring app id of the app to null

      if (appointment.recurringAppointmentId) {
        bootbox.confirm("Warning: Removing recurrence from the appointment will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            var recurringAppointment = RecurringAppointments.findOne(appointment.recurringAppointmentId);

            if (!recurringAppointment) {
              // throw
            }

            Konnektis.callMethodIfConnected("endRecurringAppointmentPreviousDay", recurringAppointment, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                appointment.recurringAppointmentId = null;
                appointment.name = form.name.value;
                appointment.description = form.description.value;
                appointment.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
                appointment.scheduledStartTime = form.scheduledStartTime.value;
                appointment.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
                appointment.scheduledEndTime = form.scheduledEndTime.value;
                appointment.carerUsername = form.carerUsername.value;

                Konnektis.callMethodIfConnected("updateAppointment", appointment, function(error, result) {
                  if (error) {
                    Errors.throw(error.message);
                  } else {
                    Konnektis.removeAllConcreteAppointmentOccurrencesFromDate(
                      recurringAppointment._id,
                      dateFromRouteData,
                      function(a) {
                        return a._id !== appointment._id;
                      });

                    Router.go('/details/' + appointment._id + '?date=' + Konnektis.formatDateForLink(dateFromRouteData));
                  }
                });
              }
            });
          }
        });
      } else {
        appointment.recurringAppointmentId = null;
        appointment.name = form.name.value;
        appointment.description = form.description.value;
        appointment.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
        appointment.scheduledStartTime = form.scheduledStartTime.value;
        appointment.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
        appointment.scheduledEndTime = form.scheduledEndTime.value;
        appointment.carerUsername = form.carerUsername.value;

        Konnektis.callMethodIfConnected("updateAppointment", appointment, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            Router.go('/details/' + appointment._id + '?date=' + Konnektis.formatDateForLink(dateFromRouteData));
          }
        });
      }
    } else {
      // if appointment doesn't exist it was recurring and needs to be concreting; also we are canceling recurrence
      // confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end of recurrence to the last occurrence and create a concrete app without recurring app id

      var recurringAppointment = RecurringAppointments.findOne(Router.current().data().id);

      if (recurringAppointment) {
        bootbox.confirm("Warning: Removing recurrence from the appointment will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            Konnektis.callMethodIfConnected("endRecurringAppointmentPreviousDay", recurringAppointment, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                Konnektis.removeAllConcreteAppointmentOccurrencesFromDate(
                  recurringAppointment._id,
                  dateFromRouteData);

                createAppointment(form, dateFromRouteData);
              }
            });
          }
        });
      } else {
        // throw
      }
    }

    return false;
  },
  'submit #updateRecurrentAppointmentForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var recurringAppointment = RecurringAppointments.findOne(Router.current().data().id);

    if (!recurringAppointment) {
      // throw
    }

    // is not concrete so just assume recurring already

    recurringAppointment.name = form.name.value;
    recurringAppointment.description = form.description.value;
    recurringAppointment.carerUsername = form.carerUsername.value;
    recurringAppointment.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
    recurringAppointment.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
    recurringAppointment.startTime = form.startTime.value;
    recurringAppointment.endTime = form.endTime.value;
    recurringAppointment.recurrenceAmount = form.recurrenceAmount.value;
    recurringAppointment.recurrenceUnit = form.recurrenceUnit.value;

    Konnektis.callMethodIfConnected("updateRecurringAppointment", recurringAppointment, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        Konnektis.updateConcreteAppointmentsWithRecurringData(recurringAppointment);

        Router.go('/details/' + recurringAppointment._id + '?date=' + Konnektis.formatDateForLink(dateFromRouteData));
      }
    });

    return false;

  },
  'submit #updateAppointmentWithRecurrenceForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var appointment = Appointments.findOne(Router.current().data().id);

    var recurringAppointment;
    if (appointment) {
      var recurringAppointment = RecurringAppointments.findOne(appointment.recurringAppointmentId);
    } else {
      var recurringAppointment = RecurringAppointments.findOne(Router.current().data().id);
    }

    if (recurringAppointment) {
      // if recurring exists already it's already recurring so we should save
      // we also need to update the concrete

      recurringAppointment.name = form.name.value;
      recurringAppointment.description = form.description.value;
      recurringAppointment.carerUsername = form.carerUsername.value;
      recurringAppointment.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
      recurringAppointment.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
      recurringAppointment.startTime = form.startTime.value;
      recurringAppointment.endTime = form.endTime.value;
      recurringAppointment.recurrenceAmount = form.recurrenceAmount.value;
      recurringAppointment.recurrenceUnit = form.recurrenceUnit.value;

      Konnektis.callMethodIfConnected("updateRecurringAppointment", recurringAppointment, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var appointment = Appointments.findOne(Router.current().data().id);

          if (!appointment) {
            // throw
          }

          appointment.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          appointment.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();

          Konnektis.callMethodIfConnected("updateAppointment", appointment, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              Konnektis.updateConcreteAppointmentsWithRecurringData(recurringAppointment);

              Router.go('/details/' + appointment._id + '?date=' + Konnektis.formatDateForLink(dateFromRouteData));
            }
          });
        }
      });

    } else {
      // if recurring doesn't exist it was concrete and needs to be recurring
      // create recurring and set recurring app id on concrete; and update other fields

      var recurringAppointment = {
        patientId: Konnektis.patientId(),
        name: form.name.value,
        description: form.description.value,
        carerUsername: form.carerUsername.value,
        recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
        recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
        startTime: form.startTime.value,
        endTime: form.endTime.value,
        recurrenceAmount: form.recurrenceAmount.value,
        recurrenceUnit: form.recurrenceUnit.value
      };

      Konnektis.callMethodIfConnected("createRecurringAppointment", recurringAppointment, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var appointment = Appointments.findOne(Router.current().data().id);

          if (!appointment) {
            // throw
          }

          appointment.recurringAppointmentId = result;
          appointment.name = form.name.value;
          appointment.description = form.description.value;
          appointment.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          appointment.scheduledStartTime = form.startTime.value;
          appointment.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
          appointment.scheduledEndTime = form.endTime.value;
          appointment.carerUsername = form.carerUsername.value;

          Konnektis.callMethodIfConnected("updateAppointment", appointment, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              Router.go('/details/' + appointment._id + '?date=' + Konnektis.formatDateForLink(dateFromRouteData));
            }
          });
        }
      });
    }

    return false;
  }
});

Template.appointmentView.events({
  'submit #recurrentAppointmentCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var recurringAppointment = {
      patientId: Konnektis.patientId(),
      name: form.name.value,
      description: form.description.value,
      carerUsername: form.carerUsername.value,
      recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
      recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
      startTime: form.startTime.value,
      endTime: form.endTime.value,
      recurrenceUnit: form.recurrenceUnit.value,
      recurrenceAmount: form.recurrenceAmount.value
    };

    var date = Template.parentData().date;
    Konnektis.callMethodIfConnected("createRecurringAppointment", recurringAppointment, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        Router.go('/details/' + result + '?date=' + Konnektis.formatDateForLink(recurringAppointment.recurrenceStartDay));
      }
    });

    return false;
  },
  'submit #appointmentCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    createAppointment(form, Template.parentData().date);

    return false;
  }
});
