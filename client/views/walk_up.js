'use strict';

Template.walkUpView.onRendered(function() {
  Konnektis.addBodyClass('walkup');
});

Template.walkUpView.helpers({
  devicePaired: function() {
    return Konnektis.devicePaired();
  },
  patientSet: function() {
    return Konnektis.patientSet();
  },
  devicePairedOrPatientSet: function() {
    return Konnektis.devicePaired() || Konnektis.patientSet();
  },
  appointments: function() {
    var appointments = Appointments.find({
      $and: [
        // { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        // { carerUsername: Meteor.user().username }
      ]
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var toReturn = [];
    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];
      a.name = Konnektis.getAppointmentTypeLabel(a.name);
      a.state = Konnektis.stateString(a);
      a.stateLabel = Konnektis.stateLabelString(a);
      a.scheduledLabel = Konnektis.scheduledLabelString(a);
      // var comments = Comments.find({ parentId: a._id });

      // a.commentCount = comments.count();

      // var tasks = Tasks.find({ appointmentId: a._id });
      // a.taskCount = tasks.count();

      // if(a.recurringAppointmentId) {
      //   var recurringTasks = RecurringTasks.find({ recurringAppointmentId: a.recurringAppointmentId });
      //   a.taskCount += recurringTasks.count();
      // }

      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate);
      }

      toReturn.push(a);
    }

    var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.currentDate, null, false, true);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      occurrence.name = Konnektis.getAppointmentTypeLabel(occurrence.name);
      occurrence.state = 'scheduled';
      occurrence.stateLabel = 'Scheduled';
      occurrence.scheduledStartDay = moment(this.currentDate).toDate();
      occurrence.scheduledEndDay = moment(this.currentDate).toDate();
      occurrence.scheduledStartTime = occurrence.startTime;
      occurrence.scheduledEndTime = occurrence.endTime;
      occurrence.scheduledLabel = Konnektis.scheduledLabelString(occurrence);

      // var recurringTasks = RecurringTasks.find({ recurringAppointmentId: occurrence._id });
      // occurrence.taskCount = recurringTasks.count();


      toReturn.push(occurrence);
    }

    toReturn.sort(Konnektis.compareAppointmentStartTimes);

    return toReturn;
  },
});

Template.appointmentSummary.helpers({
  carerName: function() {
    if(this.carerProfile) {
      var carerName = [];
      if(this.carerProfile['first-name']) {
        carerName.push(this.carerProfile['first-name']);
      }
      if(this.carerProfile['last-name']) {
        carerName.push(this.carerProfile['last-name']);
      }
      if(carerName.length > 0) {
        return carerName.join(' ');
      }
    }

    // otherwise
    return this.carerUsername;
  },
});

Template.appointmentSummary.events({
  'click .appointment a': function(e, t) {
    var params = {};
    var currentDate = Template.parentData().currentDate;

    SessionAmplify.set("loggingInToAppointmentSummary", (this.description || this.name) + ', ' + this.scheduledLabel + ', ' + Konnektis.formatNiceDate(currentDate));

    if (!Meteor.userId()) {
      SessionAmplify.set("walkupAppointment", this._id);
      SessionAmplify.set("walkupCurrentDate", currentDate);
      SessionAmplify.set("walkupPatientId", this.patientId);
    }
    else {
      if(currentDate) {
        params['_currentDate'] = Konnektis.formatDateForLink(currentDate);
      }
      SessionAmplify.set("currentAppointment", this._id);
    }

    Router.go('dashboardView', params);
  },
});
