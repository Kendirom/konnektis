'use strict';

Template.riskAssessmentView.created = function () {
  this.riskAssessmentChanged = true;

  var lastLogin = Konnektis.getCarerPatientLastLogin();

  if(lastLogin) {
    var carerId = Meteor.userId();
    var patientId = Konnektis.patientId();

    this.riskAssessmentChanged = Konnektis.hasPatientInfoChanged(RiskAssessments, carerId, patientId, lastLogin, Konnektis.patientDocuments.riskAssessment);
  }
  else {
    // no last login for some reason
  }
};

Template.riskAssessmentView.rendered = function () {
  $('#riskAssessmentChanges').modal({
    backdrop: 'static',
    keyboard: false,
    show: true
  });
  $('#riskAssessmentChanges').off('hidden.bs.modal');
};

Template.riskAssessmentView.helpers({
  riskAssessment: function() {
    var riskAssessment = RiskAssessments.findOne();
    return (riskAssessment ? riskAssessment.riskAssessment : null);
  },
  riskAssessmentChanged: function() {
    return Template.instance().riskAssessmentChanged;
  },
  riskAssessmentChanges: function() {
    var patientId = Konnektis.patientId();
    var riskAssessment = RiskAssessments.findOne({patientId: patientId});
    return riskAssessment.changeText;
  },
});

Template.riskAssessmentChangesView.events({
  'click .btn-cancel': function(event, template) {
    event.preventDefault();

    $('#riskAssessmentChanges').on('hidden.bs.modal', function (e) {
      Router.go('dashboardView');
    })
  },
  'click .btn-save': function(event, template) {
    event.preventDefault();

    var patientId = Konnektis.patientId();
    Konnektis.callMethodIfConnected('setCarerPatientInfoConfirm', patientId, Konnektis.patientDocuments.riskAssessment, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        // all's fine, nothing to do, login has been recorded
        $('#riskAssessmentChanges').modal('hide');
      }
    });
  },
});
