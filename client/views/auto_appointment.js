'use strict';

Template.autoAppointmentView.onCreated(function(){
  // attempt to auto-start appointment
  var appointmentId = SessionAmplify.get("currentAppointment");
  var isCarer = Konnektis.isCarer();
  var appointment;
  var appointment_started;

  var self = this;

  var goto_dashboard = function() {
    var params = {};
    var currentDate = self.currentDate;

    if(currentDate) {
      params['_currentDate'] = Konnektis.formatDateForLink(currentDate);
    }

    Router.go('dashboardView', params);
  };

  if(isCarer) {
    appointment = Appointments.findOne(appointmentId);
    appointment_started = appointment && appointment.actualStartDate;
  }

  if(isCarer && !appointment_started) {
    Konnektis.callMethodIfConnected("startAppointment", appointmentId, Konnektis.patientId(), function(error, result) {
      if (error) {
        if(error.message) {
          if(error.message === '[Cannot edit past events]') {
            // this is a past event, it can't be started
            goto_dashboard();
          }
          else {
            Errors.throw(error.message);
            console.log('errors: ' + error.message);
          }
        }
        else {
          Errors.throw('Error starting appointment, but no error message returned');
        }
      }
      else {
        SessionAmplify.set("currentAppointment", appointmentId);

        goto_dashboard();
        // if(appointmentId != concreteOrRecurringAppointmentId) {
        //   // navigate to newly created (made concrete) appointment
        //   Router.go('detailsView', {_id: appointmentId}, {query: 'date=' + encodeURIComponent(appointmentDate)});
        // }
        // else {
        //   // force a reload of this view
        //   // Router.go('detailsView', {_id: appointmentId}, {query: 'date=' + encodeURIComponent(appointmentDate) + '&rnd=' + Konnektis.generateId()});
        // }
      }
    });
  }
  else {
    // either not a carer or appointment has already been started - just go to the dashboard
    goto_dashboard();
  }
});
