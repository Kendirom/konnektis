'use strict';

var repeat = function(id) {
  var repeat = Session.get('repeat');

  if (repeat !== undefined) {
    return repeat;
  }

  medication = RecurringMedications.findOne(id);
  if (medication) {
    return true;
  }

  var medication = Medications.findOne(id);
  if (medication) {

    if (medication.recurringMedicationId) {
      return true;
    } else {
      return false;
    }
  }

  return false;
};

var isConcrete = function(id) {
  var medication = Medications.findOne(id);

  if (medication) {
    return true;
  } else {
    return false;
  }
}

function createMedication(form, date) {
  function create (appointmentId) {
    var medication = {
      patientId: Konnektis.patientId(),
      appointmentId: appointmentId,
      recurringAppointmentId: null,
      name: form.name.value,
      description: form.description.value,
      scheduledStartDay: moment.utc(form.scheduledStartDay.value).toDate(),
      scheduledStartTime: form.scheduledStartTime.value,
      scheduledStartTimeType: form.scheduledStartTimeType.value,
      scheduledEndDay: moment.utc(form.scheduledEndDay.value).toDate(),
      scheduledEndTime: form.scheduledEndTime.value,
      scheduledEndTimeType: form.scheduledEndTimeType.value,
      isEvent: false,
      carerUsername: form.carerUsername.value,
    };
    Konnektis.callMethodIfConnected("createMedication", medication, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        history.go(-1);
      }
    });
  }

  if (form.appointmentId.value) {
    Konnektis.runForAppointmentOrConcreteIfRecurring(
      form.appointmentId.value,
      moment.utc(form.scheduledStartDay.value).toDate(),
      function() {
       create(form.appointmentId.value);
      },
      function(id) {
       create(id);
      });
  } else {
   create(null);
  }
}

function setDefaultValuesFromRecurringAppointment(form, recurringAppointment) {
  if (Session.get('repeat') === false) {
    return;
  }

  function outsideOfBoundsOfAppointment(medicationTime, appointmentStartTime, appointmentEndTime) {
    return medicationTime < appointmentStartTime
      || medicationTime > appointmentEndTime;
  }

  function normaliseStartTime(appointmentStartTime, appointmentEndTime) {
    if (!form.startTime.value
      || outsideOfBoundsOfAppointment(form.startTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentStartTime) {
        form.startTime.value = appointmentStartTime;
      }
    }
  }

  function normaliseEndTime(appointmentStartTime, appointmentEndTime) {
    if (!form.endTime.value
      || outsideOfBoundsOfAppointment(form.endTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentEndTime) {
        form.endTime.value = appointmentEndTime;
      }
    }
  }

  if (recurringAppointment) {
    form.recurrenceStartDay.value = Konnektis.formatDateForLink(recurringAppointment.recurrenceStartDay);
    form.recurrenceEndDay.value = Konnektis.formatDateForLink(recurringAppointment.recurrenceEndDay);
    form.carerUsername.value = recurringAppointment.carerUsername;
    normaliseStartTime(recurringAppointment.startTime, recurringAppointment.endTime);
    normaliseEndTime(recurringAppointment.startTime, recurringAppointment.endTime);
  } else {
    form.carerUsername.value = "";
  }
}

function setDefaultValuesFromAppointment(form, appointment, recurringAppointment) {
  if (Session.get('repeat') === true) {
    return;
  }

  function outsideOfBoundsOfAppointment(medicationTime, appointmentStartTime, appointmentEndTime) {
    return medicationTime < appointmentStartTime
      || medicationTime > appointmentEndTime;
  }

  function normaliseStartTime(appointmentStartTime, appointmentEndTime) {
    if (!form.scheduledStartTime.value
      || outsideOfBoundsOfAppointment(form.scheduledStartTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentStartTime) {
        form.scheduledStartTime.value = appointmentStartTime;
      }
    }
  }

  function normaliseEndTime(appointmentStartTime, appointmentEndTime) {
    if (!form.scheduledEndTime.value
      || outsideOfBoundsOfAppointment(form.scheduledEndTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentEndTime) {
        form.scheduledEndTime.value = appointmentEndTime;
      }
    }
  }

  if (appointment || recurringAppointment) {
    Session.set('displayTimeTypeDropDowns', false);
    form.scheduledStartTimeType.value = 'specific';
    form.scheduledEndTimeType.value = 'specific';
    Session.set('displaySpecificStartTimeInput', true);
    Session.set('displaySpecificEndTimeInput', true);

    var date = Konnektis.formatDateForLink(moment.utc(Template.parentData().date).toDate());

    // Timeout ensures that meteor has time to re-run the reactive view
    // logic based on the above changes before running the function within.
    setTimeout(function() {
      if (appointment) {
        form.scheduledStartDay.value = Konnektis.formatDateForLink(appointment.scheduledStartDay);
        form.scheduledEndDay.value = Konnektis.formatDateForLink(appointment.scheduledEndDay);
        form.carerUsername.value = appointment.carerUsername;
        normaliseStartTime(appointment.scheduledStartTime, appointment.scheduledEndTime);
        normaliseEndTime(appointment.scheduledStartTime, appointment.scheduledEndTime);
      } else {
        form.scheduledStartDay.value = date;
        form.scheduledEndDay.value = date;
        form.carerUsername.value = recurringAppointment.carerUsername;
        normaliseStartTime(recurringAppointment.startTime, recurringAppointment.endTime);
        normaliseEndTime(recurringAppointment.startTime, recurringAppointment.endTime);
      }
    }, 0);
  } else {
    Session.set('displayTimeTypeDropDowns', true);
    form.carerUsername.value = "";
  }
}

Template.medicationRepeatOption.helpers({
  repeat: function() {
    return repeat(this.id);
  }
});

Template.medicationRepeatOption.events({
  'change .repeat': function(event) {
    Session.set('repeat', event.target.checked);
  }
});

Template.existingMedicationView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
  isConcrete: function() {
    return isConcrete(this.id);
  }
});

Template.newMedicationView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
});

Template.medicationView.helpers({
  id: function() {
    return this.id;
  }
});

Template.medicationForm.events({
  'blur #start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-field');
      var endField = $('#end-time-field');
      if (!endField) {
        console.log('Could not find the end time field, unable to auto set it\'s value from the start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #start-time-dropdown-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-dropdown-field');
      var endField = $('#end-time-dropdown-field');
      if (!endField) {
        console.log('Could not find the end time dropdown field, unable to auto set it\'s value from the start time dropdown field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #start-time-type-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-type-field');
      var endField = $('#end-time-type-field');
      if (!endField) {
        console.log('Could not find the end time type field, unable to auto set it\'s value from the start time type field');
        return;
      }

      endField.val(startField.val());
      Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
    }
    Session.set('displaySpecificStartTimeInput', event.target.value === 'specific');
  },
  'change #end-time-type-field': function(e) {
    Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
  },
  'blur #start-day-field': function(e) {
    var startField = $('#start-day-field');
    var endField = $('#end-day-field');
    if (!endField) {
      console.log('Could not find the end day field, unable to auto set it\'s value from the start day field');
      return;
    }

    endField.val(startField.val());
  },
  'change .appointment-id': function(event) {
    var form = event.target.closest('form');
    var appointment = Appointments.findOne(form.appointmentId.value);
    var recurringAppointment = RecurringAppointments.findOne(form.appointmentId.value);

    setDefaultValuesFromAppointment(form, appointment, recurringAppointment);
  }
});

Template.medicationForm.helpers({
  displaySpecificStartTimeInput: function() {
    return Session.get('displaySpecificStartTimeInput');
  },
  displaySpecificEndTimeInput: function() {
    return Session.get('displaySpecificEndTimeInput');
  },
  displayConcreteDates: function() {
    return !repeat(this.id) || isConcrete(this.id)
  },
  displayConcreteTimes: function() {
    return !repeat(this.id);
  },
  displayConcreteAppointment: function() {
    return !repeat(this.id);
  },
  displayTimeTypeDropDowns: function() {
    return Session.get('displayTimeTypeDropDowns');
  },
  medication: function() {
    var medication = Medications.findOne(this.id);

    if (!medication) {
      medication = RecurringMedications.findOne(this.id);

      if (medication) {
        medication.scheduledStartDay = Konnektis.getDateFromUtc(this.date);
        medication.scheduledEndDay = Konnektis.getDateFromUtc(this.date);
        // Because the date picker deals in UTC
        medication.scheduledStartTime = Konnektis.getDateFromUtc(medication.scheduledStartTime);
        medication.scheduledEndTime = Konnektis.getDateFromUtc(medication.scheduledEndTime);
      } else {
        medication = {
          scheduledStartDay: Konnektis.getDateFromUtc(this.date),
          scheduledEndDay: Konnektis.getDateFromUtc(this.date),
          scheduledStartTimeType: 'specific',
          scheduledEndTimeType: 'specific'
        };

        if (this.appointmentId) {
          var appointment = Appointments.findOne(this.appointmentId);
          var recurringAppointment = RecurringAppointments.findOne(this.appointmentId);

          if (appointment) {
            medication.appointmentId = appointment._id;
          } else if (recurringAppointment) {
            medication.recurringAppointmentId = recurringAppointment._id;

            Session.setDefault('repeat', true);
          }
        }
      }
    }

    // Because the date picker deals in UTC
    medication.scheduledStartDay = Konnektis.getDateFromUtc(medication.scheduledStartDay);
    medication.scheduledEndDay = Konnektis.getDateFromUtc(medication.scheduledEndDay);

    Session.setDefault('displaySpecificStartTimeInput', !medication.scheduledStartTimeType || medication.scheduledStartTimeType === 'specific');
    Session.setDefault('displaySpecificEndTimeInput', !medication.scheduledEndTimeType || medication.scheduledEndTimeType === 'specific');
    if (medication.recurringAppointmentId) {
      Session.setDefault('perAppointment', true);
    }

    return medication;
  },
  appointmentOptions: function() {
    var queries = [{ patientId: Konnektis.patientId() }];

    if (this.date) {
      queries.push({ scheduledStartDay: { $lte: moment.utc(this.date).endOf('day').toDate() } });
      queries.push({ scheduledEndDay: { $gte: moment.utc(this.date).startOf('day').toDate() } });
    }

    if (this.appointmentId) {
      queries.push({ _id: this.appointmentId });
    }

    var appointments = Appointments.find({ $and: queries }).fetch();

    var options = [];

    options.push({ label: "General responsibility", value: ""});

    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      options.push({
        label: Konnektis.formatDate(a.scheduledStartDay) + ' ' + (a.scheduledStartTime ? a.scheduledStartTime : '')
          + ' - '
          + Konnektis.formatDate(a.scheduledEndDay) + ' ' + (a.scheduledEndTime ? a.scheduledEndTime : '')
          + ' - '
          + (a.carerUsername ? ' ' + a.carerUsername : '')
          + ' - ' + Konnektis.getAppointmentTypeLabel(a.name),
        value: a._id
      });
    }

    if (this.date) {
      var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.date);

      for (var i = 0; i < occurrences.length; i++) {
        var occurrence = occurrences[i];

        options.push({
          label: (occurrence.startTime ? occurrence.startTime : '')
          + (occurrence.endTime ? ' - ' + occurrence.endTime : '')
          + (occurrence.carerUsername ? ' ' + occurrence.carerUsername : '')
          + ' - ' + Konnektis.getAppointmentTypeLabel(occurrence.name),
          value: occurrence._id
        });
      }
    }

    return options;
  },
  carers: function() {
    var carers = Carers.find({ patients: Konnektis.patientId() }).fetch();

    return carers.map(function(value) {
     return { value: value.username, label: value.username };
    });
  }
});

Template.recurringMedicationForm.events({
  'change #recurrenceUnit': function(event) {
    Session.set('perAppointment', event.target.value === 'appointment')
  },
  'change .recurring-appointment-id': function(event) {
    var form = event.target.closest('form');
    var recurringAppointment = RecurringAppointments.findOne(form.recurringAppointmentId.value);

    setDefaultValuesFromRecurringAppointment(form, recurringAppointment);
  },
  'blur #recurring-start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-field');
      var endField = $('#recurring-end-time-field');
      if (!endField) {
        console.log('Could not find the recurring end time field, unable to auto set it\'s value from the recurring start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #recurring-start-time-dropdown-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-dropdown-field');
      var endField = $('#recurring-end-time-dropdown-field');
      if (!endField) {
        console.log('Could not find the recurring end time dropdown field, unable to auto set it\'s value from the recurring start time dropdown field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #recurring-start-time-type-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Medications.findOne(id) && !Medications.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-type-field');
      var endField = $('#recurring-end-time-type-field');
      if (!endField) {
        console.log('Could not find the recurring end time type field, unable to auto set it\'s value from the recurring start time type field');
        return;
      }

      endField.val(startField.val());
      Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
    }
    Session.set('displaySpecificStartTimeInput', event.target.value === 'specific');
  },
  'change #recurring-end-time-type-field': function(e) {
    Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
  },
  'blur #recurring-start-day-field': function(e) {
    var startField = $('#recurring-start-day-field');
    var endField = $('#recurring-end-day-field');
    if (!endField) {
      console.log('Could not find the recurring end day field, unable to auto set it\'s value from the recurring start day field');
      return;
    }

    endField.val(startField.val());
  }
});

Template.medicationForm.onRendered(function() {
  var appointmentId = Template.parentData().appointmentId;
  var id = Template.parentData().id;

  var form = document.getElementsByClassName('medicationForm')[0];

  var medication;
  if (id) {
    medication = Medications.findOne(id);
  }

  if (medication) {
    if (medication.appointmentId) {
      Session.set('displayTimeTypeDropDowns', false);
      if (!medication.recurringMedicationId) {
        form.scheduledStartTimeType.value = 'specific';
        form.scheduledEndTimeType.value = 'specific';
      }
    } else {
      Session.set('displayTimeTypeDropDowns', true);
    }
  } else if (appointmentId) {
    var appointment = Appointments.findOne(appointmentId);
    var recurringAppointment = RecurringAppointments.findOne(appointmentId);

    setDefaultValuesFromAppointment(form, appointment, recurringAppointment);
    setDefaultValuesFromRecurringAppointment(form, recurringAppointment);
  } else {
    setDefaultValuesFromAppointment(form, null, null);
    setDefaultValuesFromRecurringAppointment(form, null);
  }
});

Template.recurringMedicationForm.helpers({
  displaySpecificStartTimeInput: function() {
    return Session.get('displaySpecificStartTimeInput');
  },
  displaySpecificEndTimeInput: function() {
    return Session.get('displaySpecificEndTimeInput');
  },
  perAppointment: function() {
    return Session.get('perAppointment');
  },
  recurringAppointmentOptions: function() {
    var options = [];

    if (this.date) {
      var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.date, null, true);

      for (var i = 0; i < occurrences.length; i++) {
        var occurrence = occurrences[i];

        options.push({
          label: Konnektis.formatDate(this.date)
            + ' - ' + occurrence.startTime
            + (occurrence.carerUsername ? ' - ' + occurrence.carerUsername : '')
            + ' - ' + Konnektis.getAppointmentTypeLabel(occurrence.name),
          value: occurrence._id
        });
      }
    }

    return options;
  },
  recurringMedication: function() {
    var medication = Medications.findOne(this.id);

    var recurringMedication;
    if (medication) {
      recurringMedication = RecurringMedications.findOne(medication.recurringMedicationId);
    }

    if (!recurringMedication) {
      recurringMedication = RecurringMedications.findOne(this.id);
    }

    if (recurringMedication) {
      // Because the date picker deals in UTC
      recurringMedication.recurrenceStartDay = Konnektis.getDateFromUtc(recurringMedication.recurrenceStartDay);
      recurringMedication.recurrenceEndDay = Konnektis.getDateFromUtc(recurringMedication.recurrenceEndDay);
    } else {

      recurringMedication = {
        recurrenceStartDay: Konnektis.getDateFromUtc(this.date),
        recurrenceEndDay: Konnektis.getDateFromUtc(this.date),
        recurrenceUnit: 'day',
        startTimeType: medication ? medication.scheduledStartTimeType : 'specific',
        endTimeType: medication ? medication.scheduledEndTimeType : 'specific',
        startTime: medication ? medication.scheduledStartTime : null,
        endTime: medication ? medication.scheduledEndTime : null
      };

      if (this.appointmentId) {
        var recurringAppointment = RecurringAppointments.findOne(this.appointmentId);

        if (recurringAppointment) {
          recurringMedication.carerUsername = recurringAppointment.carerUsername;
          recurringMedication.recurringAppointmentId = recurringAppointment._id;
          recurringMedication.recurrenceUnit = 'appointment';
          recurringMedication.startTimeType = 'specific';
          recurringMedication.endTimeType = 'specific';
          recurringMedication.startTime = recurringAppointment.startTime;
          recurringMedication.endTime = recurringAppointment.endTime;
        }
      }
    }

    Session.setDefault('displaySpecificStartTimeInput', !recurringMedication || !recurringMedication.startTimeType || recurringMedication.startTimeType === 'specific');
    Session.setDefault('displaySpecificEndTimeInput', !recurringMedication || !recurringMedication.endTimeType || recurringMedication.endTimeType === 'specific');
    Session.setDefault('perAppointment', recurringMedication && recurringMedication.recurrenceUnit && recurringMedication.recurrenceUnit === 'appointment');

    return recurringMedication;
  }
});

Template.medicationView.events({
  'submit #medicationCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    createMedication(form, Template.parentData().date);

    return false;
  },
  'submit #recurringMedicationCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var parentData = Template.parentData();

    var recurringMedication = {
      patientId: Konnektis.patientId(),
      name: form.name.value,
      description: form.description.value,
      carerUsername: form.carerUsername.value,
      recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
      recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
      startTimeType: form.startTimeType.value,
      endTimeType: form.endTimeType.value,
      startTime: form.startTime.value,
      endTime: form.endTime.value,
      recurrenceUnit: form.recurrenceUnit.value
    };

    if (recurringMedication.recurrenceUnit === 'appointment') {
      recurringMedication.recurringAppointmentId = form.recurringAppointmentId.value;
    }

    var date = Template.parentData().date;
    Konnektis.callMethodIfConnected("createRecurringMedication", recurringMedication, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        history.go(-1);
      }
    });

    return false;
  }
});

Template.existingMedicationView.events({
  // Update an medication without the repeat box checked
  'submit #updateMedicationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);
    var medication = Medications.findOne(Router.current().data().id);

    if (medication) {
      // if medication exists already it's either already non recurring or is concrete AND we are canceling recurrence
      // if already non recurring save without hassle
      // if was recurring confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end date of recurrence to the last occurrence and set the recurring app id of the app to null

      if (medication.recurringMedicationId) {
        bootbox.confirm("Warning: Removing recurrence from the medication will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            var recurringMedication = RecurringMedications.findOne(medication.recurringMedicationId);

            if (!recurringMedication) {
              // throw
            }

            Konnektis.callMethodIfConnected("endRecurringMedicationPreviousDay", recurringMedication, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                medication.recurringMedicationId = null;
                medication.name = form.name.value;
                medication.description = form.description.value;
                medication.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
                medication.scheduledStartTimeType = form.scheduledStartTimeType.value;
                medication.scheduledStartTime = form.scheduledStartTime.value;
                medication.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
                medication.scheduledEndTimeType = form.scheduledEndTimeType.value;
                medication.scheduledEndTime = form.scheduledEndTime.value;
                medication.carerUsername = form.carerUsername.value;

                if (form.appointmentId) {
                  medication.appointmentId = form.appointmentId.value;
                }

                Konnektis.callMethodIfConnected("updateMedication", medication, function(error, result) {
                  if (error) {
                    Errors.throw(error.message);
                  } else {
                    Konnektis.removeAllConcreteMedicationOccurrencesFromDate(
                      recurringMedication._id,
                      dateFromRouteData,
                      function(a) {
                        return a._id !== medication._id;
                      });

                    history.go(-1);
                  }
                });
              }
            });
          }
        });
      } else {
        medication.recurringMedicationId = null;
        medication.name = form.name.value;
        medication.description = form.description.value;
        medication.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
        medication.scheduledStartTimeType = form.scheduledStartTimeType.value;
        medication.scheduledStartTime = form.scheduledStartTime.value;
        medication.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
        medication.scheduledEndTimeType = form.scheduledEndTimeType.value;
        medication.scheduledEndTime = form.scheduledEndTime.value;
        medication.carerUsername = form.carerUsername.value;

        if (form.appointmentId) {
          medication.appointmentId = form.appointmentId.value;
        }

        Konnektis.callMethodIfConnected("updateMedication", medication, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            history.go(-1);
          }
        });
      }
    } else {
      // if medication doesn't exist it was recurring and needs to be concreting; also we are canceling recurrence
      // confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end of recurrence to the last occurrence and create a concrete app without recurring app id

      var recurringMedication = RecurringMedications.findOne(Router.current().data().id);

      if (recurringMedication) {
        bootbox.confirm("Warning: Removing recurrence from the medication will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            Konnektis.callMethodIfConnected("endRecurringMedicationPreviousDay", recurringMedication, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                Konnektis.removeAllConcreteMedicationOccurrencesFromDate(
                  recurringMedication._id,
                  dateFromRouteData);

                createMedication(form, dateFromRouteData);
              }
            });
          }
        });
      } else {
        // throw
      }
    }

    return false;
  },
  'submit #updateRecurringMedicationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var recurringMedication = RecurringMedications.findOne(Router.current().data().id);

    if (!recurringMedication) {
      // throw
    }

    // is not concrete so just assume recurring already
    recurringMedication.name = form.name.value;
    recurringMedication.description = form.description.value;
    recurringMedication.carerUsername = form.carerUsername.value;
    recurringMedication.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
    recurringMedication.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
    recurringMedication.startTimeType = form.startTimeType.value;
    recurringMedication.endTimeType = form.endTimeType.value;
    recurringMedication.startTime = form.startTime.value;
    recurringMedication.endTime = form.endTime.value;
    recurringMedication.recurrenceUnit = form.recurrenceUnit.value;

    if (recurringMedication.recurrenceUnit === 'appointment') {
      recurringMedication.recurringAppointmentId = form.recurringAppointmentId.value;
    }

    Konnektis.callMethodIfConnected("updateRecurringMedication", recurringMedication, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        Konnektis.updateConcreteMedicationsWithRecurringData(recurringMedication);

        history.go(-1);
      }
    });

    return false;
  },
  'submit #updateMedicationWithRecurrenceForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var medication = Medications.findOne(Router.current().data().id);

    var recurringMedication;
    if (medication) {
      var recurringMedication = RecurringMedications.findOne(medication.recurringMedicationId);
    } else {
      var recurringMedication = RecurringMedications.findOne(Router.current().data().id);
    }

    if (recurringMedication) {
      // if recurring exists already it's already recurring so we should save
      // we also need to update the concrete

      recurringMedication.name = form.name.value;
      recurringMedication.description = form.description.value;
      recurringMedication.carerUsername = form.carerUsername.value;
      recurringMedication.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
      recurringMedication.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
      recurringMedication.startTimeType = form.startTimeType.value;
      recurringMedication.endTimeType = form.endTimeType.value;
      recurringMedication.startTime = form.startTime.value;
      recurringMedication.endTime = form.endTime.value;
      recurringMedication.recurrenceUnit = form.recurrenceUnit.value;

      if (recurringMedication.recurrenceUnit === 'appointment') {
        recurringMedication.recurringAppointmentId = form.recurringAppointmentId.value;
      }

      Konnektis.callMethodIfConnected("updateRecurringMedication", recurringMedication, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var medication = Medications.findOne(Router.current().data().id);

          if (!medication) {
            // throw
          }

          medication.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          medication.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();

          Konnektis.callMethodIfConnected("updateMedication", medication, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              Konnektis.updateConcreteMedicationsWithRecurringData(recurringMedication);

              history.go(-1);
            }
          });
        }
      });

    } else {
      // if recurring doesn't exist it was concrete and needs to be recurring
      // create recurring and set recurring app id on concrete; and update other fields

      var recurringMedication = {
        patientId: Konnektis.patientId(),
        name: form.name.value,
        description: form.description.value,
        carerUsername: form.carerUsername.value,
        recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
        recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
        startTimeType: form.startTimeType.value,
        endTimeType: form.endTimeType.value,
        startTime: form.startTime.value,
        endTime: form.endTime.value,
        recurrenceUnit: form.recurrenceUnit.value
      };

      if (recurringMedication.recurrenceUnit === 'appointment') {
        recurringMedication.recurringAppointmentId = form.recurringAppointmentId.value;
      }

      Konnektis.callMethodIfConnected("createRecurringMedication", recurringMedication, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var medication = Medications.findOne(Router.current().data().id);

          if (!medication) {
            // throw
          }

          medication.recurringMedicationId = result;
          medication.name = form.name.value;
          medication.description = form.description.value;
          medication.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          medication.scheduledStartTimeType = form.startTimeType.value;
          medication.scheduledStartTime = form.startTime.value;
          medication.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
          medication.scheduledEndTimeType = form.endTimeType.value;
          medication.scheduledEndTime = form.endTime.value;
          medication.carerUsername = form.carerUsername.value;

          if (form.appointmentId) {
            medication.appointmentId = form.appointmentId.value;
          }

          Konnektis.callMethodIfConnected("updateMedication", medication, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              history.go(-1);
            }
          });
        }
      });
    }

    return false;
  }
});
