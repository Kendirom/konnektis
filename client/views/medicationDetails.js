'use strict';

Template.medicationDetailsView.helpers({
  id: function() {
    return this.id;
  },
  patientId: function() {
    return Konnektis.patientId();
  },
  familyRole: function() {
    return 'family:' + Konnektis.patientId();
  },
  isFamily: function() {
    return Konnektis.isFamily() && SessionAmplify.get("role") === 'family';
  },
  isCarer: function() {
    return Konnektis.isCarer() && SessionAmplify.get("role") === 'carer';
  }
});

Template.appointmentMedicationDetails.events({
  'keyup .appointment-comment-text-input': function(e) {
    e.preventDefault();
    var d = e.target.parentElement.parentElement.getElementsByClassName('submit-comment')[0];
    if (e.target.parentElement.parentElement.getElementsByClassName('submit-comment').length > 0) {
      if(d.className !== 'submit-comment active') {
        d.className = d.className + ' active';
      }
    }

    var input = e.target;

    var id = Template.parentData().id;

    var currentAppointment = SessionAmplify.get('currentAppointment');

    if (!currentAppointment || id !== currentAppointment) {
      return;
    }

    var commentSessionKey = Konnektis.getAppointmentCommentTextSessionKey(id);

    SessionAmplify.set(commentSessionKey, input.value);
  },
  'submit #comment-creation-form': function(e) {
    e.preventDefault();

    if(e.target.getElementsByClassName('submit-comment').length > 0) {
      e.target.getElementsByClassName('submit-comment')[0].className = 'submit-comment';
    }

    var form = e.target;

    var id = Template.parentData().id;
    var date = Template.parentData().date;

    function addComment(id, onSuccess) {
      var comment = {
        patientId: Konnektis.patientId(),
        parentType: 'appointment',
        parentId: id,
        timestamp: new Date(),
        username: Meteor.user().username,
        userType: Konnektis.isFamily() ? "Family" : "Carer",
        commentText: form.commentText.value,
        urgent: form.urgent.checked
      };

      Konnektis.callMethodIfConnected("createComment", comment, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          form.commentText.value = '';
          var commentSessionKey = Konnektis.getAppointmentCommentTextSessionKey(id);
          SessionAmplify.set(commentSessionKey, null);

          if (onSuccess) {
            onSuccess();
          }
          // Display success
        }
      });
    }

    Konnektis.runForAppointmentOrConcreteIfRecurring(
      id,
      date,
      function() {
        addComment(id);
      },
      function(id) {
        addComment(id, function() {
          Router.go('/medicationDetails/' + id + '?date=' + date);
        });
      }
    );
    form.urgent.checked = false;
    form.getElementsByClassName('urgent-icon')[0].src = '/images/urgent.png';
    return false;
  },

  'click #delete': function(e) {

      var id = Template.parentData().id;
      var date = Template.parentData().date;

      bootbox.confirm("Are you sure you want to delete this appointment? This will also delete all the tasks assigned to this appointment.", function(result) {
      if (result === true) {
          Konnektis.callMethodIfConnected("deleteAppointment", id, Konnektis.patientId(), function(error, result) {
          if (error) {
            Errors.throw(error.message);
          }
          else {
            if(date) {
              Router.go('/day/?date=' + date);
            }
            history.go(-1);
          }
      });
      }
    });
  },
  'click .delete_task': function(e) {

      e.preventDefault();

      var id = this._id;
      var date = Template.parentData().date;

      bootbox.confirm("Are you sure you want to delete this task?", function(result) {
      if (result === true) {
          Konnektis.callMethodIfConnected("deleteTask", id, Konnektis.patientId(), function(error, result) {
          if (error) {
            Errors.throw(error.message);
          }
          else {
            if(date) {
              Router.go('/day/?date=' + date);
            }
            history.go(-1);
          }
      });
      }
    });
  },

});

Template.appointmentMedicationDetails.helpers({
  patientName: function() {
    return Konnektis.patientName();
  },
  appointment: function() {
    var appointmentMedicationDetails = Appointments.findOne(this.id);

    if (appointmentMedicationDetails) {
      appointmentMedicationDetails.name = Konnektis.getAppointmentTypeLabel(appointmentMedicationDetails.name);
      appointmentMedicationDetails.carerUsername = appointmentMedicationDetails.carerUsername === 'external' ? "Not a System User" : appointmentMedicationDetails.carerUsername;
      var printDate = !Konnektis.scheduledToEndOnStartDay(appointmentMedicationDetails);

      appointmentMedicationDetails.scheduledLabel = Konnektis.scheduledLabelString(appointmentMedicationDetails, true, true);

      if (appointmentMedicationDetails.actualEndDate) {
        appointmentMedicationDetails.completedLabel = Konnektis.completedLabelString(appointmentMedicationDetails, this.date, true, true);
      }
    } else {
      appointmentMedicationDetails = RecurringAppointments.findOne(this.id);

      if (appointmentMedicationDetails) {
        appointmentMedicationDetails.name = Konnektis.getAppointmentTypeLabel(appointmentMedicationDetails.name);

        appointmentMedicationDetails.scheduledStartDay = moment(this.date).toDate();
        appointmentMedicationDetails.scheduledEndDay = moment(this.date).toDate();
        appointmentMedicationDetails.scheduledStartTime = appointmentMedicationDetails.startTime;
        appointmentMedicationDetails.scheduledEndTime = appointmentMedicationDetails.endTime;

        appointmentMedicationDetails.scheduledLabel = Konnektis.scheduledLabelString(appointmentMedicationDetails, true, true);
      }
    }

    return appointmentMedicationDetails;
  },
  commentText: function() {
    var id = Template.parentData().id;

    var commentSessionKey = Konnektis.getAppointmentCommentTextSessionKey(id);

    return SessionAmplify.get(commentSessionKey);
  },
  dateString: function() {
    return Konnektis.formatDate(this.date);
  },
  appointmentQueryParameters: function() {
    var query = {
      appointmentId: this.id,
    };

    var appointment = Appointments.findOne(this.id);

    if (appointment && appointment.scheduledStartDay) {
      var date = appointment.scheduledStartDay;
      query.date = Konnektis.formatDateForLink(date);
    }

    if (!appointment) {
      query.date = Konnektis.formatDateForLink(this.date);
    }

    return query;
  },
  dateQueryParameters: function() {
    return {
      date: Konnektis.formatDateForLink(this.date)
    }
  },
  remainingMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { appointmentId: this.id },
        { actualEndDate: null }
      ]
    },
    {reactive: true}).fetch();

    var toReturn = [];
    for (var i = 0; i < medications.length; i++) {
      var t = medications[i];

      if(t.description) {
        if(t.description.length > 80) {
          t.expandDescription = true;
          t.shortDescription = t.description.substring(0,80) + '…';
        }
      }
      var comments = Comments.find({ parentId: t._id }).fetch();

      t.comments = comments;
      t.commentCount = comments.length;

      for (var j = 0; j < t.comments.length; j++) {
        var a = t.comments[j];

        a.timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(a.timestamp));
        a.owner = Meteor.user().username === a.username;
      }

      var appointment = Appointments.findOne(this.id);

      // recurring appointments only span 1 day
      var printDate = appointment ? !Konnektis.scheduledToEndOnStartDay(appointment) : false;

      t.scheduledLabel = Konnektis.scheduledLabelString(t, true, printDate);

      t.owner = t.carerUsername === Meteor.user().username || t.carerUsername === 'external' || !t.carerUsername;
      t.carerUsername = t.carerUsername || 'General Responsibility';

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringMedicationOccurrencesForDate(this.date);

    var appointment = Appointments.findOne(this.id);
    var recurringAppointment = RecurringAppointments.findOne(this.id);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      var match = false;
      if (appointment) {
        match = occurrence.recurringAppointmentId
          && occurrence.recurringAppointmentId === appointment.recurringAppointmentId;
      } else if (recurringAppointment) {
        match = occurrence.recurringAppointmentId
          && occurrence.recurringAppointmentId === recurringAppointment._id;
      }

      if (match) {
        Konnektis.normaliseRecurringMedicationProperties(occurrence, this.date);

        occurrence.owner = occurrence.carerUsername === Meteor.user().username || occurrence.carerUsername === 'external' || !occurrence.carerUsername;

        toReturn.push(occurrence);
      }
    }

    if (toReturn.length > 0) {
      return Konnektis.orderTasksOrMedications(toReturn);
    } else {
      return null;
    }
  },
  remainingTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { appointmentId: this.id },
        { actualEndDate: null }
      ]
    },
    {reactive: true}).fetch();

    var toReturn = [];
    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      if(t.description) {
        if(t.description.length > 80) {
          t.expandDescription = true;
          t.shortDescription = t.description.substring(0,80) + '…';
        }
      }
      var comments = Comments.find({ parentId: t._id }).fetch();

      t.comments = comments;
      t.commentCount = comments.length;

      for (var j = 0; j < t.comments.length; j++) {
        var a = t.comments[j];

        a.timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(a.timestamp));
        a.owner = Meteor.user().username === a.username;
      }

      var appointment = Appointments.findOne(this.id);

      // recurring appointments only span 1 day
      var printDate = appointment ? !Konnektis.scheduledToEndOnStartDay(appointment) : false;

      t.scheduledLabel = Konnektis.scheduledLabelString(t, true, printDate);

      t.owner = t.carerUsername === Meteor.user().username || t.carerUsername === 'external' || !t.carerUsername;
      t.carerUsername = t.carerUsername || 'General Responsibility';

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringTaskOccurrencesForDate(this.date);

    var appointment = Appointments.findOne(this.id);
    var recurringAppointment = RecurringAppointments.findOne(this.id);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      var match = false;
      if (appointment) {
        match = occurrence.recurringAppointmentId
          && occurrence.recurringAppointmentId === appointment.recurringAppointmentId;
      } else if (recurringAppointment) {
        match = occurrence.recurringAppointmentId
          && occurrence.recurringAppointmentId === recurringAppointment._id;
      }

      if (match) {
        Konnektis.normaliseRecurringTaskProperties(occurrence, this.date);

        occurrence.owner = occurrence.carerUsername === Meteor.user().username || occurrence.carerUsername === 'external' || !occurrence.carerUsername;

        toReturn.push(occurrence);
      }
    }

    if (toReturn.length > 0) {
      return Konnektis.orderTasksOrMedications(toReturn);
    } else {
      return null;
    }
  },
  generalTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date(this.date)) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(new Date(this.date)) } },
        { actualEndDate: null },
        { isEvent: false },
        { appointmentId: null }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];

    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      if(t.description) {
        if(t.description.length > 80) {
          t.expandDescription = true;
          t.shortDescription = t.description.substring(0,80) + '…';
        }
      }

      var comments = Comments.find({ parentId: t._id }).fetch();
      t.comments = comments;
      t.commentCount = comments.length;

      for (var j = 0; j < t.comments.length; j++) {
        var a = t.comments[j];

        a.timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(a.timestamp));
        a.owner = Meteor.user().username === a.username;
      }

      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.owner = true;

      var appointment = Appointments.findOne(this.id);

      // recurring appointments only span 1 day
      var printDate = appointment ? !Konnektis.scheduledToEndOnStartDay(appointment) : false;

      t.scheduledLabel = Konnektis.scheduledLabelString(t, true, printDate);
      if (t.actualEndDate) {
        t.completedLabel = Konnektis.completedLabelString(t, this.currentDate, true, printDate);
      }

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringTaskOccurrencesForDate(this.date);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      var match = false;

      if (!occurrence.recurringAppointmentId && !occurrence.carerUsername)
      {
        match = true;
      }

      if (match) {
        Konnektis.normaliseRecurringTaskProperties(occurrence, this.date);

        occurrence.owner = true;

        toReturn.push(occurrence);
      }
    }

    if (toReturn.length > 0) {
      return Konnektis.orderTasksOrMedications(toReturn);;
    } else {
      return null;
    }
  },
  completedTasks: function() {
    var tasks = Tasks.find({
      $or: [{
        $and: [
          { patientId: Konnektis.patientId() },
          { appointmentId: this.id},
          { actualEndDate: { $ne: null } },
          { isEvent: false }
        ]},
        {
        $and: [
          { patientId: Konnektis.patientId() },
          { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date(this.date)) } },
          { scheduledEndDay: { $gte: Konnektis.getStartOfDay(new Date(this.date)) } },
          { actualEndDate: { $ne: null } },
          { isEvent: false },
          { completedUsername: Konnektis.appointmentCarerUsername(this.id) },
          { carerUsername: { $ne: Konnektis.appointmentCarerUsername(this.id) } },
        ]
      }
      ]
    },
    {reactive: true}).fetch();
    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      if(t.description) {
        if(t.description.length > 80) {
          t.expandDescription = true;
          t.shortDescription = t.description.substring(0,80) + '…';
        }
      }

      var comments = Comments.find({ parentId: t._id }).fetch();
      t.comments = comments;
      t.commentCount = comments.length;

      for (var j = 0; j < t.comments.length; j++) {

        t.comments[j].timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(t.timestamp));
        t.comments[j].owner = Meteor.user().username === t.comments[j].username;
      }
      var appointment = Appointments.findOne(this.id);

      // recurring appointments only span 1 day
      var printDate = appointment ? !Konnektis.scheduledToEndOnStartDay(appointment) : false;

      t.scheduledLabel = Konnektis.scheduledLabelString(t, true, printDate);
      if (t.actualEndDate) {
        t.completedLabel = Konnektis.completedLabelString(t, this.date, true, printDate);
      }

      t.owner = t.carerUsername === Meteor.user().username || t.completedUsername === Meteor.user().username;
      t.carerUsername = t.carerUsername || t.completedUsername;
    }

    if (tasks.length > 0 ) {
      return tasks;
    } else {
      return null;
    }
  },
  comments: function() {
    var comments = Comments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { parentId: this.id }
      ]
    },
    {reactive: true}).fetch();

    for (var i = 0; i < comments.length; i++) {
      var a = comments[i];

      a.timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(a.timestamp));
      a.owner = Meteor.user().username === a.username;

      var id = a.parentId;
      if (id) {
        var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
        if (appointment) {
          if (appointment.patientId !== Konnektis.patientId()) {
            Router.go('selectPatientView');
          }

          a.appointmentTitle = appointment.name.toProperCase();
        } else {
          var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
          if (task.patientId !== Konnektis.patientId()) {
            Router.go('selectPatientView');
          }

          if (task) {
            a.taskTitle = task.name;

            if(task.appointmentId) {
              var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
              if (taskAppointment) {
                if (taskAppointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = taskAppointment.name.toProperCase();
              }
            }
          }
        }
      }
    }

    if (comments.length > 0) {
      return comments;
    } else {
      return null;
    }
  },
  issues: function() {
    var date = new Date(this.date);
    var issues = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(date)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(date)) } },
            { urgent: true }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(date)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(date)) } },
            { urgent: true }
          ]
          }
        ]
    }).fetch();

    for (var i = 0; i < issues.length; i++) {
      var a = issues[i];
      if (a.timestamp) {
        a.timestamp = Konnektis.formatNiceDateTime(a.timestamp);
      }
      var medicationDetails = { type : "" , name: "" , description: ""};

      var id = a.parentId;
      if (id) {
        switch(a.parentType) {
          case 'appointment':
            var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
            if (appointment) {
              if (appointment.patientId !== Konnektis.patientId()) {
                Router.go('selectPatientView');
              }

              a.appointmentTitle = appointment.name.toProperCase();
            }
            break;
          case 'task':
            var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
            if(task) {
              if (task.patientId !== Konnektis.patientId()) {
                Router.go('selectPatientView');
              }

              a.taskTitle = task.name;

              if(task.appointmentId) {
                var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
                if (taskAppointment) {
                  if (taskAppointment.patientId !== Konnektis.patientId()) {
                    Router.go('selectPatientView');
                  }

                  a.appointmentTitle = taskAppointment.name.toProperCase();
                }
              }
            }
            break;
          case 'medication':
            var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
            if(medication) {
              if (medication.patientId !== Konnektis.patientId()) {
                Router.go('selectPatientView');
              }

              a.taskTitle = medication.name;

              if(medication.appointmentId) {
                var medicationAppointment = Appointments.findOne(medication.appointmentId) || RecurringAppointments.findOne(medication.appointmentId);
                if (medicationAppointment) {
                  if (medicationAppointment.patientId !== Konnektis.patientId()) {
                    Router.go('selectPatientView');
                  }

                  a.appointmentTitle = medicationAppointment.name.toProperCase();
                }
              }
            }
            break;
        }
      }
    }

    return issues;
  },
  numberOfIssues: function() {
    var date = new Date(this.date);
    var comments = Comments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { timestamp: { $gte: Konnektis.getStartOfDay(date) } },
        { timestamp: { $lte: Konnektis.getEndOfDay(date) } },
        { urgent: true }
      ]
    });

    return comments.count();
  },
  started: function() {
    var appointment = Appointments.findOne(this.id);

    return appointment && appointment.actualStartDate && !appointment.actualEndDate;
  },
  scheduled: function() {
    var appointment = Appointments.findOne(this.id);
    var recurringAppointment = RecurringAppointments.findOne(this.id);

    return (appointment && !appointment.actualStartDate && !appointment.actualEndDate) || recurringAppointment;
  },
  owner: function() {
    var appointment = Appointments.findOne(this.id);
    var recurringAppointment = RecurringAppointments.findOne(this.id);

    return (appointment && (appointment.carerUsername === Meteor.user().username || appointment.carerUsername === 'external' || !appointment.carerUsername))
      || (recurringAppointment && (recurringAppointment.carerUsername === Meteor.user().username || recurringAppointment.carerUsername === 'external' || !recurringAppointment.carerUsername));
  }
});

Template.appointmentCarerMedicationDetails.inheritsHelpersFrom("appointmentMedicationDetails");
Template.appointmentCarerMedicationDetails.inheritsEventsFrom("appointmentMedicationDetails");

Template.appointmentCarerMedicationDetails.helpers({
  numberOfMedications: function() {
    return getAllCarerMedications(this.id, this.date).length;
  },
  numberOfTasks: function() {
    return getAllCarerTasks(this.id, this.date).length;
  },
  allMedications: function() {
    return getAllCarerMedications(this.id, this.date);
  },
  allTasks: function() {
    return getAllCarerTasks(this.id, this.date);
  }
});

Template.appointmentCarerMedicationDetails.events({
  'click .select_status span': function(e) {
    function setCareStatus(appointmentId) {

      $('.select_status span').removeClass('selected');
      $(e.target).addClass('selected');

      Konnektis.callMethodIfConnected("setCareStatus", appointmentId, Konnektis.patientId(), e.target.dataset.status, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          // Display success

        }
      });
    }

    Konnektis.runForAppointmentOrConcreteIfRecurring(
      Router.current().data().id,
      Router.current().data().date,
      function(appointmentId) {
        setCareStatus(appointmentId);
      },
      function(appointmentId) {
        setCareStatus(appointmentId);
      });

    return false;
  },
  'click .complete': function(e) {
    function completeAppointment(appointmentId) {
      Konnektis.callMethodIfConnected("completeAppointment", appointmentId, Konnektis.patientId(), function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
           SessionAmplify.set("currentAppointment", null);
           Meteor.logout();
        }
      });
    }

    bootbox.confirm("Are you sure you want to complete this call?", function(result) {
      if (result === true) {
        Konnektis.runForAppointmentOrConcreteIfRecurring(
          Router.current().data().id,
          Router.current().data().date,
          function(appointmentId) {
            completeAppointment(appointmentId);
          },
          function (error) {
            Errors.throw(error.message);
          });
      }
    });

    return false;
  },
  'click .start': function(e) {
    function startAppointment(appointmentId) {
      Konnektis.callMethodIfConnected("startAppointment", appointmentId, Konnektis.patientId(), function(error, result) {
        if (error) {
          Errors.throw(error.message);
        }
        else {
          SessionAmplify.set("currentAppointment", appointmentId);
        }
      });
    }


    bootbox.confirm("Are you sure you want to start this appointment?", function(result) {
      if (result === true) {
        Konnektis.runForAppointmentOrConcreteIfRecurring(
          Router.current().data().id,
          Router.current().data().date,
          function(appointmentId) {
            startAppointment(appointmentId);
          },
          function(appointmentId) {
            startAppointment(appointmentId);
          });
      }
    });

    return false;
  },
  'click .urgent-icon': function(e, templateInstance) {
    e.preventDefault();

    var input = e.target;

    var form = e.target.parentElement;
    if(!form.urgent.checked) {
      input.src = '/images/urgent-checked.png';
    }
    else {
      input.src = '/images/urgent.png';
    }

    form.urgent.checked = !form.urgent.checked;
  },
});

function getAllCarerMedications(appointmentId, date) {
  var medications = Medications.find({
    $or: [
      // current user's completed medications today
      {
        $and: [
          { patientId: Konnektis.patientId() },
          { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date(date)) } },
          { scheduledEndDay: { $gte: Konnektis.getStartOfDay(new Date(date)) } },
          { actualEndDate: { $ne: null } },
          { isEvent: false },
          { completedUsername: Konnektis.appointmentCarerUsername(appointmentId) },
          { carerUsername: { $ne: Konnektis.appointmentCarerUsername(appointmentId) } },
        ]
      },
      // uncompleted medications from today without an appointment
      {
        $and: [
          { patientId: Konnektis.patientId() },
          { scheduledStartDay: { $lte: Konnektis.getEndOfDay(new Date(date)) } },
          { scheduledEndDay: { $gte: Konnektis.getStartOfDay(new Date(date)) } },
          { actualEndDate: null },
          { isEvent: false },
          { appointmentId: null }
        ]
      },
      // uncompleted medications in this appointment
      {
        $and: [
          { patientId: Konnektis.patientId() },
          { appointmentId: appointmentId },
        ]
      }
    ]
  },
  {reactive: true}).fetch();

  var toReturn = [];
  for (var i = 0; i < medications.length; i++) {
    var t = medications[i];

    if(t.description) {
      if(t.description.length > 80) {
        t.expandDescription = true;
        t.shortDescription = t.description.substring(0,80) + '…';
      }
    }
    var comments = Comments.find({ parentId: t._id }).fetch();

    t.comments = comments;
    t.commentCount = comments.length;

    for (var j = 0; j < t.comments.length; j++) {
      var a = t.comments[j];

      a.timestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(a.timestamp));
      a.owner = Meteor.user().username === a.username;
    }

    var appointment = Appointments.findOne(appointmentId);

    // recurring appointments only span 1 day
    var printDate = appointment ? !Konnektis.scheduledToEndOnStartDay(appointment) : false;

    t.scheduledLabel = Konnektis.scheduledLabelString(t, true, printDate);

    t.owner = t.carerUsername === Meteor.user().username || t.carerUsername === 'external' || !t.carerUsername;
    t.carerUsername = t.carerUsername || 'General Responsibility';

    toReturn.push(t);
  }

  var occurrences = Konnektis.getRecurringMedicationOccurrencesForDate(date);

  var appointment = Appointments.findOne(appointmentId);
  var recurringAppointment = RecurringAppointments.findOne(appointmentId);

  for (var i = 0; i < occurrences.length; i++) {
    var occurrence = occurrences[i];

    if (occurrence.description) {
      if (occurrence.description.length > 80) {
        occurrence.expandDescription = true;
        occurrence.shortDescription = occurrence.description.substring(0,80) + '…';
      }
    }

    var belongsToAppointment = false;
    if (appointment) {
      belongsToAppointment = occurrence.recurringAppointmentId
        && occurrence.recurringAppointmentId === appointment.recurringAppointmentId;
    } else if (recurringAppointment) {
      belongsToAppointment = occurrence.recurringAppointmentId
        && occurrence.recurringAppointmentId === recurringAppointment._id;
    }

    if (belongsToAppointment) {
      Konnektis.normaliseRecurringMedicationProperties(occurrence, date);

      occurrence.owner = occurrence.carerUsername === Meteor.user().username || occurrence.carerUsername === 'external' || !occurrence.carerUsername;

      toReturn.push(occurrence);
      continue;
    } else if (!occurrence.recurringAppointmentId && !occurrence.carerUsername) {
      // Does not belong to an appointment and is not assigned to anyone

      Konnektis.normaliseRecurringMedicationProperties(occurrence, date);

      occurrence.owner = true;

      toReturn.push(occurrence);
    }
  }

  if (toReturn.length > 0) {
    return Konnektis.orderTasksOrMedications(toReturn);
  } else {
    return [];
  }
}
