'use strict';

Template.appointment.helpers({
  appointmentQueryParameters: function() {
    var query = { };

    var parentData = Template.parentData();

    if (parentData.currentDate) {
      query.date = Konnektis.formatDateForLink(parentData.currentDate)
    }

    if (parentData.id) {
      query.appointmentId = parentData.id;

      var appointment = Appointments.findOne(query.appointmentId);

      if (appointment && appointment.scheduledStartDay) {
        var date = appointment.scheduledStartDay;
        query.date = Konnektis.formatDateForLink(date);
      }
    }

    return query;
  },
  dateQueryParameters: function() {
    return Konnektis.getDateQueryParameters(this.currentDate || Template.parentData().currentDate || Template.parentData().date);
  }
});

Template.issue.inheritsHelpersFrom("appointment");
Template.task.inheritsHelpersFrom("appointment");
Template.medication.inheritsHelpersFrom("appointment");

Template.taskCarerList.inheritsHelpersFrom("appointment");
Template.medicationCarerCompletedList.inheritsHelpersFrom("appointment");
Template.taskCarerCompletedList.inheritsHelpersFrom("appointment");

Template.dayView.helpers({
  id: function() {
    return this.id;
  },
  patientId: function() {
    return Konnektis.patientId();
  },
  patientName: function() {
    return Konnektis.patientName();
  },
  familyRole: function() {
    return 'family:' + Konnektis.patientId();
  },
  isFamily: function() {
    return Konnektis.isFamily() && SessionAmplify.get("role") === 'family';
  },
  isCarer: function() {
    return Konnektis.isCarer() && SessionAmplify.get("role") === 'carer';
  }
});

Template.medication.events({
 'click .delete_medication': function(e) {
      e.preventDefault();

      var id = this._id;

      bootbox.confirm("Are you sure you want to delete this medication?", function(result) {
      if (result === true) {
          Konnektis.callMethodIfConnected("deleteMedication", id, Konnektis.patientId(), function(error, result) {
          if (error) {
            Errors.throw(error.message);
          }
      });
      }
    });
  }
});

Template.task.events({
 'click .delete_task': function(e) {
      e.preventDefault();

      var id = this._id;

      bootbox.confirm("Are you sure you want to delete this task?", function(result) {
      if (result === true) {
          Konnektis.callMethodIfConnected("deleteTask", id, Konnektis.patientId(), function(error, result) {
          if (error) {
            Errors.throw(error.message);
          }
      });
      }
    });
  }
});

Template.dayFamilyView.helpers({
  careStatus: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
      ]
    }, {sort: {scheduledStartTime: -1}}).fetch();

    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      if(a.careStatus != null) {
        return a.careStatus
      }
    }

    return "neutral";
  },
  dateString: function() {
    return Konnektis.formatDate(this.currentDate);
  },
  patientName: function() {
    return Konnektis.patientName();
  },
  previousDateString: function() {
    var previousDay = new Date(this.currentDate);
    previousDay.setDate(this.currentDate.getDate() - 1);
    return Konnektis.formatDateForLink(previousDay);
  },
  nextDateString: function() {
    var nextDay = new Date(this.currentDate);
    nextDay.setDate(this.currentDate.getDate() + 1);
    return Konnektis.formatDateForLink(nextDay);
  },
  dateQueryParameters: function() {
    return Konnektis.getDateQueryParameters(this.currentDate);
  },
  remainingMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];
    for (var i = 0; i < medications.length; i++) {
      var t = medications[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringMedicationOccurrencesForDate(this.currentDate);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringMedicationProperties(occurrence, this.currentDate);

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  remainingTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];
    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringTaskOccurrencesForDate(this.currentDate);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringTaskProperties(occurrence, this.currentDate);

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  completedMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: { $ne: null } },
        { isEvent: false }
      ]
    },
    {reactive: false}).fetch();

    for (var i = 0; i < medications.length; i++) {
      var a = medications[i];

      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      //changed actualEndTime to actualEndDate so it works for now
      a.scheduledLabel = Konnektis.scheduledLabelString(a, true);
      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate, true);
      }
      a.carerUsername = a.carerUsername || a.completedUsername;
    }

    return medications;
  },
  completedTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: { $ne: null } },
        { isEvent: false }
      ]
    },
    {reactive: false}).fetch();

    for (var i = 0; i < tasks.length; i++) {
      var a = tasks[i];

      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      //changed actualEndTime to actualEndDate so it works for now
      a.scheduledLabel = Konnektis.scheduledLabelString(a, true);
      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate, true);
      }
      a.carerUsername = a.carerUsername || a.completedUsername;
    }

    return tasks;
  },
  appointments: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
      ]
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var toReturn = [];
    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      a.state = Konnektis.stateString(a);
      a.carerUsername = a.carerUsername === 'external' ? "Not a System User" : a.carerUsername;
      a.stateLabel = Konnektis.stateLabelString(a);
      a.scheduledLabel = Konnektis.scheduledLabelString(a);
      a.name = Konnektis.getAppointmentTypeLabel(a.name);
      var comments = Comments.find({ parentId: a._id });
      a.commentCount = comments.count();

      var medications = Medications.find({ appointmentId: a._id });
      a.medicationCount = medications.count();

      if (a.recurringAppointmentId) {
        var recurringMedications = RecurringMedications.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.medicationCount = Math.max(a.medicationCount, recurringMedications.count());
      }

      var tasks = Tasks.find({ appointmentId: a._id });
      a.taskCount = tasks.count();

      if (a.recurringAppointmentId) {
        var recurringTasks = RecurringTasks.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.taskCount = Math.max(a.taskCount, recurringTasks.count());
      }

      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate);
      }

      toReturn.push(a);
    }

    var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.currentDate);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      occurrence.state = 'scheduled';
      occurrence.stateLabel = 'Scheduled';
      occurrence.name = Konnektis.getAppointmentTypeLabel(occurrence.name);
      occurrence.carerUsername = occurrence.carerUsername === 'external' ? "Not a System User" : occurrence.carerUsername;
      occurrence.scheduledStartDay = moment(this.currentDate).toDate();
      occurrence.scheduledEndDay = moment(this.currentDate).toDate();
      occurrence.scheduledStartTime = occurrence.startTime;
      occurrence.scheduledEndTime = occurrence.endTime;
      occurrence.scheduledLabel = Konnektis.scheduledLabelString(occurrence);

      var recurringMedications = RecurringMedications.find({ recurringAppointmentId: occurrence._id });
      occurrence.medicationCount = recurringMedications.count();

      var recurringTasks = RecurringTasks.find({ recurringAppointmentId: occurrence._id });
      occurrence.taskCount = recurringTasks.count();

      toReturn.push(occurrence);
    }

    return _.sortBy(toReturn, 'scheduledStartTime');
  },
  issues: function() {
    var issues = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]
          }
        ]
    }).fetch();

    for (var i = 0; i < issues.length; i++) {
      var a = issues[i];
      if (a.timestamp) {
        a.timestamp = Konnektis.formatNiceDateTime(a.timestamp);
      }

      var id = a.parentId;
      if (id) {
        var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
        if (appointment) {
          if (appointment.patientId !== Konnektis.patientId()) {
            Router.go('selectPatientView');
          }

          a.appointmentTitle = appointment.name.toProperCase();
        } else {
          var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
          if (medication && (medication.patientId !== Konnektis.patientId())) {
            Router.go('selectPatientView');
          }

          if (medication) {
            a.medicationTitle = medication.name;

            if(medication.appointmentId) {
              var medicationAppointment = Appointments.findOne(medication.appointmentId) || RecurringAppointments.findOne(medication.appointmentId);
              if (medicationAppointment) {
                if (medicationAppointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = medicationAppointment.name.toProperCase();
              }
            }
          }

          var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
          if (task && (task.patientId !== Konnektis.patientId())) {
            Router.go('selectPatientView');
          }

          if (task) {
            a.taskTitle = task.name;

            if(task.appointmentId) {
              var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
              if (taskAppointment) {
                if (taskAppointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = taskAppointment.name.toProperCase();
              }
            }
          }
        }
      }
    }

    return issues;
  },
  numberOfComments: function() {
    var comments = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } }
          ]
          }
        ]
    });

    return comments.count();
  },
  numberOfIssues: function() {
    var comments = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]
          }
        ]
    });

    return comments.count();
  }
});

Template.dayCarerView.helpers({
  careStatus: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
      ]
    }, {sort: {scheduledStartTime: -1}}).fetch();

    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      if (a.careStatus != null) {
        return a.careStatus
      }
    }

    return "neutral";
  },
  dateString: function() {
    return Konnektis.formatDate(this.currentDate);
  },
  previousDateString: function() {
    var previousDay = new Date(this.currentDate);
    previousDay.setDate(this.currentDate.getDate() - 1);
    return Konnektis.formatDateForLink(previousDay);
  },
  nextDateString: function() {
    var nextDay = new Date(this.currentDate);
    nextDay.setDate(this.currentDate.getDate() + 1);
    return Konnektis.formatDateForLink(nextDay);
  },
  dateQueryParameters: function() {
    return Konnektis.getDateQueryParameters(this.currentDate);
  },
  patientName: function() {
    return Konnektis.patientName();
  },
  remainingMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false },
        {
          $or: [
            { carerUsername: Meteor.user().username },
            { carerUsername: null }
          ]
        }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];

    for (var i = 0; i < medications.length; i++) {
      var t = medications[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.owner = true;
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringMedicationOccurrencesForDate(this.currentDate,
      [{
        $or: [
          { carerUsername: Meteor.user().username },
          { carerUsername: null }
        ]
      }]
    );

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringMedicationProperties(occurrence, this.currentDate);

      occurrence.owner = true;

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  remainingTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false },
        {
          $or: [
            { carerUsername: Meteor.user().username },
            { carerUsername: null }
          ]
        }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];

    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.owner = true;
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringTaskOccurrencesForDate(this.currentDate,
      [{
        $or: [
          { carerUsername: Meteor.user().username },
          { carerUsername: null }
        ]
      }]
    );

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringTaskProperties(occurrence, this.currentDate);

      occurrence.owner = true;

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  otherRemainingMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false },
        { carerUsername: { $not: Meteor.user().username  } },
        { carerUsername: { $not: null  } }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];

    for (var i = 0; i < medications.length; i++) {
      var t = medications[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.owner = false;
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringMedicationOccurrencesForDate(this.currentDate,
      [
        { carerUsername: { $not: Meteor.user().username  } },
        { carerUsername: { $not: null  } }
      ]
    );

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringMedicationProperties(occurrence, this.currentDate);

      occurrence.owner = false;

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  otherRemainingTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: null },
        { isEvent: false },
        { carerUsername: { $not: Meteor.user().username  } },
        { carerUsername: { $not: null  } }
      ]
    }, {sort: {scheduledStartTimeType: -1, scheduledStartTime: 1}}, {reactive: false}).fetch();

    var toReturn = [];

    for (var i = 0; i < tasks.length; i++) {
      var t = tasks[i];

      var comments = Comments.find({ parentId: t._id });

      t.commentCount = comments.count();
      t.owner = false;
      t.carerUsername = t.carerUsername || 'General Responsibility';
      t.scheduledLabel = Konnektis.scheduledLabelString(t, true);

      toReturn.push(t);
    }

    var occurrences = Konnektis.getRecurringTaskOccurrencesForDate(this.currentDate,
      [
        { carerUsername: { $not: Meteor.user().username  } },
        { carerUsername: { $not: null  } }
      ]
    );

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      Konnektis.normaliseRecurringTaskProperties(occurrence, this.currentDate);

      occurrence.owner = false;

      toReturn.push(occurrence);
    }

    return Konnektis.orderTasksOrMedications(toReturn);
  },
  completedMedications: function() {
    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: { $ne: null } },
        { isEvent: false }
      ]
    },
    {reactive: true}).fetch();

    for (var i = 0; i < medications.length; i++) {
      var a = medications[i];
      //changed actualEndTime to actualEndDate so it works for now
      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      a.scheduledLabel = Konnektis.scheduledLabelString(a, true);
      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate, true);
      }

      a.owner = a.carerUsername === Meteor.user().username || a.completedUsername === Meteor.user().username;
      a.carerUsername = a.carerUsername ||  a.completedUsername;
    }

    return medications;
  },
  completedTasks: function() {
    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { actualEndDate: { $ne: null } },
        { isEvent: false }
      ]
    },
    {reactive: true}).fetch();

    for (var i = 0; i < tasks.length; i++) {
      var a = tasks[i];
      //changed actualEndTime to actualEndDate so it works for now
      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      a.scheduledLabel = Konnektis.scheduledLabelString(a, true);
      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate, true);
      }

      a.owner = a.carerUsername === Meteor.user().username || a.completedUsername === Meteor.user().username;
      a.carerUsername = a.carerUsername ||  a.completedUsername;
    }

    return tasks;
  },
  appointments: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { carerUsername: Meteor.user().username }
      ]
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var toReturn = [];
    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];
      a.name = Konnektis.getAppointmentTypeLabel(a.name);
      a.state = Konnektis.stateString(a);
      a.stateLabel = Konnektis.stateLabelString(a);
      a.scheduledLabel = Konnektis.scheduledLabelString(a);
      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      var medications = Medications.find({ appointmentId: a._id });
      a.medicationCount = medications.count();

      var tasks = Tasks.find({ appointmentId: a._id });
      a.taskCount = tasks.count();

      if(a.recurringAppointmentId) {
        var recurringMedications = RecurringMedications.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.medicationCount = Math.max(a.medicationCount, recurringMedications.count());

        var recurringTasks = RecurringTasks.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.taskCount = Math.max(a.taskCount, recurringTasks.count());
      }

      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate);
      }

      toReturn.push(a);
    }

    var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.currentDate, [{ carerUsername: Meteor.user().username }]);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      occurrence.name = Konnektis.getAppointmentTypeLabel(occurrence.name);
      occurrence.state = 'scheduled';
      occurrence.stateLabel = 'Scheduled';
      occurrence.scheduledStartDay = moment(this.currentDate).toDate();
      occurrence.scheduledEndDay = moment(this.currentDate).toDate();
      occurrence.scheduledStartTime = occurrence.startTime;
      occurrence.scheduledEndTime = occurrence.endTime;
      occurrence.scheduledLabel = Konnektis.scheduledLabelString(occurrence);

      var recurringMedications = RecurringMedications.find({ recurringAppointmentId: occurrence._id });
      occurrence.medicationCount = recurringMedications.count();

      var recurringTasks = RecurringTasks.find({ recurringAppointmentId: occurrence._id });
      occurrence.taskCount = recurringTasks.count();

      toReturn.push(occurrence);
    }

    return toReturn;
  },
  otherAppointments: function() {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(this.currentDate) } },
        { scheduledEndDay: { $gte: Konnektis.getStartOfDay(this.currentDate) } },
        { carerUsername: { $not: Meteor.user().username  } }
      ]
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var toReturn = [];
    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];
      a.name = Konnektis.getAppointmentTypeLabel(a.name);
      a.carerUsername = a.carerUsername === 'external' ? "Not a System User" : a.carerUsername;
      a.state = Konnektis.stateString(a);
      a.stateLabel = Konnektis.stateLabelString(a);
      a.scheduledLabel = Konnektis.scheduledLabelString(a);
      var comments = Comments.find({ parentId: a._id });

      a.commentCount = comments.count();

      var medications = Medications.find({ appointmentId: a._id });
      a.medicationCount = medications.count();

      var tasks = Tasks.find({ appointmentId: a._id });
      a.taskCount = tasks.count();

      if(a.recurringAppointmentId) {
        var recurringMedications = RecurringMedications.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.medicationCount += recurringMedications.count();

        var recurringTasks = RecurringTasks.find({ recurringAppointmentId: a.recurringAppointmentId });
        a.taskCount += recurringTasks.count();
      }

      if (a.actualEndDate) {
        a.completedLabel = Konnektis.completedLabelString(a, this.currentDate);
      }

      toReturn.push(a);
    }

    var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.currentDate, [{ carerUsername: { $not: Meteor.user().username  } }]);

    for (var i = 0; i < occurrences.length; i++) {
      var occurrence = occurrences[i];

      occurrence.name = Konnektis.getAppointmentTypeLabel(occurrence.name);
      occurrence.carerUsername = occurrence.carerUsername === 'external' ? "Not a System User" : occurrence.carerUsername;
      occurrence.state = 'scheduled';
      occurrence.stateLabel = 'Scheduled';
      occurrence.scheduledStartDay = moment(this.currentDate).toDate();
      occurrence.scheduledEndDay = moment(this.currentDate).toDate();
      occurrence.scheduledStartTime = occurrence.startTime;
      occurrence.scheduledEndTime = occurrence.endTime;
      occurrence.scheduledLabel = Konnektis.scheduledLabelString(occurrence);

      var recurringMedications = RecurringMedications.find({ recurringAppointmentId: occurrence._id });
      occurrence.medicationCount = recurringMedications.count();

      var recurringTasks = RecurringTasks.find({ recurringAppointmentId: occurrence._id });
      occurrence.taskCount = recurringTasks.count();

      toReturn.push(occurrence);
    }

    return toReturn;
  },
  issues: function() {
    var issues = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]
          }
        ]
    }).fetch();

    for (var i = 0; i < issues.length; i++) {
      var a = issues[i];
      if (a.timestamp) {
        a.timestamp = Konnektis.formatNiceDateTime(a.timestamp);
      }

      var id = a.parentId;
      if (id) {
        var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
        if (appointment) {
          if (appointment.patientId !== Konnektis.patientId()) {
            Router.go('selectPatientView');
          }

          a.appointmentTitle = appointment.name.toProperCase();
        } else {
          var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
          if (medication && (medication.patientId !== Konnektis.patientId())) {
            Router.go('selectPatientView');
          }

          if (medication) {
            a.medicationTitle = medication.name;

            if(medication.appointmentId) {
              var medicationAppointment = Appointments.findOne(medication.appointmentId) || RecurringAppointments.findOne(medication.appointmentId);
              if (medicationAppointment) {
                if (medicationAppointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = medicationAppointment.name.toProperCase();
              }
            }
          }

          var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
          if (task && (task.patientId !== Konnektis.patientId())) {
            Router.go('selectPatientView');
          }

          if (task) {
            a.taskTitle = task.name;

            if(task.appointmentId) {
              var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
              if (taskAppointment) {
                if (taskAppointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = taskAppointment.name.toProperCase();
              }
            }
          }
        }
      }
    }

    return issues;
  },
  numberOfComments: function() {
    var comments = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } }
          ]
          }
        ]
    });

    return comments.count();
  },
  numberOfIssues: function() {
    var comments = Comments.find({
      $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.currentDate)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.currentDate)) } },
            { urgent: true }
          ]
          }
        ]
    });

    return comments.count();
  }
});
