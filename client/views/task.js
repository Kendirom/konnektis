'use strict';

var repeat = function(id) {
  var repeat = Session.get('repeat');

  if (repeat !== undefined) {
    return repeat;
  }

  task = RecurringTasks.findOne(id);
  if (task) {
    return true;
  }

  var task = Tasks.findOne(id);
  if (task) {

    if (task.recurringTaskId) {
      return true;
    } else {
      return false;
    }
  }

  return false;
};

var isConcrete = function(id) {
  var task = Tasks.findOne(id);

  if (task) {
    return true;
  } else {
    return false;
  }
}

function createTask(form, date) {
  function create (appointmentId) {
    var task = {
      patientId: Konnektis.patientId(),
      appointmentId: appointmentId,
      recurringAppointmentId: null,
      name: form.name.value,
      description: form.description.value,
      scheduledStartDay: moment.utc(form.scheduledStartDay.value).toDate(),
      scheduledStartTime: form.scheduledStartTime.value,
      scheduledStartTimeType: form.scheduledStartTimeType.value,
      scheduledEndDay: moment.utc(form.scheduledEndDay.value).toDate(),
      scheduledEndTime: form.scheduledEndTime.value,
      scheduledEndTimeType: form.scheduledEndTimeType.value,
      isEvent: false,
      carerUsername: form.carerUsername.value,
    };
    Konnektis.callMethodIfConnected("createTask", task, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        history.go(-1);
      }
    });
  }

  if (form.appointmentId.value) {
    Konnektis.runForAppointmentOrConcreteIfRecurring(
      form.appointmentId.value,
      moment.utc(form.scheduledStartDay.value).toDate(),
      function() {
       create(form.appointmentId.value);
      },
      function(id) {
       create(id);
      });
  } else {
   create(null);
  }
}

function setDefaultValuesFromRecurringAppointment(form, recurringAppointment) {
  if (Session.get('repeat') === false) {
    return;
  }

  function outsideOfBoundsOfAppointment(taskTime, appointmentStartTime, appointmentEndTime) {
    return taskTime < appointmentStartTime
      || taskTime > appointmentEndTime;
  }

  function normaliseStartTime(appointmentStartTime, appointmentEndTime) {
    if (!form.startTime.value
      || outsideOfBoundsOfAppointment(form.startTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentStartTime) {
        form.startTime.value = appointmentStartTime;
      }
    }
  }

  function normaliseEndTime(appointmentStartTime, appointmentEndTime) {
    if (!form.endTime.value
      || outsideOfBoundsOfAppointment(form.endTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentEndTime) {
        form.endTime.value = appointmentEndTime;
      }
    }
  }

  if (recurringAppointment) {
    form.recurrenceStartDay.value = Konnektis.formatDateForLink(recurringAppointment.recurrenceStartDay);
    form.recurrenceEndDay.value = Konnektis.formatDateForLink(recurringAppointment.recurrenceEndDay);
    form.carerUsername.value = recurringAppointment.carerUsername;
    normaliseStartTime(recurringAppointment.startTime, recurringAppointment.endTime);
    normaliseEndTime(recurringAppointment.startTime, recurringAppointment.endTime);
  } else {
    form.carerUsername.value = "";
  }
}

function setDefaultValuesFromAppointment(form, appointment, recurringAppointment) {
  if (Session.get('repeat') === true) {
    return;
  }

  function outsideOfBoundsOfAppointment(taskTime, appointmentStartTime, appointmentEndTime) {
    return taskTime < appointmentStartTime
      || taskTime > appointmentEndTime;
  }

  function normaliseStartTime(appointmentStartTime, appointmentEndTime) {
    if (!form.scheduledStartTime.value
      || outsideOfBoundsOfAppointment(form.scheduledStartTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentStartTime) {
        form.scheduledStartTime.value = appointmentStartTime;
      }
    }
  }

  function normaliseEndTime(appointmentStartTime, appointmentEndTime) {
    if (!form.scheduledEndTime.value
      || outsideOfBoundsOfAppointment(form.scheduledEndTime.value, appointmentStartTime, appointmentEndTime)) {
      if (appointmentEndTime) {
        form.scheduledEndTime.value = appointmentEndTime;
      }
    }
  }

  if (appointment || recurringAppointment) {
    Session.set('displayTimeTypeDropDowns', false);
    form.scheduledStartTimeType.value = 'specific';
    form.scheduledEndTimeType.value = 'specific';
    Session.set('displaySpecificStartTimeInput', true);
    Session.set('displaySpecificEndTimeInput', true);

    var date = Konnektis.formatDateForLink(moment.utc(Template.parentData().date).toDate());

    // Timeout ensures that meteor has time to re-run the reactive view
    // logic based on the above changes before running the function within.
    setTimeout(function() {
      if (appointment) {
        form.scheduledStartDay.value = Konnektis.formatDateForLink(appointment.scheduledStartDay);
        form.scheduledEndDay.value = Konnektis.formatDateForLink(appointment.scheduledEndDay);
        form.carerUsername.value = appointment.carerUsername;
        normaliseStartTime(appointment.scheduledStartTime, appointment.scheduledEndTime);
        normaliseEndTime(appointment.scheduledStartTime, appointment.scheduledEndTime);
      } else {
        form.scheduledStartDay.value = date;
        form.scheduledEndDay.value = date;
        form.carerUsername.value = recurringAppointment.carerUsername;
        normaliseStartTime(recurringAppointment.startTime, recurringAppointment.endTime);
        normaliseEndTime(recurringAppointment.startTime, recurringAppointment.endTime);
      }
    }, 0);
  } else {
    Session.set('displayTimeTypeDropDowns', true);
    form.carerUsername.value = "";
  }
}

Template.taskRepeatOption.helpers({
  repeat: function() {
    return repeat(this.id);
  }
});

Template.taskRepeatOption.events({
  'change .repeat': function(event) {
    Session.set('repeat', event.target.checked);
  }
});

Template.existingTaskView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
  isConcrete: function() {
    return isConcrete(this.id);
  }
});

Template.newTaskView.helpers({
  repeat: function() {
    return repeat(this.id);
  },
});

Template.taskView.helpers({
  id: function() {
    return this.id;
  }
});

Template.taskForm.events({
  'blur #start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-field');
      var endField = $('#end-time-field');
      if (!endField) {
        console.log('Could not find the end time field, unable to auto set it\'s value from the start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #start-time-dropdown-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-dropdown-field');
      var endField = $('#end-time-dropdown-field');
      if (!endField) {
        console.log('Could not find the end time dropdown field, unable to auto set it\'s value from the start time dropdown field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #start-time-type-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#start-time-type-field');
      var endField = $('#end-time-type-field');
      if (!endField) {
        console.log('Could not find the end time type field, unable to auto set it\'s value from the start time type field');
        return;
      }

      endField.val(startField.val());
      Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
    }
    Session.set('displaySpecificStartTimeInput', event.target.value === 'specific');
  },
  'change #end-time-type-field': function(e) {
    Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
  },
  'blur #start-day-field': function(e) {
    var startField = $('#start-day-field');
    var endField = $('#end-day-field');
    if (!endField) {
      console.log('Could not find the end day field, unable to auto set it\'s value from the start day field');
      return;
    }

    endField.val(startField.val());
  },
  'change .appointment-id': function(event) {
    var form = event.target.closest('form');
    var appointment = Appointments.findOne(form.appointmentId.value);
    var recurringAppointment = RecurringAppointments.findOne(form.appointmentId.value);

    setDefaultValuesFromAppointment(form, appointment, recurringAppointment);
  }
});

Template.taskForm.helpers({
  displaySpecificStartTimeInput: function() {
    return Session.get('displaySpecificStartTimeInput');
  },
  displaySpecificEndTimeInput: function() {
    return Session.get('displaySpecificEndTimeInput');
  },
  displayConcreteDates: function() {
    return !repeat(this.id) || isConcrete(this.id)
  },
  displayConcreteTimes: function() {
    return !repeat(this.id);
  },
  displayConcreteAppointment: function() {
    return !repeat(this.id);
  },
  displayTimeTypeDropDowns: function() {
    return Session.get('displayTimeTypeDropDowns');
  },
  task: function() {
    var task = Tasks.findOne(this.id);

    if (!task) {
      task = RecurringTasks.findOne(this.id);

      if (task) {
        task.scheduledStartDay = Konnektis.getDateFromUtc(this.date);
        task.scheduledEndDay = Konnektis.getDateFromUtc(this.date);
        // Because the date picker deals in UTC
        task.scheduledStartTime = Konnektis.getDateFromUtc(task.scheduledStartTime);
        task.scheduledEndTime = Konnektis.getDateFromUtc(task.scheduledEndTime);
      } else {
        task = {
          scheduledStartDay: Konnektis.getDateFromUtc(this.date),
          scheduledEndDay: Konnektis.getDateFromUtc(this.date),
          scheduledStartTimeType: 'specific',
          scheduledEndTimeType: 'specific'
        };

        if (this.appointmentId) {
          var appointment = Appointments.findOne(this.appointmentId);
          var recurringAppointment = RecurringAppointments.findOne(this.appointmentId);

          if (appointment) {
            task.appointmentId = appointment._id;
          } else if (recurringAppointment) {
            task.recurringAppointmentId = recurringAppointment._id;

            Session.setDefault('repeat', true);
          }
        }
      }
    }

    // Because the date picker deals in UTC
    task.scheduledStartDay = Konnektis.getDateFromUtc(task.scheduledStartDay);
    task.scheduledEndDay = Konnektis.getDateFromUtc(task.scheduledEndDay);

    Session.setDefault('displaySpecificStartTimeInput', !task.scheduledStartTimeType || task.scheduledStartTimeType === 'specific');
    Session.setDefault('displaySpecificEndTimeInput', !task.scheduledEndTimeType || task.scheduledEndTimeType === 'specific');
    if (task.recurringAppointmentId) {
      Session.setDefault('perAppointment', true);
    }

    return task;
  },
  appointmentOptions: function() {
    var queries = [{ patientId: Konnektis.patientId() }];

    if (this.date) {
      queries.push({ scheduledStartDay: { $lte: moment.utc(this.date).endOf('day').toDate() } });
      queries.push({ scheduledEndDay: { $gte: moment.utc(this.date).startOf('day').toDate() } });
    }

    if (this.appointmentId) {
      queries.push({ _id: this.appointmentId });
    }

    var appointments = Appointments.find({ $and: queries }).fetch();

    var options = [];

    options.push({ label: "General responsibility", value: ""});

    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      options.push({
        label: Konnektis.formatDate(a.scheduledStartDay) + ' ' + (a.scheduledStartTime ? a.scheduledStartTime : '')
          + ' - '
          + Konnektis.formatDate(a.scheduledEndDay) + ' ' + (a.scheduledEndTime ? a.scheduledEndTime : '')
          + ' - '
          + (a.carerUsername ? ' ' + a.carerUsername : '')
          + ' - ' + Konnektis.getAppointmentTypeLabel(a.name),
        value: a._id
      });
    }

    if (this.date) {
      var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.date);

      for (var i = 0; i < occurrences.length; i++) {
        var occurrence = occurrences[i];

        options.push({
          label: (occurrence.startTime ? occurrence.startTime : '')
          + (occurrence.endTime ? ' - ' + occurrence.endTime : '')
          + (occurrence.carerUsername ? ' ' + occurrence.carerUsername : '')
          + ' - ' + Konnektis.getAppointmentTypeLabel(occurrence.name),
          value: occurrence._id
        });
      }
    }

    return options;
  },
  carers: function() {
    var carers = Carers.find({ patients: Konnektis.patientId() }).fetch();

    return carers.map(function(value) {
     return { value: value.username, label: value.username };
    });
  }
});

Template.recurringTaskForm.events({
  'change #recurrenceUnit': function(event) {
    Session.set('perAppointment', event.target.value === 'appointment')
  },
  'change .recurring-appointment-id': function(event) {
    var form = event.target.closest('form');
    var recurringAppointment = RecurringAppointments.findOne(form.recurringAppointmentId.value);

    setDefaultValuesFromRecurringAppointment(form, recurringAppointment);
  },
  'blur #recurring-start-time-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-field');
      var endField = $('#recurring-end-time-field');
      if (!endField) {
        console.log('Could not find the recurring end time field, unable to auto set it\'s value from the recurring start time field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #recurring-start-time-dropdown-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-dropdown-field');
      var endField = $('#recurring-end-time-dropdown-field');
      if (!endField) {
        console.log('Could not find the recurring end time dropdown field, unable to auto set it\'s value from the recurring start time dropdown field');
        return;
      }

      endField.val(startField.val());
    }
  },
  'change #recurring-start-time-type-field': function(e) {
    var id = Template.parentData().id;
    if (!id || (Tasks.findOne(id) && !Tasks.findOne(id).scheduledEndTime)) {
      var startField = $('#recurring-start-time-type-field');
      var endField = $('#recurring-end-time-type-field');
      if (!endField) {
        console.log('Could not find the recurring end time type field, unable to auto set it\'s value from the recurring start time type field');
        return;
      }

      endField.val(startField.val());
      Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
    }
    Session.set('displaySpecificStartTimeInput', event.target.value === 'specific');
  },
  'change #recurring-end-time-type-field': function(e) {
    Session.set('displaySpecificEndTimeInput', event.target.value === 'specific');
  },
  'blur #recurring-start-day-field': function(e) {
    var startField = $('#recurring-start-day-field');
    var endField = $('#recurring-end-day-field');
    if (!endField) {
      console.log('Could not find the recurring end day field, unable to auto set it\'s value from the recurring start day field');
      return;
    }

    endField.val(startField.val());
  }
});

Template.taskForm.onRendered(function() {
  var appointmentId = Template.parentData().appointmentId;
  var id = Template.parentData().id;

  var form = document.getElementsByClassName('taskForm')[0];

  var task;
  if (id) {
    task = Tasks.findOne(id);
  }

  if (task) {
    if (task.appointmentId) {
      Session.set('displayTimeTypeDropDowns', false);
      if (!task.recurringTaskId) {
        form.scheduledStartTimeType.value = 'specific';
        form.scheduledEndTimeType.value = 'specific';
      }
    } else {
      Session.set('displayTimeTypeDropDowns', true);
    }
  } else if (appointmentId) {
    var appointment = Appointments.findOne(appointmentId);
    var recurringAppointment = RecurringAppointments.findOne(appointmentId);

    setDefaultValuesFromAppointment(form, appointment, recurringAppointment);
    setDefaultValuesFromRecurringAppointment(form, recurringAppointment);
  } else {
    setDefaultValuesFromAppointment(form, null, null);
    setDefaultValuesFromRecurringAppointment(form, null);
  }
});

Template.recurringTaskForm.helpers({
  displaySpecificStartTimeInput: function() {
    return Session.get('displaySpecificStartTimeInput');
  },
  displaySpecificEndTimeInput: function() {
    return Session.get('displaySpecificEndTimeInput');
  },
  perAppointment: function() {
    return Session.get('perAppointment');
  },
  recurringAppointmentOptions: function() {
    var options = [];

    if (this.date) {
      var occurrences = Konnektis.getRecurringAppointmentOccurrencesForDate(this.date, null, true);

      for (var i = 0; i < occurrences.length; i++) {
        var occurrence = occurrences[i];

        options.push({
          label: Konnektis.formatDate(this.date)
            + ' - ' + occurrence.startTime
            + (occurrence.carerUsername ? ' - ' + occurrence.carerUsername : '')
            + ' - ' + Konnektis.getAppointmentTypeLabel(occurrence.name),
          value: occurrence._id
        });
      }
    }

    return options;
  },
  recurringTask: function() {
console.log('recurringTask');
    var task = Tasks.findOne(this.id);

    var recurringTask;
    if (task) {
console.log('recurringTask found task');
      recurringTask = RecurringTasks.findOne(task.recurringTaskId);
console.log(recurringTask);
    }

    if (!recurringTask) {
console.log('recurringTask not found recurring task');
      recurringTask = RecurringTasks.findOne(this.id);
console.log(recurringTask);
    }

    if (recurringTask) {
console.log('recurringTask found recurring task');
      // Because the date picker deals in UTC
      recurringTask.recurrenceStartDay = Konnektis.getDateFromUtc(recurringTask.recurrenceStartDay);
      recurringTask.recurrenceEndDay = Konnektis.getDateFromUtc(recurringTask.recurrenceEndDay);
console.log(recurringTask);
    } else {
console.log('recurringTask not found recurring task at all');

      recurringTask = {
        recurrenceStartDay: Konnektis.getDateFromUtc(this.date),
        recurrenceEndDay: Konnektis.getDateFromUtc(this.date),
        recurrenceUnit: 'day',
        startTimeType: task ? task.scheduledStartTimeType : 'specific',
        endTimeType: task ? task.scheduledEndTimeType : 'specific',
        startTime: task ? task.scheduledStartTime : null,
        endTime: task ? task.scheduledEndTime : null
      };
console.log(recurringTask);

      if (this.appointmentId) {
console.log('recurringTask got appointment id:' + this.appointmentId);
        var recurringAppointment = RecurringAppointments.findOne(this.appointmentId);

        if (recurringAppointment) {
console.log('recurringTask got recurring appointment');
          recurringTask.carerUsername = recurringAppointment.carerUsername;
          recurringTask.recurringAppointmentId = recurringAppointment._id;
          recurringTask.recurrenceUnit = 'appointment';
          recurringTask.startTimeType = 'specific';
          recurringTask.endTimeType = 'specific';
          recurringTask.startTime = recurringAppointment.startTime;
          recurringTask.endTime = recurringAppointment.endTime;
console.log(recurringTask);
        }
      }
    }

    Session.setDefault('displaySpecificStartTimeInput', !recurringTask || !recurringTask.startTimeType || recurringTask.startTimeType === 'specific');
    Session.setDefault('displaySpecificEndTimeInput', !recurringTask || !recurringTask.endTimeType || recurringTask.endTimeType === 'specific');
    Session.setDefault('perAppointment', recurringTask && recurringTask.recurrenceUnit && recurringTask.recurrenceUnit === 'appointment');

console.log(recurringTask);
    return recurringTask;
  }
});

Template.taskView.events({
  'submit #taskCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    createTask(form, Template.parentData().date);

    return false;
  },
  'submit #recurringTaskCreationForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var parentData = Template.parentData();

    var recurringTask = {
      patientId: Konnektis.patientId(),
      name: form.name.value,
      description: form.description.value,
      carerUsername: form.carerUsername.value,
      recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
      recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
      startTimeType: form.startTimeType.value,
      endTimeType: form.endTimeType.value,
      startTime: form.startTime.value,
      endTime: form.endTime.value,
      recurrenceUnit: form.recurrenceUnit.value
    };

    if (recurringTask.recurrenceUnit === 'appointment') {
      recurringTask.recurringAppointmentId = form.recurringAppointmentId.value;
    }

    var date = Template.parentData().date;
    Konnektis.callMethodIfConnected("createRecurringTask", recurringTask, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        history.go(-1);
      }
    });

    return false;
  }
});

Template.existingTaskView.events({
  // Update an task without the repeat box checked
  'submit #updateTaskForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);
    var task = Tasks.findOne(Router.current().data().id);

    if (task) {
      // if task exists already it's either already non recurring or is concrete AND we are canceling recurrence
      // if already non recurring save without hassle
      // if was recurring confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end date of recurrence to the last occurrence and set the recurring app id of the app to null

      if (task.recurringTaskId) {
        bootbox.confirm("Warning: Removing recurrence from the task will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            var recurringTask = RecurringTasks.findOne(task.recurringTaskId);

            if (!recurringTask) {
              // throw
            }

            Konnektis.callMethodIfConnected("endRecurringTaskPreviousDay", recurringTask, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                task.recurringTaskId = null;
                task.name = form.name.value;
                task.description = form.description.value;
                task.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
                task.scheduledStartTimeType = form.scheduledStartTimeType.value;
                task.scheduledStartTime = form.scheduledStartTime.value;
                task.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
                task.scheduledEndTimeType = form.scheduledEndTimeType.value;
                task.scheduledEndTime = form.scheduledEndTime.value;
                task.carerUsername = form.carerUsername.value;

                if (form.appointmentId) {
                  task.appointmentId = form.appointmentId.value;
                }

                Konnektis.callMethodIfConnected("updateTask", task, function(error, result) {
                  if (error) {
                    Errors.throw(error.message);
                  } else {
                    Konnektis.removeAllConcreteTaskOccurrencesFromDate(
                      recurringTask._id,
                      dateFromRouteData,
                      function(a) {
                        return a._id !== task._id;
                      });

                    history.go(-1);
                  }
                });
              }
            });
          }
        });
      } else {
        task.recurringTaskId = null;
        task.name = form.name.value;
        task.description = form.description.value;
        task.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
        task.scheduledStartTimeType = form.scheduledStartTimeType.value;
        task.scheduledStartTime = form.scheduledStartTime.value;
        task.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
        task.scheduledEndTimeType = form.scheduledEndTimeType.value;
        task.scheduledEndTime = form.scheduledEndTime.value;
        task.carerUsername = form.carerUsername.value;

        if (form.appointmentId) {
          task.appointmentId = form.appointmentId.value;
        }

        Konnektis.callMethodIfConnected("updateTask", task, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            history.go(-1);
          }
        });
      }
    } else {
      // if task doesn't exist it was recurring and needs to be concreting; also we are canceling recurrence
      // confirm with user that we want to cancel recurring app here and going forwards
      // if confirmed, change end of recurrence to the last occurrence and create a concrete app without recurring app id

      var recurringTask = RecurringTasks.findOne(Router.current().data().id);

      if (recurringTask) {
        bootbox.confirm("Warning: Removing recurrence from the task will stop all future occurrences. All past occurrences will remain untouched. Are you sure you wish to continue?", function(result) {
          if (result === true) {
            Konnektis.callMethodIfConnected("endRecurringTaskPreviousDay", recurringTask, dateFromRouteData, function(error, result) {
              if (error) {
                Errors.throw(error.message);
              } else {
                Konnektis.removeAllConcreteTaskOccurrencesFromDate(
                  recurringTask._id,
                  dateFromRouteData);

                createTask(form, dateFromRouteData);
              }
            });
          }
        });
      } else {
        // throw
      }
    }

    return false;
  },
  'submit #updateRecurringTaskForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var recurringTask = RecurringTasks.findOne(Router.current().data().id);

    if (!recurringTask) {
      // throw
    }

    // is not concrete so just assume recurring already
    recurringTask.name = form.name.value;
    recurringTask.description = form.description.value;
    recurringTask.carerUsername = form.carerUsername.value;
    recurringTask.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
    recurringTask.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
    recurringTask.startTimeType = form.startTimeType.value;
    recurringTask.endTimeType = form.endTimeType.value;
    recurringTask.startTime = form.startTime.value;
    recurringTask.endTime = form.endTime.value;
    recurringTask.recurrenceUnit = form.recurrenceUnit.value;

    if (recurringTask.recurrenceUnit === 'appointment') {
      recurringTask.recurringAppointmentId = form.recurringAppointmentId.value;
    }

    Konnektis.callMethodIfConnected("updateRecurringTask", recurringTask, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        Konnektis.updateConcreteTasksWithRecurringData(recurringTask);

        history.go(-1);
      }
    });

    return false;
  },
  'submit #updateTaskWithRecurrenceForm': function(e) {
    e.preventDefault();

    var form = e.target;

    var dateFromRouteData = Konnektis.getDateFromUtc(Router.current().data().date);

    var task = Tasks.findOne(Router.current().data().id);

    var recurringTask;
    if (task) {
      var recurringTask = RecurringTasks.findOne(task.recurringTaskId);
    } else {
      var recurringTask = RecurringTasks.findOne(Router.current().data().id);
    }

    if (recurringTask) {
      // if recurring exists already it's already recurring so we should save
      // we also need to update the concrete

      recurringTask.name = form.name.value;
      recurringTask.description = form.description.value;
      recurringTask.carerUsername = form.carerUsername.value;
      recurringTask.recurrenceStartDay = moment.utc(form.recurrenceStartDay.value).toDate();
      recurringTask.recurrenceEndDay = moment.utc(form.recurrenceEndDay.value).toDate();
      recurringTask.startTimeType = form.startTimeType.value;
      recurringTask.endTimeType = form.endTimeType.value;
      recurringTask.startTime = form.startTime.value;
      recurringTask.endTime = form.endTime.value;
      recurringTask.recurrenceUnit = form.recurrenceUnit.value;

      if (recurringTask.recurrenceUnit === 'appointment') {
        recurringTask.recurringAppointmentId = form.recurringAppointmentId.value;
      }

      Konnektis.callMethodIfConnected("updateRecurringTask", recurringTask, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var task = Tasks.findOne(Router.current().data().id);

          if (!task) {
            // throw
          }

          task.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          task.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();

          Konnektis.callMethodIfConnected("updateTask", task, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              Konnektis.updateConcreteTasksWithRecurringData(recurringTask);

              history.go(-1);
            }
          });
        }
      });

    } else {
      // if recurring doesn't exist it was concrete and needs to be recurring
      // create recurring and set recurring app id on concrete; and update other fields

      var recurringTask = {
        patientId: Konnektis.patientId(),
        name: form.name.value,
        description: form.description.value,
        carerUsername: form.carerUsername.value,
        recurrenceStartDay: moment.utc(form.recurrenceStartDay.value).toDate(),
        recurrenceEndDay: moment.utc(form.recurrenceEndDay.value).toDate(),
        startTimeType: form.startTimeType.value,
        endTimeType: form.endTimeType.value,
        startTime: form.startTime.value,
        endTime: form.endTime.value,
        recurrenceUnit: form.recurrenceUnit.value
      };

      if (recurringTask.recurrenceUnit === 'appointment') {
        recurringTask.recurringAppointmentId = form.recurringAppointmentId.value;
      }

      Konnektis.callMethodIfConnected("createRecurringTask", recurringTask, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          var task = Tasks.findOne(Router.current().data().id);

          if (!task) {
            // throw
          }

          task.recurringTaskId = result;
          task.name = form.name.value;
          task.description = form.description.value;
          task.scheduledStartDay = moment.utc(form.scheduledStartDay.value).toDate();
          task.scheduledStartTimeType = form.startTimeType.value;
          task.scheduledStartTime = form.startTime.value;
          task.scheduledEndDay = moment.utc(form.scheduledEndDay.value).toDate();
          task.scheduledEndTimeType = form.endTimeType.value;
          task.scheduledEndTime = form.endTime.value;
          task.carerUsername = form.carerUsername.value;

          if (form.appointmentId) {
            task.appointmentId = form.appointmentId.value;
          }

          Konnektis.callMethodIfConnected("updateTask", task, function(error, result) {
            if (error) {
              Errors.throw(error.message);
            } else {
              history.go(-1);
            }
          });
        }
      });
    }

    return false;
  }
});
