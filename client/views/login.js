'use strict';

/*
  client/lib/views/login.js
*/

Template.login.onRendered(function() {
  Konnektis.addBodyClass('login');
  // $('#login-username').focus();
});

Template.login.events({
  'click .btn-cancelLogin': function(event) {
    event.stopPropagation();

    var routerController = Router.current();
    var currentDate = SessionAmplify.get("walkupCurrentDate");
    var params = {};

    SessionAmplify.set("walkupAppointment", null);
    SessionAmplify.set("walkupCurrentDate", null);

    routerController.layout('layout');
    // routerController.render();

    if(currentDate) {
      params['_currentDate'] = Konnektis.formatDateForLink(currentDate);
    }
// console.log('cancelling login, going to walkupview');
    Router.go('walkUpView', params);
  }
});

Template.login.helpers({
  signingInToAppointment: function() {
    var appointmentSummary = SessionAmplify.get("loggingInToAppointmentSummary");
    return (appointmentSummary ? true : false);
  },
  appointmentSummary: function() {
    return SessionAmplify.get("loggingInToAppointmentSummary");
  },
  appVersion: function() {
    return Konnektis.appVersion();
  },
});
