'use strict';

Template.commentsView.events({
  'keyup .comment-text-input': function(e) {
    e.preventDefault();

    var input = e.target;

    var id = Template.parentData().id;

    var currentAppointment = SessionAmplify.get('currentAppointment');

    if (!currentAppointment || id !== currentAppointment) {
      return;
    }

    var commentSessionKey = Konnektis.getAppointmentCommentTextSessionKey(id);

    SessionAmplify.set(commentSessionKey, input.value);
  },
  'submit #comment-creation-form': function(e) {
    e.preventDefault();

    var form = e.target;

    var data = Template.parentData();
    var id = data.id;
    var date = data.date;

    function createComment(id, parentType) {
      var comment = {
        patientId: Konnektis.patientId(),
        parentType: parentType,
        parentId: id,
        timestamp: new Date(),
        username: Meteor.user().username,
        userType: Konnektis.isFamily() ? "Family" : "Carer",
        commentText: form.commentText.value,
        urgent: form.urgent.checked
      };

      Konnektis.callMethodIfConnected("createComment", comment, function(error, result) {
        if (error) {
          Errors.throw(error.message);
        } else {
          form.commentText.value = '';
          //Router.go('/comments/' + id + '?date=' + Konnektis.formatDateForLink(date));
          history.go(-1);
        }
      });
    }

    var appointment = Appointments.findOne(id);
    var recurringAppointment = RecurringAppointments.findOne(id);

    if (appointment || recurringAppointment) {
      var parentType = 'appointment';

      Konnektis.runForAppointmentOrConcreteIfRecurring(
        id,
        date,
        function() {
          createComment(id, parentType);
        },
        function(newId) {
          createComment(newId, parentType);
        });
    } else {
      var task = Tasks.findOne(id);
      var recurringTask = RecurringTasks.findOne(id);

      if (task || recurringTask) {
        var parentType = 'task';

        Konnektis.runForTaskOrConcreteIfRecurring(
          id,
          date,
          function() {
            createComment(id, parentType);
          },
          function(newId) {
            createComment(newId, parentType);
          });
      }
    }

    return false;
  }
});

Template.details.helpers({
  patientName: function() {
    return Konnektis.patientName();
  },
  parent: function() {

    var details = { type : "" , name: "" , description: ""};

    var id = Template.parentData().id;
    if (id) {
      var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
      if (appointment) {
        if (appointment.patientId !== Konnektis.patientId()) {
          Router.go('selectPatientView');
        }

        details.type = 'Appointment';
        details.name = Konnektis.getAppointmentTypeLabel(appointment.name);
        details.description = appointment.description;
        details.id = appointment._id;

        return details;
      }
      var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
      if(task) {
        if (task.patientId !== Konnektis.patientId()) {
          Router.go('selectPatientView');
        }

        details.type = 'Task';
        details.name = task.name;
        details.description = task.description;
        details.id = task._id;

        return details;
      }
      var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
      if(medication) {
        if (medication.patientId !== Konnektis.patientId()) {
          Router.go('selectPatientView');
        }

        details.type = 'Medication';
        details.name = medication.name;
        details.description = medication.description;
        details.id = medication._id;

        return details;
      }
    }
    else {
      details.type = 'Daily';
      details.name = Konnektis.formatNiceDate(this.date);
    }

    return details;
  },
  isFamily: function() {
    return Konnektis.isFamily();
  },
  dateQueryParameters: function() {
    return Konnektis.getDateQueryParameters(this.date);
  }
});

Template.commentLink.helpers({
  dateQueryParameters: function() {
    return Konnektis.getDateQueryParameters(Template.parentData().date);
  }
});

Template.commentsView.helpers({
  id: function() {
    return this.id;
  },
  general: function() {

    var details;

    if(this.id) {
      var appointment = Appointments.findOne(id);
      if (appointment) {
        details.parentType = 'Appointment';
        details.title = appointment.name;
      } else {
        var task = Tasks.findOne(id);
        if (task) {
          details.parentType = 'Task';
          details.title = task.name;
        } else {
          var medication = Medications.findOne(id);
          if (medication) {
            details.parentType = 'Medication';
            details.title = medication.name;
          }
        }
      }

    }
    else {
      details.parentType = 'Daily';
      details.title = this.date;
    }

    return details;
  },
  commentText: function() {
    if (this.id) {
      var commentSessionKey = Konnektis.getAppointmentCommentTextSessionKey(this.id);

      return SessionAmplify.get(commentSessionKey);
    }
  },
  comments: function() {
    if(this.id) {
      var comments = Comments.find({
        $and: [
          { patientId: Konnektis.patientId() },
          { parentId: this.id }
        ]
      }).fetch();


    for (var i = 0; i < comments.length; i++) {
      var a = comments[i];
      if (a.timestamp) {
        a.timestamp = Konnektis.formatNiceDateTime(a.timestamp);
        a.owner = Meteor.user().username === a.username;
      }

      var id = a.parentId;
        if (id) {
          switch(a.parentType) {
            case 'appointment':
              var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
              if (appointment) {
                if (appointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = appointment.name.toProperCase();
              }
              break;
            case 'task':
              var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
              if (task) {
                if (task.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.taskTitle = task.name;

                if(task.appointmentId) {
                  var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
                  if (taskAppointment) {
                    if (taskAppointment.patientId !== Konnektis.patientId()) {
                      Router.go('selectPatientView');
                    }

                    a.appointmentTitle = taskAppointment.name.toProperCase();
                  }
                }
              }
              break;
            case 'medication':
              var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
              if (medication) {
                if (medication.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.taskTitle = medication.name;

                if(medication.appointmentId) {
                  var medicationAppointment = Appointments.findOne(medication.appointmentId) || RecurringAppointments.findOne(medication.appointmentId);
                  if (medicationAppointment) {
                    if (medicationAppointment.patientId !== Konnektis.patientId()) {
                      Router.go('selectPatientView');
                    }

                    a.appointmentTitle = medicationAppointment.name.toProperCase();
                  }
                }
              }
              break;
          }
        }
    }

    return comments;
    }
    return null;
  },
  commentsDay: function() {
    if(!this.id) {
      var comments = Comments.find({
        $or: [{
          $and: [
            { patientId: Konnektis.patientId() },
            { timestamp: { $gte: Konnektis.getStartOfDay(new Date(this.date)) } },
            { timestamp: { $lte: Konnektis.getEndOfDay(new Date(this.date)) } }
          ]},
          {
            $and: [
            { patientId: Konnektis.patientId() },
            { relevantDate: { $gte: Konnektis.getStartOfDay(new Date(this.date)) } },
            { relevantDate: { $lte: Konnektis.getEndOfDay(new Date(this.date)) } }
          ]
          }
        ]
      }).fetch();

      for (var i = 0; i < comments.length; i++) {
        var a = comments[i];
        if (a.timestamp) {
          a.timestamp = Konnektis.formatNiceDateTime(a.timestamp);
          a.owner = Meteor.user().username === a.username;
        }

        var id = a.parentId;
        if (id) {
          switch(a.parentType) {
            case('appointment'):
              var appointment = Appointments.findOne(id) || RecurringAppointments.findOne(id);
              if (appointment) {
                if (appointment.patientId !== Konnektis.patientId()) {
                  Router.go('selectPatientView');
                }

                a.appointmentTitle = appointment.name.toProperCase();
              }
              break;
            case('task'):
              var task = Tasks.findOne(id) || RecurringTasks.findOne(id);
              if (task.patientId !== Konnektis.patientId()) {
                Router.go('selectPatientView');
              }

              if (task) {
                a.taskTitle = task.name;

                if(task.appointmentId) {
                  var taskAppointment = Appointments.findOne(task.appointmentId) || RecurringAppointments.findOne(task.appointmentId);
                  if (taskAppointment) {
                    if (taskAppointment.patientId !== Konnektis.patientId()) {
                      Router.go('selectPatientView');
                    }

                    a.appointmentTitle = taskAppointment.name.toProperCase();
                  }
                }
              }
              break;
            case('medication'):
              var medication = Medications.findOne(id) || RecurringMedications.findOne(id);
              if (medication.patientId !== Konnektis.patientId()) {
                Router.go('selectPatientView');
              }

              if (medication) {
                a.taskTitle = medication.name;

                if(medication.appointmentId) {
                  var medicationAppointment = Appointments.findOne(medication.appointmentId) || RecurringAppointments.findOne(medication.appointmentId);
                  if (medicationAppointment) {
                    if (medicationAppointment.patientId !== Konnektis.patientId()) {
                      Router.go('selectPatientView');
                    }

                    a.appointmentTitle = medicationAppointment.name.toProperCase();
                  }
                }
              }
              break;
          }
        }
      }

      return comments;
    }
    return null;
  }
});
