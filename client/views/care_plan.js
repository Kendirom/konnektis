'use strict';

Template.carePlanView.created = function () {
  this.carePlanChanged = true;

  var lastLogin = Konnektis.getCarerPatientLastLogin();

  if(lastLogin) {
    var carerId = Meteor.userId();
    var patientId = Konnektis.patientId();

    this.carePlanChanged = Konnektis.hasPatientInfoChanged(CarePlans, carerId, patientId, lastLogin, Konnektis.patientDocuments.carePlan);
  }
  else {
    // no last login for some reason
  }
};

Template.carePlanView.rendered = function () {
  $('#carePlanChanges').modal({
    backdrop: 'static',
    keyboard: false,
    show: true
  });
  $('#carePlanChanges').off('hidden.bs.modal');
};

Template.carePlanView.helpers({
  carePlanSections: function() {
    return CarePlanSections.find({}, {sort: {displayOrder: 1}}).fetch();
  },
  isFamily: function() {
    return Konnektis.isFamily() && SessionAmplify.get("role") === 'family';
  },
  isCarer: function() {
    return Konnektis.isCarer() && SessionAmplify.get("role") === 'carer';
  },
  currentDate: function() {
    return this.params._currentDate;
  },
  carePlanChanged: function() {
    return Template.instance().carePlanChanged;
  },
  carePlanChanges: function() {
    var patientId = Konnektis.patientId();
    var carePlan = CarePlans.findOne({patientId: patientId});
    return carePlan.carePlanChanges;
  },
});

Template.carePlanTab.helpers({
  isSelected: function() {
    return (SessionAmplify.get('carePlanTab') == this.cssClass);
  }
});

Template.carePlanTab.events({
  'click li': function(e, t) {
    SessionAmplify.set('carePlanTab', $(e.target).attr('konnektis-care-plan-section'));
  },
});

Template.carePlanBody.inheritsHelpersFrom("carePlanTab");

Template.carePlanBody.helpers({
  carePlanSection: function(section) {
    var patientId = Konnektis.patientId();
    var carePlan = CarePlans.findOne({patientId: patientId});

    if(carePlan) {
      return carePlan[section];
    }
    else {
      return null;
    }
  },
});

Template.carePlanChangesView.events({
  'click .btn-cancel': function(event, template) {
    event.preventDefault();

    $('#carePlanChanges').on('hidden.bs.modal', function (e) {
      Router.go('dashboardView');
    })
  },
  'click .btn-save': function(event, template) {
    event.preventDefault();

    var patientId = Konnektis.patientId();
    Konnektis.callMethodIfConnected('setCarerPatientInfoConfirm', patientId, Konnektis.patientDocuments.carePlan, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        // all's fine, nothing to do, login has been recorded
        $('#carePlanChanges').modal('hide');
      }
    });
  },
});
