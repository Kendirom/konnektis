'use strict';

Template.adminClientInfoView.onRendered(function() {
  tinymce.init({
    // selector: '#txt_care_plan',
    selector: 'textarea',
    skin_url: '/packages/teamon_tinymce/skins/lightgray',
  });
});

var findTextEditor = function(id) {
  for (var edId in tinymce.editors) {
    var editor = tinymce.editors[edId];
    if(editor.id == id) {
      return editor;
    }
  }
  return null;
};

var getTextBlocks = function() {
  var documentEditor = findTextEditor('txt_document');
  if(!documentEditor) {
    bootbox.alert('Unable to find document text, cannot save text, please contact support.');
    return false;
  }

  var changesEditor = findTextEditor('txt_changes');
  if(!changesEditor) {
    bootbox.alert('Unable to find changes text entry, cannot save text, please contact support.');
    return false;
  }

  return([documentEditor.getContent(), changesEditor.getContent()]);
}

var adminClientInfoSaveText = function(saveMethod, saveMessage) {
  var textBlocks = getTextBlocks();
  if(!textBlocks) {
    return false;
  }

  Konnektis.callMethodIfConnected(saveMethod, Konnektis.patientId(), textBlocks[0], textBlocks[1], function(error, result) {
    if (error) {
      Errors.throw(error.message);
    } else {
      bootbox.alert(saveMessage);
    }
  });
};

Template.adminClientInfoButton.events({
  'click #save_client_info': function(e, t) {
    adminClientInfoSaveText('setClientInfo', 'Client information has been saved');
  },
});

Template.adminClientInfoView.helpers({
  ClientInfo: function() {
    var patientId = Konnektis.patientId();
    var patientInfo = PatientInfo.findOne({patientId: patientId});
    return patientInfo;
  }
});
