KonnektisNotifications = {};

(function (KonnektisNotifications) {
  'use strict';

  KonnektisNotifications.notifyFamilyMembers = function (patientId, fromUser, title, message) {
    if (Meteor.isServer) {
      var familyMembers = Meteor.users.find({ "roles": { $elemMatch: { $regex: new RegExp('^family\:' + patientId + '$') } } }).fetch();
      familyMembers = _.reject(familyMembers, function(member) {
        return member.username === fromUser;
      });

      _.each(familyMembers, function(user) {
        if (user.profile) {
          if (user.profile.sendSmsNotifications && user.profile.phoneNumber && Meteor.settings.twilio && Meteor.settings.twilio.doSending) {
            if (Meteor.twilioClient) {
              Meteor.twilioClient.sendMessage({
                to: user.profile.phoneNumber,
                from: Meteor.settings.twilio.phoneNumber,
                body: title + '\n' + message
              },
              function(err, responseData) {
                if (err) {
                  console.log('Error sending sms message to family member: ' + err.message);
                }
              });
            }
          }

          if (user.profile.sendAppNotifications === undefined || user.profile.sendAppNotifications === true) {
            NotificationHistory.insert({ userId: user._id, title: title, message: message, timestamp: new Date() },
            function(error, result) {
              if (error) {
                console.log('error: ' + error);
                return;
              }

              Push.send({
                from: 'push',
                title: title,
                text: message,
                payload: {
                  title: title,
                  historyId: result
                },
                query: {
                  // Ex. send to a specific user if using accounts:
                  userId: user._id
                },
                gcm: true,
              });
            });
          }
        }
      });
    }
  };

  KonnektisNotifications.notifyAppointmentEndSummary = function (patientId, message) {
    // find the user, get the admins to notify and send the text message to them
    if (Meteor.isServer) {
      if(!Meteor.settings.twilio || !Meteor.settings.twilio.doSending) {
        return;
      }

      var patient = Patients.findOne(patientId);

      if(!patient || !patient.notifiers || !patient.notifiers.appointmentEndSummary) {
        return;
      }

      _.each(patient.notifiers.appointmentEndSummary, function(userId) {
        var user = Meteor.users.findOne(userId);
        if(user && user.profile && (user.profile.masterSendSms || user.profile.masterSendSms === undefined) && user.profile.sendSmsAppointmentEndSummary && user.profile.phoneNumber) {
          if (Meteor.twilioClient) {
            Meteor.twilioClient.sendMessage({
              to: user.profile.phoneNumber,
              from: Meteor.settings.twilio.phoneNumber,
              body: message
            },
            function(err, responseData) {
              if (err) {
                console.log('Error sending sms message to user: ' + err.message);
              }
            });
          }
        }
      });
    }
  };

  KonnektisNotifications.getAppointmentTypeLabel = function(type) {
    if (!type) {
      throw new Meteor.Error('Type must have a value');
    }

    switch(type) {
        case 'agency':
          return "Care Agency Appointment";
          break;
        case 'volunteer':
          return "Volunteer Care Appointment";
          break;
        case 'family':
          return "Family Care Appointment";
          break;
        case 'home':
          return "Home Appointment";
          break;
        case 'outside':
          return "Outside Appointment";
          break;
    }
  };
})(KonnektisNotifications);
