buildRoleString = function (role, patientId) {
  if (!role || !patientId) {
    throw new Error('role and patient id must both have a value');
  }

  if (patientId instanceof Mongo.ObjectID) {
    return role + ':' + patientId._str;
  }

  return role + ':' + patientId;
}

parseRoles = function(roles) {
  if (!roles) {
    return [];
  }

  var parsed = [];
  for (var i = 0; i < roles.length; i++) {
    var split = roles[i].split(':');
    var role = split[0];
    var patientId = split[1];
    parsed.push({ patientId: patientId, role: role });
  }

  return parsed;
}
