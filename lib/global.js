Konnektis = {};

(function (Konnektis) {
  'use strict';

  Konnektis.appVersion = function() {
    return '1.0.23';
  };
  // track if we are auto-showing an appointment on login
  Konnektis.autoAppointment = false;

  Konnektis.callMethodIfConnected = function() {
    if (Konnektis.checkConnection()) {
      Meteor.call.apply(Meteor, arguments);
      return true;
    } else {
      if (arguments.length > 0) {
        var ultimate = arguments[arguments.length - 1];
        if (typeof ultimate === "function") {
          try {
            ultimate({message: 'You are not connected to the server'});
          } catch (e) {
            console.log(e);
          }
        }
      }
      return false;
    }
  };

  Konnektis.checkConnection = function() {
    if (Meteor.status().connected) {
      return true;
    } else {
      bootbox.alert('You are not currently connected to the Konnektis server, please check your internet connection and try again.');
      return false;
    }
  };

  Konnektis.getAppointmentCommentTextSessionKey = function (appointmentId) {
    return Meteor.userId() + '_appointment_' + appointmentId + '_comment_text';
  };

  Konnektis.getMedicationCommentTextSessionKey = function (medicationId, date) {
    return Meteor.userId() + '_medication_' + medicationId + '_' + Konnektis.formatDateForLink(date) + '_comment_text';
  };

  Konnektis.getTaskCommentTextSessionKey = function (taskId, date) {
    return Meteor.userId() + '_task_' + taskId + '_' + Konnektis.formatDateForLink(date) + '_comment_text';
  };

  Konnektis.isAdmin = function () {
    return Roles.userIsInRole(Meteor.user(), ['admin']);
  };

  Konnektis.isFamily = function () {
    return Roles.userIsInRole(Meteor.user(), ['family:' + Konnektis.patientId()]);
  };

  Konnektis.isCarer = function () {
    return Roles.userIsInRole(Meteor.user(), ['carer:' + Konnektis.patientId()]);
  };

  Konnektis.getDateFromUtc = function(date) {
    return moment.utc(date).toDate()
  }

  Konnektis.getStartOfDay = function (date) {
    return moment.utc(date).startOf('day').toDate();
  };

  Konnektis.getEndOfDay = function (date) {
    return moment.utc(date).endOf('day').toDate();
  };

  Konnektis.getTime = function (time) {
    if (!time) {
      return null;
    }
    var timeOfDay = time.split(':');
    timeOfDay[0] =  Konnektis.ensureNumberHasTwoDigits(timeOfDay[0]);
    timeOfDay[1] =  Konnektis.ensureNumberHasTwoDigits(timeOfDay[1]);
    return { hours: timeOfDay[0], minutes: timeOfDay[1]};
  };

  Konnektis.partOfDayForTime = function (timeString) {
    switch(timeString) {
      case "07:00":
      case "08:00":
        return "Morning";
      case "12:00":
        return "Afternoon";
      case "17:00":
      case "18:00":
        return "Evening";
      case "21:00":
        return "Night";
      case "22:00":
      default:
        return "All Day";
    }
  };

  Konnektis.calculateMedicationOccurrences = function(recurringMedication, toDate) {
    if (!recurringMedication || !toDate) {
      return {};
    }

    var onDays;
    if (recurringMedication.recurrenceUnit === 'appointment') {
      if (!recurringMedication.recurringAppointmentId) {
        return {};
      }

      var recurringAppointment = RecurringAppointments.findOne(recurringMedication.recurringAppointmentId);

      if (!recurringAppointment) {
        throw new Meteor.Error('Expected to find a recurring appointment for id: ' + recurringMedication.recurringAppointmentId);
      }

      onDays = Konnektis.calculateOccurrences(recurringAppointment, toDate);
    }

    var occurrences = {};
    var from = new Date(recurringMedication.recurrenceStartDay);
    var to = new Date(toDate);
    for (var d = from; d <= to; d.setDate(d.getDate() + 1)) {
      if (recurringMedication.recurrenceUnit !== 'appointment' || onDays[Konnektis.formatDateForLink(d)]) {
        occurrences[Konnektis.formatDateForLink(d)] = {
          id: recurringMedication._id,
          name: recurringMedication.name,
          description: recurringMedication.description,
          startDate: Konnektis.setTimeFromString(d, recurringMedication.startTime),
          endDate: Konnektis.setTimeFromString(d, recurringMedication.endTime),
          carerUsername: recurringMedication.carerUsername,
        };
      }
    }

    return occurrences;
  };

  Konnektis.calculateTaskOccurrences = function(recurringTask, toDate) {
    if (!recurringTask || !toDate) {
      return {};
    }

    var onDays;
    if (recurringTask.recurrenceUnit === 'appointment') {
      if (!recurringTask.recurringAppointmentId) {
        return {};
      }

      var recurringAppointment = RecurringAppointments.findOne(recurringTask.recurringAppointmentId);

      if (!recurringAppointment) {
        throw new Meteor.Error('Expected to find a recurring appointment for id: ' + recurringTask.recurringAppointmentId);
      }

      onDays = Konnektis.calculateOccurrences(recurringAppointment, toDate);
    }

    var occurrences = {};
    var from = new Date(recurringTask.recurrenceStartDay);
    var to = new Date(toDate);
    for (var d = from; d <= to; d.setDate(d.getDate() + 1)) {
      if (recurringTask.recurrenceUnit !== 'appointment' || onDays[Konnektis.formatDateForLink(d)]) {
        occurrences[Konnektis.formatDateForLink(d)] = {
          id: recurringTask._id,
          name: recurringTask.name,
          description: recurringTask.description,
          startDate: Konnektis.setTimeFromString(d, recurringTask.startTime),
          endDate: Konnektis.setTimeFromString(d, recurringTask.endTime),
          carerUsername: recurringTask.carerUsername,
        };
      }
    }

    return occurrences;
  };

  Konnektis.calculateOccurrences = function(recurringAppointment, toDate) {
    if (!recurringAppointment || !toDate) {
      return {};
    }

    var intervalInDays;
    if (recurringAppointment.recurrenceUnit === 'day') {
      intervalInDays = recurringAppointment.recurrenceAmount;
    } else if (recurringAppointment.recurrenceUnit === 'week') {
      intervalInDays = recurringAppointment.recurrenceAmount * 7;
    } else {
      throw new Meteor.Error('Unexpected recurrence unit: ' + recurringAppointment.recurrenceUnit);
    }

    var occurrences = {};
    var from = new Date(recurringAppointment.recurrenceStartDay);
    var to = new Date(toDate);
    for (var d = from; d <= to; d.setDate(d.getDate() + intervalInDays)) {
      occurrences[Konnektis.formatDateForLink(d)] = {
        id: recurringAppointment._id,
        name: recurringAppointment.name,
        description: recurringAppointment.description,
        startDate: Konnektis.setTimeFromString(d, recurringAppointment.startTime),
        endDate: Konnektis.setTimeFromString(d, recurringAppointment.endTime),
      };
    }

    return occurrences;
  };

  Konnektis.setTimeFromString = function(date, timeString) {
    if (!date || !timeString) {
      throw new Meteor.Error('date and timeString must both have a value');
    }

    var date = new Date(date);
    var time = Konnektis.getTime(timeString);
    date.setHours(time.hours);
    date.setMinutes(time.minutes);
    return date;
  };

  Konnektis.extractTimeString = function(date) {
    if (!date) {
      return null;
    }
    return moment.utc(date).local().format('DD/MM/YYYY HH:mm');
  };

  Konnektis.combineDayAndTime = function(day, time) {
    if (!day) {
      throw new Meteor.Error('Day cannot be null');
    }
    var date = new Date(day);
    if (time) {
      date.setHours(time.getHours());
      date.setMinutes(time.getMinutes());
    } else {
      date.setHours(0);
      date.setMinutes(0);
    }
    return date;
  };

  Konnektis.ensureNumberHasTwoDigits = function (number) {
    if (!number) {
      throw new Meteor.Error('Number must have a value');
    }

    return ('0' + number).slice(-2);
  };

  Konnektis.formatDateForLink = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return moment(date).format('YYYY-MM-DD');
  };

  Konnektis.formatDate = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return moment(date).format('DD/MM/YYYY');
  };

  Konnektis.formatNiceDate = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return moment(date).format('DD MMM');
  };

  Konnektis.formatNiceDateTime = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return moment(date).format('DD MMM') + ' at ' + moment(date).format('HH:mm');
  };

  Konnektis.formatCommentDateTime = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return moment(date).format('HH:mm') + ' | ' + moment(date).format('DD/MM/YY');
  };

  Konnektis.getDateQueryParameters = function (date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    return {
      date: Konnektis.formatDateForLink(date)
    };
  };

  Konnektis.updateConcreteMedicationsWithRecurringData = function (recurringMedication) {
    if (!recurringMedication) {
      throw new Meteor.Error('Recurring medication must have a value');
    }

    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringMedicationId: recurringMedication._id }
      ]
    }).fetch();

    var errors = [];
    _.each(medications, function(value) {
      value.name = recurringMedication.name;
      value.description = recurringMedication.description;
      value.carerUsername = recurringMedication.carerUsername;
      value.scheduledStartTime = recurringMedication.startTime;
      value.scheduledEndTime = recurringMedication.endTime;

      Konnektis.callMethodIfConnected("updateMedication", value, function(error) {
        if (error) {
          errors.push(error);
        }
      });
    });
  };

  Konnektis.updateConcreteTasksWithRecurringData = function (recurringTask) {
    if (!recurringTask) {
      throw new Meteor.Error('Recurring task must have a value');
    }

    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringTaskId: recurringTask._id }
      ]
    }).fetch();

    var errors = [];
    _.each(tasks, function(value) {
      value.name = recurringTask.name;
      value.description = recurringTask.description;
      value.carerUsername = recurringTask.carerUsername;
      value.scheduledStartTime = recurringTask.startTime;
      value.scheduledEndTime = recurringTask.endTime;

      Konnektis.callMethodIfConnected("updateTask", value, function(error) {
        if (error) {
          errors.push(error);
        }
      });
    });
  };

  Konnektis.updateConcreteAppointmentsWithRecurringData = function (recurringAppointment) {
    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringAppointmentId: recurringAppointment._id }
      ]
    }).fetch();

    var errors = [];
    _.each(appointments, function(value) {
      value.name = recurringAppointment.name;
      value.description = recurringAppointment.description;
      value.carerUsername = recurringAppointment.carerUsername;
      value.scheduledStartTime = recurringAppointment.startTime;
      value.scheduledEndTime = recurringAppointment.endTime;

      Konnektis.callMethodIfConnected("updateAppointment", value, function(error) {
        if (error) {
          errors.push(error);
        }
      });
    });
  };

  Konnektis.getConcreteAppointmentForDate = function (recurringAppointmentId, date) {
    if (!recurringAppointmentId || !date) {
      throw new Meteor.Error('Recurring appointment id and date must both have values');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var concrete = Appointments.findOne({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $gte: startOfDay } },
        { scheduledEndDay: { $lte: endOfDay } },
        { recurringAppointmentId: recurringAppointmentId }
      ]
    });

    return concrete;
  };

  Konnektis.getConcreteMedicationForDate = function (recurringMedicationId, date) {
    if (!recurringMedicationId || !date) {
      throw new Meteor.Error('Recurring medication id and date must both have values');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var concrete = Medications.findOne({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $gte: startOfDay } },
        { scheduledEndDay: { $lte: endOfDay } },
        { recurringMedicationId: recurringMedicationId }
      ]
    });

    return concrete;
  };

  Konnektis.getConcreteTaskForDate = function (recurringTaskId, date) {
    if (!recurringTaskId || !date) {
      throw new Meteor.Error('Recurring task id and date must both have values');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var concrete = Tasks.findOne({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $gte: startOfDay } },
        { scheduledEndDay: { $lte: endOfDay } },
        { recurringTaskId: recurringTaskId }
      ]
    });

    return concrete;
  };

  Konnektis.runForMedicationOrConcreteIfRecurring = function (id, date, forMedication, forRecurring) {

    function createMedication(appointmentId) {
      var concrete = Konnektis.getConcreteMedicationForDate(recurringMedication._id, date);
      if (concrete) {
        forRecurring(concrete._id);
      } else {
        var medication = {
          patientId: Konnektis.patientId(),
          appointmentId: appointmentId,
          recurringMedicationId: recurringMedication._id,
          name: recurringMedication.name,
          description: recurringMedication.description,
          scheduledStartDay: moment.utc(date).toDate(),
          scheduledStartTimeType: recurringMedication.startTimeType,
          scheduledStartTime: recurringMedication.startTime,
          scheduledEndDay: moment.utc(date).toDate(),
          scheduledEndTimeType: recurringMedication.endTimeType,
          scheduledEndTime: recurringMedication.endTime,
          carerUsername: recurringMedication.carerUsername,
          isEvent: false,
          taskOrder: recurringMedication.taskOrder,
        };

        Konnektis.callMethodIfConnected("createMedication", medication, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            forRecurring(result);
          }
        });
      }
    }

    if (!id || !date) {
      throw new Meteor.Error('Id and date must both have values');
    }

    var medication = Medications.findOne(id);
    if (medication) {
      forMedication(id);
    } else {
      var recurringMedication = RecurringMedications.findOne(id);

      if (!recurringMedication) {
        throw new Meteor.Error('Cannot find medication or recurring medication');
      }

      var appointmentId;
      if (recurringMedication.recurringAppointmentId) {
        // check if concrete appointment exists on that day
        // if so use it's id for the medication
        // if not create one and use it's id for the medication
        var concrete = Konnektis.getConcreteAppointmentForDate(recurringMedication.recurringAppointmentId, date);

        if (concrete) {
          createMedication(concrete._id);
        } else {
          var recurringAppointment = RecurringAppointments.findOne(recurringMedication.recurringAppointmentId);

          if (!recurringAppointment) {
            throw new Meteor.Error('Could not find assigned recurring appointment: ' + recurringMedication.recurringAppointmentId);
          }

          Konnektis.convertConcreteAppointmentFromRecurring(
            recurringAppointment,
            date,
            function(appointmentId) {
              createMedication(appointmentId);
            },
            function(e) {
              Errors.throw(e.message);
            });
        }
      } else {
        createMedication(null);
      }
    }
  };


  Konnektis.runForTaskOrConcreteIfRecurring = function (id, date, forTask, forRecurring) {

    function createTask(appointmentId) {
      var concrete = Konnektis.getConcreteTaskForDate(recurringTask._id, date);
      if (concrete) {
        forRecurring(concrete._id);
      } else {
        var task = {
          patientId: Konnektis.patientId(),
          appointmentId: appointmentId,
          recurringTaskId: recurringTask._id,
          name: recurringTask.name,
          description: recurringTask.description,
          scheduledStartDay: moment.utc(date).toDate(),
          scheduledStartTimeType: recurringTask.startTimeType,
          scheduledStartTime: recurringTask.startTime,
          scheduledEndDay: moment.utc(date).toDate(),
          scheduledEndTimeType: recurringTask.endTimeType,
          scheduledEndTime: recurringTask.endTime,
          carerUsername: recurringTask.carerUsername,
          isEvent: false,
          taskOrder: recurringTask.taskOrder,
        };

        Konnektis.callMethodIfConnected("createTask", task, function(error, result) {
          if (error) {
            Errors.throw(error.message);
          } else {
            forRecurring(result);
          }
        });
      }
    }

    if (!id || !date) {
      throw new Meteor.Error('Id and date must both have values');
    }

    var task = Tasks.findOne(id);
    if (task) {
      forTask(id);
    } else {
      var recurringTask = RecurringTasks.findOne(id);

      if (!recurringTask) {
        throw new Meteor.Error('Cannot find task or recurring task');
      }

      var appointmentId;
      if (recurringTask.recurringAppointmentId) {
        // check if concrete appointment exists on that day
        // if so use it's id for the task
        // if not create one and use it's id for the task

        var concrete = Konnektis.getConcreteAppointmentForDate(recurringTask.recurringAppointmentId, date);

        if (concrete) {
          createTask(concrete._id);
        } else {
          var recurringAppointment = RecurringAppointments.findOne(recurringTask.recurringAppointmentId);

          if (!recurringAppointment) {
            throw new Meteor.Error('Could not find assigned recurring appointment: ' + recurringTask.recurringAppointmentId);
          }

          Konnektis.convertConcreteAppointmentFromRecurring(
            recurringAppointment,
            date,
            function(appointmentId) {
              createTask(appointmentId);
            },
            function(e) {
              Errors.throw(e.message);
            });
        }
      } else {
        createTask(null);
      }
    }
  };

  Konnektis.createConcreteAppointmentFromRecurring = function (recurringAppointment, date, onSuccess, onError) {
    if (!recurringAppointment || !date) {
      throw new Meteor.Error('Recurring appointment and date must both have values');
    }

    var appointment = {
      patientId: Konnektis.patientId(),
      recurringAppointmentId: recurringAppointment._id,
      name: recurringAppointment.name,
      description: recurringAppointment.description,
      scheduledStartDay: moment.utc(date).toDate(),
      scheduledStartTime: recurringAppointment.startTime,
      scheduledEndDay: moment.utc(date).toDate(),
      scheduledEndTime: recurringAppointment.endTime,
      carerUsername: recurringAppointment.carerUsername,
    };

    Konnektis.callMethodIfConnected("createAppointment", appointment, function(error, result) {
      if (error) {
        if (onError) {
          onError(error);
        }
      } else {
        if (onSuccess) {
          onSuccess(result);
        }
      }
    });
  };

  Konnektis.convertConcreteAppointmentFromRecurring = function (recurringAppointment, date, onSuccess, onError) {
    if (!recurringAppointment || !date) {
      throw new Meteor.Error('Recurring appointment and date must both have values');
    }

    var appointment = {
      patientId: recurringAppointment.patientId,
      recurringAppointmentId: recurringAppointment._id,
      name: recurringAppointment.name,
      description: recurringAppointment.description,
      scheduledStartDay: moment.utc(date).startOf('day').toDate(),
      scheduledStartTime: recurringAppointment.startTime,
      scheduledEndDay: moment.utc(date).startOf('day').toDate(),
      scheduledEndTime: recurringAppointment.endTime,
      carerUsername: recurringAppointment.carerUsername,
    };

    Konnektis.callMethodIfConnected("convertAppointment", appointment, function(error, result) {
      if (error) {
        if (onError) {
          onError(error);
        }
      } else {
        if (onSuccess) {
          onSuccess(result);
        }
      }
    });
  }

  Konnektis.runForAppointmentOrConcreteIfRecurring = function (id, date, forAppointment, forRecurring) {
    if (!id || !date) {
      throw new Meteor.Error('Id and date must both have values');
    }

    var appointment = Appointments.findOne(id);
    if (appointment) {
      forAppointment(id);
    } else {
      var recurringAppointment = RecurringAppointments.findOne(id);

      if (!recurringAppointment) {
        throw new Meteor.Error('Cannot find appointment or recurring appointment');
      }

      var concrete = Konnektis.getConcreteAppointmentForDate(recurringAppointment._id, date);
      if (concrete) {
        forRecurring(concrete._id);
      } else {
        Konnektis.convertConcreteAppointmentFromRecurring(
          recurringAppointment,
          date,
          forRecurring,
          function(e) {
            Errors.throw(e.message);
          });
      }
    }
  };

  Konnektis.removeAllConcreteMedicationOccurrencesFromDate = function (id, fromDate, whereSelector) {
    if (!id || !fromDate) {
      throw new Meteor.Error('Id and from date must both have values');
    }

    var recurringMedication = RecurringMedications.findOne(id);

    if (!recurringMedication) {
      throw new Meteor.Error('Cannot find recurring medication');
    }

    var startOfDay = moment.utc(fromDate).startOf('day').toDate();

    var medications = Medications.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringMedicationId: recurringMedication._id },
        { scheduledStartDay: { $gte: startOfDay } }
      ]
    }).fetch();

    var errors = [];
    _.each(medications, function(value) {
      if (!whereSelector || whereSelector(value) === true) {
        Konnektis.callMethodIfConnected("deleteMedication", value._id, Konnektis.patientId(), function(error, result) {
          if (error) {
            errors.push(error);
          }
        });
      }
    });
  };

  Konnektis.removeAllConcreteTaskOccurrencesFromDate = function (id, fromDate, whereSelector) {
    if (!id || !fromDate) {
      throw new Meteor.Error('Id and from date must both have values');
    }

    var recurringTask = RecurringTasks.findOne(id);

    if (!recurringTask) {
      throw new Meteor.Error('Cannot find recurring task');
    }

    var startOfDay = moment.utc(fromDate).startOf('day').toDate();

    var tasks = Tasks.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringTaskId: recurringTask._id },
        { scheduledStartDay: { $gte: startOfDay } }
      ]
    }).fetch();

    var errors = [];
    _.each(tasks, function(value) {
      if (!whereSelector || whereSelector(value) === true) {
        Konnektis.callMethodIfConnected("deleteTask", value._id, Konnektis.patientId(), function(error, result) {
          if (error) {
            errors.push(error);
          }
        });
      }
    });
  };

  Konnektis.removeAllConcreteAppointmentOccurrencesFromDate = function (id, fromDate, whereSelector) {
    if (!id || !fromDate) {
      throw new Meteor.Error('Id and from date must both have values');
    }

    var recurringAppointment = RecurringAppointments.findOne(id);

    if (!recurringAppointment) {
      throw new Meteor.Error('Cannot find recurring appointment');
    }

    var startOfDay = moment.utc(fromDate).startOf('day').toDate();

    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { recurringAppointmentId: recurringAppointment._id },
        { scheduledStartDay: { $gte: startOfDay } }
      ]
    }).fetch();

    var errors = [];
    _.each(appointments, function(value) {
      if (!whereSelector || whereSelector(value) === true) {
        Konnektis.callMethodIfConnected("deleteAppointment", value._id, Konnektis.patientId(), function(error, result) {
          if (error) {
            errors.push(error);
          }
        });
      }
    });
  };

  Konnektis.getRecurringAppointmentOccurrencesForDate = function(date, additionalQueries, includeWhenHasConcrete, ignorePatientId) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var queries = [];

    if(!ignorePatientId) {
      queries.push({ patientId: Konnektis.patientId() });
    }

    queries = queries.concat([
      { recurrenceStartDay: { $lte: endOfDay } },
      { recurrenceEndDay: { $gte: startOfDay } }
    ]);

    if (additionalQueries) {
      Array.prototype.push.apply(queries, additionalQueries);
    }

    var recurringAppointments = RecurringAppointments.find({
      $and: queries
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var nextDay = new Date(date);
    nextDay.setDate(nextDay.getDate() + 1);

    var toReturn = [];

    for (var i = 0; i < recurringAppointments.length; i++) {
      var r = recurringAppointments[i];
      var appointmentQueries = [];

      if(!ignorePatientId) {
        appointmentQueries.push({ patientId: Konnektis.patientId() });
      }

      appointmentQueries = appointmentQueries.concat([
        { scheduledStartDay: { $gte: startOfDay } },
        { scheduledEndDay: { $lte: endOfDay } },
        { recurringAppointmentId: r._id }
      ]);

      var concrete = Appointments.findOne({
        $and: appointmentQueries
      });

      if (!concrete || includeWhenHasConcrete === true) {
        var current = moment.utc(date);

        var occurrences = Konnektis.calculateOccurrences(r, nextDay);
        var occurrence = occurrences[Konnektis.formatDateForLink(current.toDate())];

        if (occurrence) {
          toReturn.push(r);
        }
      }
    }

    return toReturn;
  };

  Konnektis.getCarerRecurringAppointmentOccurrencesForDate = function(date, additionalQueries, includeWhenHasConcrete) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var queries = [
      { carerUsername: Meteor.user().username },
      { recurrenceStartDay: { $lte: endOfDay } },
      { recurrenceEndDay: { $gte: startOfDay } }
    ];

    if (additionalQueries) {
      Array.prototype.push.apply(queries, additionalQueries);
    }

    var recurringAppointments = RecurringAppointments.find({
      $and: queries
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var nextDay = new Date(date);
    nextDay.setDate(nextDay.getDate() + 1);

    var toReturn = [];

    for (var i = 0; i < recurringAppointments.length; i++) {
      var r = recurringAppointments[i];

      var concrete = Appointments.findOne({
        $and: [
          { carerUsername: Meteor.user().username },
          { scheduledStartDay: { $gte: startOfDay } },
          { scheduledEndDay: { $lte: endOfDay } },
          { recurringAppointmentId: r._id }
        ]
      });

      if (!concrete || includeWhenHasConcrete === true) {
        var current = moment.utc(date);

        var occurrences = Konnektis.calculateOccurrences(r, nextDay);
        var occurrence = occurrences[Konnektis.formatDateForLink(current.toDate())];

        if (occurrence) {
          toReturn.push(r);
        }
      }
    }

    return toReturn;
  };

  Konnektis.getRecurringMedicationOccurrencesForDate = function(date, additionalQueries) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var queries = [
      { patientId: Konnektis.patientId() },
      { recurrenceStartDay: { $lte: endOfDay } },
      { recurrenceEndDay: { $gte: startOfDay } }
    ];

    if (additionalQueries) {
      Array.prototype.push.apply(queries, additionalQueries);
    }

    var recurringMedications = RecurringMedications.find({
      $and: queries
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var nextDay = new Date(date);
    nextDay.setDate(nextDay.getDate() + 1);

    var toReturn = [];

    for (var i = 0; i < recurringMedications.length; i++) {
      var r = recurringMedications[i];

      var concrete = Medications.findOne({
        $and: [
          { patientId: Konnektis.patientId() },
          { scheduledStartDay: { $gte: startOfDay } },
          { scheduledEndDay: { $lte: endOfDay } },
          { recurringMedicationId: r._id }
        ]
      });

      if (!concrete) {
        var current = moment.utc(date);

        var occurrences = Konnektis.calculateMedicationOccurrences(r, nextDay);
        var occurrence = occurrences[Konnektis.formatDateForLink(current.toDate())];

        if (occurrence) {
          toReturn.push(r);
        }
      }
    }

    return toReturn;
  };

  Konnektis.getRecurringTaskOccurrencesForDate = function(date, additionalQueries) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    var startOfDay = moment.utc(date).startOf('day').toDate();
    var endOfDay = moment.utc(date).endOf('day').toDate();

    var queries = [
      { patientId: Konnektis.patientId() },
      { recurrenceStartDay: { $lte: endOfDay } },
      { recurrenceEndDay: { $gte: startOfDay } }
    ];

    if (additionalQueries) {
      Array.prototype.push.apply(queries, additionalQueries);
    }

    var recurringTasks = RecurringTasks.find({
      $and: queries
    }, {sort: {scheduledStartTime: 1}}).fetch();

    var nextDay = new Date(date);
    nextDay.setDate(nextDay.getDate() + 1);

    var toReturn = [];

    for (var i = 0; i < recurringTasks.length; i++) {
      var r = recurringTasks[i];

      var concrete = Tasks.findOne({
        $and: [
          { patientId: Konnektis.patientId() },
          { scheduledStartDay: { $gte: startOfDay } },
          { scheduledEndDay: { $lte: endOfDay } },
          { recurringTaskId: r._id }
        ]
      });

      if (!concrete) {
        var current = moment.utc(date);

        var occurrences = Konnektis.calculateTaskOccurrences(r, nextDay);
        var occurrence = occurrences[Konnektis.formatDateForLink(current.toDate())];

        if (occurrence) {
          toReturn.push(r);
        }
      }
    }

    return toReturn;
  };

  Konnektis.patientId = function() {
    return SessionAmplify.get("patientId");
  };

  Konnektis.patientName = function() {
    var patientId = SessionAmplify.get("patientId");

    if (!patientId) {
      return null;
    }

    var patient = Patients.findOne(patientId);

    if (!patient) {
      return null;
    }

    return patient.name;
  };

  Konnektis.appointmentCarerUsername = function(id) {
    var appointmentDetails = Appointments.findOne(id);

    if (appointmentDetails) {

      return appointmentDetails.carerUsername;

    }

    else {
      appointmentDetails = RecurringAppointments.findOne(id);

      if (appointmentDetails) {
        return appointmentDetails.carerUsername;
      }
    }
  };

  Konnektis.getCareStatus = function(date) {
    if (!date) {
      throw new Meteor.Error('Date must have a value');
    }

    var appointments = Appointments.find({
      $and: [
        { patientId: Konnektis.patientId() },
        { scheduledStartDay: { $gte: Konnektis.getStartOfDay(date) } },
        { scheduledStartDay: { $lte: Konnektis.getEndOfDay(date) } }
      ]
    }, {sort: {scheduledStartTime: -1}}).fetch();

    for (var i = 0; i < appointments.length; i++) {
      var a = appointments[i];

      if(a.careStatus != null) {
        return a.careStatus
      }
    }

    return "neutral";
  };

  Konnektis.getAppointmentTypeLabel = function(type) {
    if (!type) {
      throw new Meteor.Error('Type must have a value');
    }

    switch(type) {
        case 'agency':
          return "Care Agency Appointment";
          break;
        case 'volunteer':
          return "Volunteer Care Appointment";
          break;
        case 'family':
          return "Family Care Appointment";
          break;
        case 'home':
          return "Home Appointment";
          break;
        case 'outside':
          return "Outside Appointment";
          break;
    }
  };

  Konnektis.orderTasksOrMedications = function(tasks) {

    var orderedTasks;

    orderedTasks = _.filter(tasks, function(t) { return (t.scheduledStartTimeType === 'specific' || !t.scheduledStartTimeType) && ((t.scheduledStartTime && t.scheduledStartTime <= '11:59') || (t.startTime && t.startTime <= '11:59')) });
    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return t.scheduledStartTimeType === 'partOfDay' && (t.scheduledStartTime === 'Morning' || t.scheduledStartTime === '08:00') }));

    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return (t.scheduledStartTimeType === 'specific' || !t.scheduledStartTimeType) && ((t.scheduledStartTime && t.scheduledStartTime >= '12:00' && t.scheduledStartTime <= '17:59') || (t.startTime && t.startTime >= '12:00' && t.startTime <= '17:59')) }));
    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return t.scheduledStartTimeType === 'partOfDay' && t.scheduledStartTime === 'Afternoon' }));

    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return (t.scheduledStartTimeType === 'specific' || !t.scheduledStartTimeType) && ((t.scheduledStartTime && t.scheduledStartTime >= '18:00' && t.scheduledStartTime <= '20:59') || (t.startTime && t.startTime >= '18:00' && t.startTime <= '20:59')) }));
    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return t.scheduledStartTimeType === 'partOfDay' && (t.scheduledStartTime === 'Evening' || t.scheduledStartTime === '18:00') }));

    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return (t.scheduledStartTimeType === 'specific' || !t.scheduledStartTimeType) && ((t.scheduledStartTime && t.scheduledStartTime >= '21:00' && t.scheduledStartTime <= '00:00') || (t.startTime && t.startTime >= '21:00' && t.startTime <= '00:00')) }));
    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return t.scheduledStartTimeType === 'partOfDay' && t.scheduledStartTime === 'Night' }));

    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return t.scheduledStartTimeType === 'partOfDay' && t.scheduledStartTime === 'All Day' }));

    // Catch all remaining
    orderedTasks = _.union(orderedTasks, _.filter(tasks, function(t) { return orderedTasks.indexOf(t) < 0 }));

    return orderedTasks;
  };

  Konnektis.orderTasksOrMedicationsForAppointment = function(tasks) {
    // assign an order for any tasks/meds that don't already have an order
    var lastTaskOrder = 0;
    tasks = _.map(tasks, function(task) {
      if(task.taskOrder) {
        lastTaskOrder = task.taskOrder;
      }
      else {
        lastTaskOrder++;
        task.taskOrder = lastTaskOrder + 1;
      }

      return task;
    });

    return _.sortBy(tasks, function(task) {return task.taskOrder;});
 };

  Konnektis.normaliseRecurringMedicationProperties = function(recurringMedication, date) {
    if (!recurringMedication) {
      throw new Meteor.Error('Recurring medication must have a value when trying to normalise');
    }
    if (!date) {
      throw new Meteor.Error('Date must have a value when trying to normalise');
    }

    recurringMedication.state = 'scheduled';
    recurringMedication.scheduledStartDay = moment(date).startOf('day').toDate();
    recurringMedication.scheduledEndDay = moment(date).startOf('day').toDate();
    recurringMedication.scheduledStartTime = recurringMedication.startTime;
    recurringMedication.scheduledEndTime = recurringMedication.endTime;
    recurringMedication.scheduledStartTimeType = recurringMedication.startTimeType;
    recurringMedication.scheduledEndTimeType = recurringMedication.endTimeType;
    recurringMedication.carerUsername = recurringMedication.carerUsername || 'General Responsibility';

    recurringMedication.scheduledLabel = Konnektis.scheduledLabelString(recurringMedication, true, false);
  };

  Konnektis.normaliseRecurringTaskProperties = function(recurringTask, date) {
    if (!recurringTask) {
      throw new Meteor.Error('Recurring task must have a value when trying to normalise');
    }
    if (!date) {
      throw new Meteor.Error('Date must have a value when trying to normalise');
    }

    recurringTask.state = 'scheduled';
    recurringTask.scheduledStartDay = moment(date).startOf('day').toDate();
    recurringTask.scheduledEndDay = moment(date).startOf('day').toDate();
    recurringTask.scheduledStartTime = recurringTask.startTime;
    recurringTask.scheduledEndTime = recurringTask.endTime;
    recurringTask.scheduledStartTimeType = recurringTask.startTimeType;
    recurringTask.scheduledEndTimeType = recurringTask.endTimeType;
    recurringTask.carerUsername = recurringTask.carerUsername || 'General Responsibility';

    recurringTask.scheduledLabel = Konnektis.scheduledLabelString(recurringTask, true, false);
  };

  Konnektis.scheduledToEndOnStartDay = function(entity) {
    if (!entity) {
      throw new Meteor.Error('Entity must not be null when checking if scheduled to end on start day');
    }

    var startDay = moment(entity.scheduledStartDay);
    var endDay = moment(entity.scheduledEndDay);
    return startDay.diff(endDay, 'days') === 0;
  };

  Konnektis.scheduledLabelString = function(entity, condenseSimpleTimesToStartOnly, alwaysAddDate) {
    if (!entity.scheduledStartDay || !entity.scheduledEndDay) {
      throw new Meteor.Error('Both dates must have a value when determining entity date label');
    }

    var startDay = moment(entity.scheduledStartDay);
    var endDay = moment(entity.scheduledEndDay);

    var startTime, endTime;

    if (entity.scheduledStartTimeType === 'partOfDay') {
      startTime = Konnektis.partOfDayForTime(entity.scheduledStartTime);
    } else {
      startTime = entity.scheduledStartTime || '';
    }

    if (entity.scheduledEndTimeType === 'partOfDay') {
      endTime = Konnektis.partOfDayForTime(entity.scheduledEndTime);
    } else {
      endTime = entity.scheduledEndTime || '';
    }

    var scheduledToEndOnStartDay = startDay.diff(endDay, 'days') === 0;

    if (alwaysAddDate === true || !scheduledToEndOnStartDay) {
      return startDay.format('DD MMM')
        + ' '
        + startTime
        + ' - '
        + ((condenseSimpleTimesToStartOnly && scheduledToEndOnStartDay) ? '' : endDay.format('DD MMM'))
        + ' '
        + endTime;
    } else {
      if (condenseSimpleTimesToStartOnly === true) {
        return startTime;
      } else {
        return startTime + ' - ' + endTime;
      }
    }
  };

  Konnektis.recurringAppointmentScheduledLabelString = function(recurringAppointment, date, condenseSimpleTimesToStartOnly, alwaysAddDate) {
    if(date == undefined) {
      date = moment(new Date()).toDate();
    }
    else {
      date = moment(this.currentDate).toDate();
    }

    recurringAppointment.scheduledStartDay = date;
    recurringAppointment.scheduledEndDay = date;
    recurringAppointment.scheduledStartTime = recurringAppointment.startTime;
    recurringAppointment.scheduledEndTime = recurringAppointment.endTime;

    return Konnektis.scheduledLabelString(recurringAppointment, condenseSimpleTimesToStartOnly, alwaysAddDate);
  };

  Konnektis.completedLabelString = function(entity, currentDate, condenseSimpleTimesToStartOnly, alwaysAddDate) {
    if (!entity.actualEndDate) {
      throw new Meteor.Error('Both dates must have a value when determining entity date label');
    }

    if (!entity.actualStartDate) {
      entity.actualStartDate = entity.actualEndDate;
    }

    var startDay = moment(entity.actualStartDate);
    var endDay = moment(entity.actualEndDate);

    var endedOnStartDay = startDay.diff(endDay, 'days') === 0;
    var completedToday = startDay.diff(moment(currentDate), 'days') === 0;

    if (alwaysAddDate === true || !endedOnStartDay || !completedToday) {
      return startDay.format('DD MMM HH:mm')
        + ' - '
        + ((condenseSimpleTimesToStartOnly && endedOnStartDay) ? endDay.format('HH:mm') : endDay.format('DD MMM HH:mm'));
    } else {
      if (condenseSimpleTimesToStartOnly === true) {
        return startDay.format('HH:mm');
      } else {
        return startDay.format('HH:mm') + ' - ' + endDay.format('HH:mm');
      }
    }
  };

  Konnektis.inactivity_timeout = undefined;
  Konnektis.pre_inactivity_timeout = undefined;
  Konnektis.logout_message_timer = undefined;
  Konnektis.logout_timer = undefined;
  Konnektis.logout_warning_started_at = undefined;
  Konnektis.got_timer_durations = false;
  Konnektis.document_ready = false;

  if (Meteor.isClient) {
    Konnektis.timerElements = [document, document.documentElement];

    Meteor.call('getTimerSettings', function(err, results) {
      if(!err) {
        Konnektis.inactivity_timeout = results.inactivity_timeout;
        Konnektis.pre_inactivity_timeout = results.pre_inactivity_timeout;

        if(Konnektis.inactivity_timeout == false || isNaN(Konnektis.inactivity_timeout) || Konnektis.inactivity_timeout <= 0) {
          Konnektis.inactivity_timeout = 60 * 60 * 1000;   // 60 minutes
        };

        if(Konnektis.pre_inactivity_timeout == false || isNaN(Konnektis.pre_inactivity_timeout) || Konnektis.pre_inactivity_timeout <= 0) {
          Konnektis.pre_inactivity_timeout = 55 * 60 * 1000;   // 55 minutes
        };
        Konnektis.got_timer_durations = true;
        Konnektis.setupTimers();
      }
    });
  }
  else {
    Konnektis.timerElements = [];
  }

  Konnektis.imminentLogoutMessage = function() {
    if(Konnektis.logout_warning_started_at == undefined) {
      // this shouldn't happen!
      Konnektis.logout_warning_started_at = moment();
    }
    var logout_delay = Konnektis.inactivity_timeout - Konnektis.pre_inactivity_timeout;
    var time_waited = moment() - Konnektis.logout_warning_started_at;
    var remaing_time = (time_waited > logout_delay ? 0 : logout_delay - time_waited);
    var logoutRemainingStr = moment.utc(remaing_time).format("mm:ss");
    return "You have been away from Konnektis for too long and will be logged out in " + logoutRemainingStr + " unless you click the OK button";
  };

  if (Meteor.isClient) {

  Konnektis.timersAction = function(timerAction) {
    var numTimers = Konnektis.timerElements.length;

    for (var timerIndex = 0; timerIndex < numTimers; timerIndex++) {
      var docElement = Konnektis.timerElements[timerIndex];
      $( docElement ).idleTimer(timerAction);
    }
  };

  Konnektis.startTimers = function() {
    Konnektis.timersAction("reset");
  };

  Konnektis.pauseTimers = function() {
    Konnektis.timersAction("pause");
  };

  Konnektis.setupTimers = function() {
    if(!Konnektis.got_timer_durations || !Konnektis.document_ready) {
      // not yet ready - two things have to happen!
      return;
    }
    $( document ).idleTimer(Konnektis.pre_inactivity_timeout);
    // $( document.documentElement ).idleTimer(Konnektis.inactivity_timeout);

    Konnektis.pauseTimers();

    $( document ).on( "idle.idleTimer", function(event, elem, obj){
      event.stopPropagation();

      Konnektis.logout_warning_started_at = moment();

      if(Konnektis.logout_message_timer != undefined) {
        clearInterval(Konnektis.logout_message_timer);
      }
      Konnektis.logout_message_timer = setInterval(function() {
        $('.idleWarningBox .bootbox-body').text(Konnektis.imminentLogoutMessage());
        },
        250
      );

      if(Konnektis.logout_timer != undefined) {
        clearInterval(Konnektis.logout_timer);
      }
      Konnektis.logout_timer = setInterval(function() {
        clearInterval(Konnektis.logout_message_timer);
        clearInterval(Konnektis.logout_timer);
        Meteor.logout(true);
        },
        Konnektis.inactivity_timeout - Konnektis.pre_inactivity_timeout
      );

      bootbox.alert({
        className: "idleWarningBox",
        message: Konnektis.imminentLogoutMessage(),
        callback: function() {
          if(Konnektis.logout_message_timer != undefined) {
            clearInterval(Konnektis.logout_message_timer);
            clearInterval(Konnektis.logout_timer);
          }
          Konnektis.startTimers();
        }
      });
    });

    $( document.documentElement ).on( "idle.idleTimer", function(event, elem, obj){
      event.stopPropagation();
      bootbox.hideAll();  // just in case there is a message box up about taking the current appointment, for example
      Meteor.logout(true);
    });
  };

  $( document ).ready(function() {
    Konnektis.document_ready = true;
    Konnektis.setupTimers();
  });

  }

  Konnektis.stateString = function(appointment) {
    return appointment.actualEndDate !== null ? 'finished' : appointment.actualStartDate !== null ? 'taking-place' : 'scheduled';
  };

  Konnektis.stateLabelString = function(appointment) {
    return appointment.actualEndDate !== null ? 'Visit Completed' : appointment.actualStartDate !== null ? 'Visit Started' : 'Scheduled';
  };

  Konnektis.closeMenu = function() {
    $('#menu-dropdown-list')
      .removeClass('open')
      .find('a.dropdown-toggle').prop('aria-expanded', 'false');
  };

  Konnektis.setRole = function() {
    console.log('Konnektis.setRole role not set');
    if(Konnektis.isAdmin())
    {
      console.log('Konnektis.setRole is admin');
      SessionAmplify.set("role", "admin");
    }
    else if(Konnektis.isFamily())
    {
      console.log('Konnektis.setRole is family');
      SessionAmplify.set("role", "family");
    }
    else {
      console.log('Konnektis.setRole is carer');
      SessionAmplify.set("role", "carer");
    }
  };

  Konnektis.allBodyClasses = ['carer', 'family', 'admin', 'login', 'walkup'];
  Konnektis.optionalBodyClasses = ['login', 'walkup'];

  Konnektis.clearBodyClass = function() {
    $('body').removeClass(Konnektis.allBodyClasses.join(' '));
  }

  Konnektis.addBodyClass = function(cssClass) {
    var body = $('body');

    if(Array.isArray(cssClass)) {
      var removingCssClasses = Konnektis.optionalBodyClasses.filter(function (optionalCssClass, index, arr) {
        return cssClass.indexOf(optionalCssClass) == -1;
      });

      if(removingCssClasses.length > 0) {
        body.removeClass(removingCssClasses.join(' '));
      }

      body.addClass(cssClass.join(' '));
    }
    else {
      var removingCssClasses = Konnektis.optionalBodyClasses.filter(function (optionalCssClass, index, arr) {
        return optionalCssClass != cssClass;
      });

      if(removingCssClasses.length > 0) {
        body.removeClass(removingCssClasses.join(' '));
      }

      body.addClass(cssClass);
    }
  }

  Konnektis.setBodyClass = function() {
    var body = $('body');

    // default to family role if patient hasn't been selected yet
    var role = SessionAmplify.get('role') || 'family';

    switch(role) {
      case 'carer':
        body.addClass('carer')
          .removeClass('family');
        break;
      case 'family':
        body.addClass('family')
          .removeClass('carer');
        break;
      default:
        body.removeClass('family')
          .removeClass('carer');
    }
  }

  Konnektis.compareAppointmentStartTimes = function(a, b) {
    if(a.scheduledStartTime < b.scheduledStartTime)
      return -1;
    else if (a.scheduledStartTime > b.scheduledStartTime)
      return 1;
    else
      return 0;
  }

  Konnektis.addStringTimeToMoment = function(stringTime, thisMoment) {
    if(typeof stringTime == 'string') {
      var timeSplit = stringTime.split(':');
      if(Array.isArray(timeSplit) && (timeSplit.length == 2)) {
        thisMoment.add({hours: parseInt(timeSplit[0]), minutes: parseInt(timeSplit[1])});
      }
    }
    return thisMoment;
  }

  Konnektis.recordCarerLogin = function(patientId) {
    Konnektis.callMethodIfConnected('setCarerPatientLastLogin', patientId, function(error, result) {
      if (error) {
        Errors.throw(error.message);
      } else {
        // all's fine, nothing to do, login has been recorded
      }
    });
  }

  Konnektis.alertCarerReviewChanges = function() {
    bootbox.alert('Please review all changed items, indicated by an exclamation mark, before continuing with other tasks.');
  }

  Konnektis.hasPatientInfoChanged = function(collection, carerId, patientId, lastLogin, patientDocument) {
    var doc = collection.findOne({patientId: patientId});
    if(!doc) {
      // no document in this collection, can't have changed
      return false;
    }

    if(!lastLogin.patientDocumentLastReviewDates) {
      // no last review dates, can't have seen this change
      return true;
    }

    var lastReviewed = lastLogin.patientDocumentLastReviewDates[patientDocument];

    if(!lastReviewed) {
      // there is a last reviews object, but no value for this patient document
      return true;
    }

    var updatedTime = doc.updatedTime;

    if(updatedTime && updatedTime < lastReviewed) {
      return false
    }

    // otherwise
    return true;
  };

  Konnektis.getCarerPatientLastLogin = function() {
    var carerId = Meteor.userId();
    if(!carerId) {
      return false;
    }

    var patientId = Konnektis.patientId();
    if(!patientId) {
      return false;
    }

    var carerPatientLastLogin = CarerPatientLastLogins.findOne({carerId: carerId, patientId: patientId});
    if(!carerPatientLastLogin) {
      return false;
    }

    return carerPatientLastLogin;
  };

  // carer/patient document constants
  Konnektis.patientDocuments = {
    patientInfo: 'patientInfo',
    riskAssessment: 'riskAssessment',
    carePlan: 'carePlan'
  };

  Konnektis.clientDisplayName = function(patientId) {
    if(patientId == undefined) {
      patientId = Konnektis.patientId();
    }

    if(!patientId) {
      return false;
    }

    var patient = Patients.findOne(patientId);
    if(!patient) {
      return false;
    }

    return patient.displayName || patient.name;
  };

  Konnektis.appointmentTimes = function(appointmentId, date, condenseSimpleTimesToStartOnly, alwaysAddDate) {
    if(appointmentId == undefined) {
      appointmentId = SessionAmplify.get("currentAppointment");
    }

    if(!appointmentId) {
      return false;
    }

    var appointment = Appointments.findOne(appointmentId);

    if(appointment) {
      return Konnektis.scheduledLabelString(appointment, condenseSimpleTimesToStartOnly, alwaysAddDate);
    }
    else {
      // could be a recurring appointment
      var recurringAppointment = RecurringAppointments.findOne(appointmentId);
      if(recurringAppointment) {
        return Konnektis.recurringAppointmentScheduledLabelString(recurringAppointment, date, condenseSimpleTimesToStartOnly, alwaysAddDate);
      }
      else {
        return false;
      }
    }
  };

  Konnektis.carerDisplayName = function(carerUsername) {
    var carer = Carers.findOne({username: carerUsername});
    if(!carer) {
      return false;
    }
    return (carer.profile ? carer.profile.displayName || carer.username : carer.username);
  };

  Konnektis.devicePaired = function() {
    var deviceId = localStorage.getItem('KonnektisDeviceId');
    var deviceNotPaired = (deviceId == false || deviceId == null);
    return (!deviceNotPaired);
  };

  Konnektis.patientSet = function() {
    if(Konnektis.patientId()) {
      return true;
    }
    return false;
  };

  Konnektis.generateId = function(len) {
    var arr = new Uint8Array((len || 40) / 2);
    window.crypto.getRandomValues(arr);
    return [].map.call(arr, function(n) { return n.toString(16); }).join("");
  };

  Konnektis.possessiveName = function(name) {
    // make a possessive version of name

    if(typeof name === 'string') {
      if(name.substr(name.length - 1) == 's') {
        name = name + "'";
      }
      else {
        name = name + "'s";
      }
    }
    else {
      return name;
    }
  }

  Konnektis.patientAugmentPossessiveName = function(patient) {
    // add a possessive version of the service user's/patient's name

    if(!patient || !patient.name) {
      return patient;
    }

    patient.possessiveName = Konnektis.possessiveName(patient.name);

    return patient;
  }

  Konnektis.apptAugmentPatient = function(appointmentDetails) {
    // take an Appointment document and augment the service user/patient's name
    // requires access to the Patients collection (for this service user at least)

    if(!appointmentDetails || !appointmentDetails.patientId) {
      return appointmentDetails;
    }

    var patient = Patients.findOne(appointmentDetails.patientId);

    if(!patient.displayName && patient.name) {
      patient.displayName = patient.name;
    }

    appointmentDetails.patient = Konnektis.patientAugmentPossessiveName(patient);

    return appointmentDetails;
  }

  Konnektis.isPassValid = function(password){
    if(password.length < 8 ){
      return false
    }

    if(!/[A-Z]/.test(password)){
      return false
    }

    if(!/[a-z]/.test(password)){
      return false
    }

    if(!/[0-9]/.test(password)){
      return false
    }

    if(!/[@#\$%\^&\*\(\)_\+\|~\-=\\`{}\[\]:";'<>\/]/.test(password)){
      return false
    }

    return true
  }
})(Konnektis);


if (Meteor.isClient) {

Handlebars.registerHelper("lineBreakString", function (str) {
  if(str == undefined) {
    return str;
  }
  else {
    return new Handlebars.SafeString(str.replace(/(\r\n|\n\r|\r|\n)/g, "<br />"));
  }
});


Handlebars.registerHelper('equals', function(v1, v2) {
  return (v1 == v2);
});

}

// if(FontFace) {
//   var font = new FontFace('Glyphicons Halflings', "url(/fonts/bootstrap/glyphicons-halflings-regular.woff2)", {
//     style: 'normal', weight: 'normal'
//   });

//   font.load(); // don't wait for render tree, initiate immediate fetch!
// }

// font.ready().then(function() {
//   // apply the font (which may rerender text and cause a page reflow)
//   // once the font has finished downloading
//   document.fonts.add(font);
// });
