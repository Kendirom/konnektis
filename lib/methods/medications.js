if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])) {
    throw new Meteor.Error("You aren't authorised to create medications for that patient");
  }
}

function throwIfAppointmentNotFound(appointmentId) {
  if (appointmentId) {
    var appointment = Appointments.findOne(appointmentId);

    if (!appointment) {
      throw new Meteor.Error("Cannot find the specified appointment");
    }
  }

  return appointment;
}

function throwIfCompleteUnauthorised(patientId, medicationId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])) {
    throw new Meteor.Error("You aren't authorised to update medications for that patient");
  }

  if(medicationId) {
    var medication = Medications.findOne(medicationId);

    if (!medication) {
      throw new Meteor.Error("Medication not found");
    }

    if(!(medication.carerUsername === Meteor.user().username || !medication.carerUsername)) {
      throw new Meteor.Error("You aren't authorised to update this medication");
    }
  }
}

function throwIfAppointmentInPast(appointmentId, appointment) {
  var startOfToday = moment.utc().startOf('day');

  if (appointmentId) {
    if(appointment == undefined) {
      appointment = Appointments.findOne(appointmentId);
    }

    if (moment(appointment.scheduledStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create medications for past appointments');
    }
  }
}

function throwIfInThePast(medicationId, medication) {
  var startOfToday = moment().startOf('day');

  var a = Medications.findOne(medicationId);
  if (a && moment(a.scheduledStartDay).isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot edit past events');
  }

  if (medication && moment(medication.scheduledStartDay).isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot create events in the past');
  }
}

function throwIfSpansTooManyDays(medication) {
  var medicationMaximumLengthInHours = 23;

  var from = combineDayAndTime(medication.scheduledStartDay, medication.scheduledStartTime);
  var to = combineDayAndTime(medication.scheduledEndDay, medication.scheduledEndTime);
  if (to.diff(from, 'hours') > medicationMaximumLengthInHours) {
    throw new Meteor.Error('Medications cannot span more than 24 hours');
  }
}

function throwIfInvalidMedicationTakenOption(takenOption) {
  var medicationTakenOption = MedicationTakenOptions.findOne({taken: takenOption});

  if (!medicationTakenOption) {
    throw new Meteor.Error("Medication taken option not found");
  }
}

function throwIfInvalidMedicationLocationOption(locationOption) {
  var medicationLocationOption = MedicationLocationOptions.findOne({location: locationOption});

  if (!medicationLocationOption) {
    throw new Meteor.Error("Medication location option not found");
  }
}

function parseTimeString(timeString) {
  if (!timeString) {
    return null;
  }

  var segments = timeString.split(':');

  return {
    hours: segments[0], minutes: segments[1]
  };
}

function combineDayAndTime(day, timeString) {
  if (!day) {
    throw new Meteor.Error('Day cannot be null');
  }
  var date = moment(day);
  var time = parseTimeString(timeString);
  if (time) {
    date.hours(time.hours);
    date.minutes(time.minutes);
    date.seconds(0);
    date.milliseconds(0);
  } else {
    date.hours(0);
    date.minutes(0);
    date.seconds(0);
    date.milliseconds(0);
  }
  return date;
}

function throwIfInvalidStartOrEndTimes(medication) {
  var startDate = combineDayAndTime(medication.scheduledStartDay, medication.scheduledStartTime);
  var endDate = combineDayAndTime(medication.scheduledEndDay, medication.scheduledEndTime);

  if (endDate.isBefore(startDate)) {
    throw new Meteor.Error('Start days must preceed end days');
  }
}

function throwIfOutsideBoundsOfAppointment(medication, appointmentId, appointment) {
  if (appointmentId) {
    if(appointment == undefined) {
      appointment = Appointments.findOne(appointmentId);
    }

    var medicationStartDate = combineDayAndTime(medication.scheduledStartDay, medication.scheduledStartTime);
    var medicationEndDate = combineDayAndTime(medication.scheduledEndDay, medication.scheduledEndTime);

    var appointmentStartDate = combineDayAndTime(appointment.scheduledStartDay, appointment.scheduledStartTime);
    var appointmentEndDate = combineDayAndTime(appointment.scheduledEndDay, appointment.scheduledEndTime);

    if (medicationStartDate.isBefore(appointmentStartDate) || medicationEndDate.isAfter(appointmentEndDate)) {
      throw new Meteor.Error('Medication must fall within the bounds of their appointment');
    }
  }
}

Meteor.methods({
  createMedication: function (medication) {
    throwIfUnauthorised(medication.patientId);
    throwIfAppointmentNotFound(medication.appointmentId);
    throwIfAppointmentInPast(medication.appointmentId);
    throwIfInThePast(null, medication);
    throwIfInvalidStartOrEndTimes(medication);
    throwIfOutsideBoundsOfAppointment(medication, medication.appointmentId);
    throwIfSpansTooManyDays(medication);

    if(!medication.taskOrder) {
      var numMedications = Medications.find({appointmentId: medication.appointmentId}).count();
      var maxTaskOrderMedication = Medications.findOne({appointmentId: medication.appointmentId}, {sort: {taskOrder: -1}, limit: 1});
      var taskOrder = numMedications + 1;

      if(maxTaskOrderMedication && maxTaskOrderMedication.taskOrder > numMedications) {
        taskOrder = maxTaskOrderMedication.taskOrder + 1;
      }

      medication.taskOrder = taskOrder;
    }

    var id = Medications.insert({
      patientId: medication.patientId,
      appointmentId: medication.appointmentId,
      recurringMedicationId: medication.recurringMedicationId,
      name: medication.name,
      description: medication.description,
      scheduledStartDay: medication.scheduledStartDay,
      scheduledStartTime: medication.scheduledStartTime,
      scheduledStartTimeType: medication.scheduledStartTimeType,
      scheduledEndDay: medication.scheduledEndDay,
      scheduledEndTime: medication.scheduledEndTime,
      scheduledEndTimeType: medication.scheduledEndTimeType,
      actualStartDate: null,
      actualEndDate: null,
      carerUsername: medication.carerUsername,
      isEvent: medication.isEvent,
      careStatus: null,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username,
      taskOrder: medication.taskOrder
    });

    return id;
  },
  updateMedication: function(medication) {
    throwIfUnauthorised(medication.patientId);
    var appointment = throwIfAppointmentNotFound(medication.appointmentId);
    throwIfAppointmentInPast(medication.appointmentId, appointment);
    throwIfInThePast(medication._id, medication);
    throwIfInvalidStartOrEndTimes(medication);
    throwIfOutsideBoundsOfAppointment(medication, medication.appointmentId, appointment);
    throwIfSpansTooManyDays(medication);

    var result = Medications.update(
      { _id: medication._id },
      {
        $set: {
          appointmentId: medication.appointmentId,
          recurringMedicationId: medication.recurringMedicationId,
          name: medication.name,
          description: medication.description,
          scheduledStartDay: medication.scheduledStartDay,
          scheduledStartTime: medication.scheduledStartTime,
          scheduledStartTimeType: medication.scheduledStartTimeType,
          scheduledEndDay: medication.scheduledEndDay,
          scheduledEndTime: medication.scheduledEndTime,
          scheduledEndTimeType: medication.scheduledEndTimeType,
          actualStartDate: medication.actualStartDate,
          actualEndDate: medication.actualEndDate,
          carerUsername: medication.carerUsername,
          isEvent: medication.isEvent,
          careStatus: medication.careStatus,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
        }
    });

    return result;
  },
  deleteMedication: function(id, patientId) {
    throwIfUnauthorised(patientId);
    throwIfInThePast(id, null);

    var a = RecurringMedications.findOne(id);

    if (a) {
      throw new Meteor.Error("You can't delete a recurring medication.");
    }

    var medication = Medications.findOne(id);

    if (!medication) {
      throw new Meteor.Error("Could not find the medication.");
    }

    var result = Comments.remove({ parentId: id });

    result = Medications.remove({ _id: id });

    return !result.writeError;
  },
  completeMedication: function(medication) {
    throwIfCompleteUnauthorised(medication.patientId, medication._id);
    var appointment = throwIfAppointmentNotFound(medication.appointmentId);
    throwIfInThePast(medication._id, medication, appointment);

    var result = Medications.update(
      { _id: medication._id },
      {
        $set: {
          actualEndDate: moment().toDate(),
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
          completedUsername: Meteor.user().username,
        }
    });

    // var patient = Patients.findOne({ _id: medication.patientId });

    // return result;
  },
  setMedicationTakenOption: function(medication, takenOption) {
    throwIfInvalidMedicationTakenOption(takenOption);
    throwIfCompleteUnauthorised(medication.patientId, medication._id);
    var appointment = throwIfAppointmentNotFound(medication.appointmentId);
    throwIfInThePast(medication._id, medication, appointment);

    var result = Medications.update(
      { _id: medication._id },
      {
        $set: {
          taken: takenOption,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });
  },
  setMedicationLocationOption: function(medication, locationOption) {
    throwIfInvalidMedicationLocationOption(locationOption);
    throwIfCompleteUnauthorised(medication.patientId, medication._id);
    var appointment = throwIfAppointmentNotFound(medication.appointmentId);
    throwIfInThePast(medication._id, medication, appointment);

    var result = Medications.update(
      { _id: medication._id },
      {
        $set: {
          location: locationOption,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });
  },
  uncompleteMedication: function(medication) {
    throwIfCompleteUnauthorised(medication.patientId, medication._id);
    throwIfAppointmentNotFound(medication.appointmentId);
    throwIfInThePast(medication._id, medication);

    var result = Medications.update(
      { _id: medication._id },
      {
        $set: {

          actualEndDate: null,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
        }
    });

    var patient = Patients.findOne({ _id: medication.patientId });

    return result;
  }
});


  // close if (Meteor.isServer)
}
