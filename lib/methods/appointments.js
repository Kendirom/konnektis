if (Meteor.isServer) {

function throwIfUnauthorised(patientId, roles, returnNotThrow) {
  if (! Meteor.userId() || !patientId) {
    if(returnNotThrow) {
      return false;
    }
    else {
      throw new Meteor.Error("Not authorised");
    }
  }

  if (!Roles.userIsInRole(Meteor.userId(), roles)) {
    if(returnNotThrow) {
      return false;
    }
    else {
      throw new Meteor.Error("You aren't authorised to perform that function for the patient");
    }
  }

  if(returnNotThrow) {
    return true;
  }
}

function throwIfNotFamilyOrAdminOrCarer(patientId, returnNotThrow) {
  throwIfUnauthorised(patientId, ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)], returnNotThrow)
}

function throwIfNotFamilyOrAdmin(patientId, returnNotThrow) {
  throwIfUnauthorised(patientId, ['admin', buildRoleString('family', patientId)], returnNotThrow)
}

function throwIfInThePast(appointmentId, appointment) {
  var startOfToday = moment.utc().startOf('day');

  var a = Appointments.findOne(appointmentId);
  if (a && moment(a.scheduledStartDay).isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot edit past events');
  }

  if (appointment && moment(appointment.scheduledStartDay).utc().isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot create events in the past');
  }
}

function throwIfSpansTooManyDays(appointment) {
  var appointmentMaximumLengthInHours = 23;

  var from = combineDayAndTime(appointment.scheduledStartDay, appointment.scheduledStartTime);
  var to = combineDayAndTime(appointment.scheduledEndDay, appointment.scheduledEndTime);
  if (to.diff(from, 'hours') > appointmentMaximumLengthInHours) {
    throw new Meteor.Error('Appointments cannot span more than 24 hours');
  }
}

function throwIfStarted(appointmentId, appointment) {

  var a = Appointments.findOne(appointmentId);

  if (a && (a.actualStartDate !== null && a.actualEndDate == null)){
    throw new Meteor.Error('Cannot deleted started appointments');
  }
}

function parseTimeString(timeString) {
  if (!timeString) {
    return null;
  }

  var segments = timeString.split(':');

  return {
    hours: segments[0], minutes: segments[1]
  };
}

function combineDayAndTime(day, timeString) {
  if (!day) {
    throw new Meteor.Error('Day cannot be null');
  }
  var date = moment(day);
  var time = parseTimeString(timeString);
  if (time) {
    date.hours(time.hours);
    date.minutes(time.minutes);
  } else {
    date.hours(0);
    date.minutes(0);
  }
  return date;
}

function throwIfInvalidStartOrEndTimes(appointment) {
  var startDate = combineDayAndTime(appointment.scheduledStartDay, appointment.scheduledStartTime);
  var endDate = combineDayAndTime(appointment.scheduledEndDay, appointment.scheduledEndTime);

  if (endDate.isBefore(startDate)) {
    throw new Meteor.Error('Start days must preceed end days');
  }
}

Meteor.methods({
  createAppointment: function (appointment) {
    throwIfNotFamilyOrAdmin(appointment.patientId);
    throwIfInThePast(null, appointment);
    throwIfInvalidStartOrEndTimes(appointment);
    throwIfSpansTooManyDays(appointment);

    var id = Appointments.insert({
      patientId: appointment.patientId,
      recurringAppointmentId: appointment.recurringAppointmentId,
      name: appointment.name,
      description: appointment.description,
      scheduledStartDay: appointment.scheduledStartDay,
      scheduledStartTime: appointment.scheduledStartTime,
      scheduledEndDay: appointment.scheduledEndDay,
      scheduledEndTime: appointment.scheduledEndTime,
      actualStartDate: null,
      actualEndDate: null,
      carerUsername: appointment.carerUsername,
      careStatus: null,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username
    });

    return id;
  },
  convertAppointment: function (appointment) {
    throwIfNotFamilyOrAdminOrCarer(appointment.patientId);
    throwIfInThePast(null, appointment);
    throwIfInvalidStartOrEndTimes(appointment);

    var id = Appointments.insert({
      patientId: appointment.patientId,
      recurringAppointmentId: appointment.recurringAppointmentId,
      name: appointment.name,
      description: appointment.description,
      scheduledStartDay: appointment.scheduledStartDay,
      scheduledStartTime: appointment.scheduledStartTime,
      scheduledEndDay: appointment.scheduledEndDay,
      scheduledEndTime: appointment.scheduledEndTime,
      actualStartDate: null,
      actualEndDate: null,
      carerUsername: appointment.carerUsername,
      careStatus: null,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username
    });

    return id;
  },
  updateAppointment: function(appointment) {
    throwIfNotFamilyOrAdmin(appointment.patientId);
    throwIfInThePast(appointment._id, appointment);
    throwIfInvalidStartOrEndTimes(appointment);
    throwIfSpansTooManyDays(appointment);

    var result = Appointments.update(
      { _id: appointment._id },
      {
        $set: {
          recurringAppointmentId: appointment.recurringAppointmentId,
          name: appointment.name,
          description: appointment.description,
          scheduledStartDay: appointment.scheduledStartDay,
          scheduledStartTime: appointment.scheduledStartTime,
          scheduledEndDay: appointment.scheduledEndDay,
          scheduledEndTime: appointment.scheduledEndTime,
          actualStartDate: appointment.actualStartDate,
          actualEndDate: appointment.actualEndDate,
          carerUsername: appointment.carerUsername,
          careStatus: appointment.careStatus,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
        }
    });

    return result;
  },
  deleteAppointment: function(id, patientId) {
    throwIfNotFamilyOrAdmin(patientId);
    throwIfInThePast(id, null);
    throwIfStarted(id, null);

    var recurringAppointment = RecurringAppointments.findOne({_id: id});
    if (recurringAppointment) {
      throw new Meteor.Error("You can't delete a recurring appointment.");
    }

    var appointment = Appointments.findOne(id);

    if (!appointment) {
      throw new Meteor.Error("Could not find the appointment.");
    }

    var result = Tasks.remove({ appointmentId: id });

    result = Comments.remove({ parentId: id });

    result = Appointments.remove({ _id: id });

    return !result.writeError;
  },
  setCareStatus: function(id, patientId, careStatus) {
    throwIfNotFamilyOrAdminOrCarer(patientId);
    throwIfInThePast(id, null);

    var appointment = Appointments.findOne(id);

    if (!appointment) {
      throw new Meteor.Error("Could not find the appointment.");
    }

    var result = Appointments.update(
      { _id: appointment._id },
      {
        $set: {
          careStatus: careStatus
        }
    });

    var patient = Patients.findOne({ _id: appointment.patientId });

    return result;
  },
  startAppointment: function(id, patientId) {
    throwIfNotFamilyOrAdminOrCarer(patientId);
    throwIfInThePast(id, null);

    var appointment = Appointments.findOne(id);

    if (!appointment) {
      throw new Meteor.Error("Could not find the appointment.");
    }

    var result = Appointments.update(
      { _id: appointment._id },
      {
        $set: {
          actualStartDate: moment().toDate()
        }
    });

    var patient = Patients.findOne({ _id: appointment.patientId });

    var userRoles = parseRoles(Meteor.user().roles);
    var isFamilyMember = _.some(userRoles, function(value) {
      return value.role === 'family';
    });

    KonnektisNotifications.notifyFamilyMembers(
      appointment.patientId,
      Meteor.user().username,
      patient.name + ' – ' + KonnektisNotifications.getAppointmentTypeLabel(appointment.name),
      Meteor.user().username + ', ' + (isFamilyMember ? 'family member,' : 'carer,') + ' has started an appointment at ' + moment.tz('Europe/London').format('HH:mm'));

    return result;
  },
  completeAppointment: function(id, patientId) {
    throwIfNotFamilyOrAdminOrCarer(patientId);
    throwIfInThePast(id, null);

    var appointment = Appointments.findOne(id);

    if (!appointment) {
      throw new Meteor.Error("Could not find the appointment.");
    }

    var update = Appointments.update(
      { _id: appointment._id },
      {
        $set: {
          actualEndDate: moment().toDate()
        }
    });

    var patient = Patients.findOne({ _id: appointment.patientId });

    var userRoles = parseRoles(Meteor.user().roles);
    var isFamilyMember = _.some(userRoles, function(value) {
      return value.role === 'family';
    });

    var careStatus;
    switch (appointment.careStatus) {
      case '0': careStatus = 'Bad'; break;
      case '1': careStatus = 'Ok'; break;
      case '2': careStatus = 'Good'; break;
      default: careStatus = 'None set'; break;
    }

    var comments = Comments.find({
      $and: [
        { patientId: appointment.patientId },
        { parentId: appointment._id },
        { urgent: true }
      ]
    });

    var urgentCommentCount = comments.count();

    var urgentCommentText = '';
    _.each(comments.fetch(), function(comment) {
      urgentCommentText = urgentCommentText + comment.commentText + '\n';
    });

    KonnektisNotifications.notifyFamilyMembers(
      appointment.patientId,
      Meteor.user().username,
      patient.name + ' – ' + KonnektisNotifications.getAppointmentTypeLabel(appointment.name),
      Meteor.user().username
        + ', ' + (isFamilyMember ? 'family member,' : 'carer,')
        + ' has completed an appointment at '
        + moment.tz('Europe/London').format('HH:mm')
        + '. Care status: ' + careStatus + ', ' + urgentCommentCount + ' urgent comment(s).');

    KonnektisNotifications.notifyAppointmentEndSummary(
      appointment.patientId,
      'CLIENT: '
        + KonnektisPatient.getDisplayName(appointment.patientId) + '\n'
        + 'CARER: '
        + KonnektisCarer.getDisplayName(Meteor.userId()) + '\n'
        + 'VISIT: '
        // + KonnektisAppointment.shortDescription(appointment) + '\n'
        + appointment.description + '\n'
        + 'URGENT COMMENTS: '
        + urgentCommentCount + '\n'
        + urgentCommentText  // new line ending will already be present from above
        + 'CARE STATUS: '
        + careStatus
        );

    return update;
  },
});


  // close if (Meteor.isServer)
}
