if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId)])) {
    throw new Meteor.Error("You aren't authorised to edit recurring tasks for that patient");
  }
}

function throwIfAppointmentNotFound(recurringTask) {
  if (recurringTask.recurrenceUnit === 'appointment') {
    var appointment = RecurringAppointments.findOne(recurringTask.recurringAppointmentId);

    if (!appointment) {
      throw new Meteor.Error("Cannot find the specified appointment");
    }
  }
}

function equals(a, b) {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false;
  }

  return a === b;
}

function throwIfInThePast(recurringTaskId, recurringTask) {
  var startOfToday = moment.utc().startOf('day');

  var a = RecurringTasks.findOne(recurringTaskId);

  if (a) {
    if (moment(a.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot edit past events');
    }

    if (moment(a.recurrenceStartDay).isBefore(startOfToday)) {
      if (recurringTask.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
        || !equals(recurringTask.recurringAppointmentId, a.recurringAppointmentId)
        || !equals(recurringTask.name, a.name)
        || !equals(recurringTask.description, a.description)
        || !equals(recurringTask.carerUsername, a.carerUsername)
        || !equals(recurringTask.startTime, a.startTime)
        || !equals(recurringTask.endTime, a.endTime)
        || !equals(recurringTask.recurrenceUnit, a.recurrenceUnit)) {
        throw new Meteor.Error('Cannot edit past events');
      }
    }

    if (recurringTask.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
      && moment(recurringTask.recurrenceStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }

    if (recurringTask.recurrenceEndDay.toISOString() != a.recurrenceEndDay.toISOString()
      && moment(recurringTask.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  } else {
    if (moment(recurringTask.recurrenceStartDay).isBefore(startOfToday) || moment(recurringTask.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  }
}

function throwIfAppointmentInPast(appointmentId) {
  var startOfToday = moment.utc().startOf('day');

  if (appointmentId) {
    var appointment = RecurringAppointments.findOne(appointmentId);

    if (moment(appointment.recurrenceStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create tasks for past appointments');
    }
  }
}

function throwIfInvalidStartOrEndTimes(recurringTask) {
  if (recurringTask.recurrenceEndDay < recurringTask.recurrenceStartDay) {
    throw new Meteor.Error('Start days must preceed end days');
  }

  if (recurringTask.endTime < recurringTask.startTime) {
    throw new Meteor.Error('Start times must preceed end times');
  }
}

function throwIfOutsideBoundsOfAppointment(task, appointmentId) {
  if (appointmentId) {
    var appointment = RecurringAppointments.findOne(appointmentId);

    if (moment(task.recurrenceStartDay).startOf('day').toDate() < moment(appointment.recurrenceStartDay).startOf('day').toDate()
      || moment(task.recurrenceEndDay).startOf('day').toDate() > moment(appointment.recurrenceEndDay).startOf('day').toDate()
      || task.startTime < appointment.startTime
      || task.endTime > appointment.endTime) {
        throw new Meteor.Error('Task must fall within the bounds of their appointment');
      }
  }
}

Meteor.methods({
  createRecurringTask: function (recurringTask) {
    throwIfUnauthorised(recurringTask.patientId);
    throwIfAppointmentNotFound(recurringTask);
    throwIfAppointmentInPast(recurringTask.recurringAppointmentId);
    throwIfInThePast(null, recurringTask);
    throwIfInvalidStartOrEndTimes(recurringTask);
    throwIfOutsideBoundsOfAppointment(recurringTask, recurringTask.recurringAppointmentId);

    if(!recurringTask.taskOrder) {
      var numTasks = RecurringTasks.find({recurringAppointmentId: recurringTask.recurringAppointmentId}).count();
      var maxTaskOrderTask = RecurringTasks.findOne({recurringAppointmentId: recurringTask.recurringAppointmentId}, {sort: {taskOrder: -1}, limit: 1});
      var taskOrder = numTasks + 1;

      if(maxTaskOrderTask && maxTaskOrderTask.taskOrder > numTasks) {
        taskOrder = maxTaskOrderTask.taskOrder + 1;
      }

      recurringTask.taskOrder = taskOrder;
    }

    var id = RecurringTasks.insert({
      patientId: recurringTask.patientId,
      recurringAppointmentId: recurringTask.recurringAppointmentId,
      name: recurringTask.name,
      description: recurringTask.description,
      carerUsername: recurringTask.carerUsername,
      recurrenceStartDay: recurringTask.recurrenceStartDay,
      recurrenceEndDay: recurringTask.recurrenceEndDay,
      startTimeType: recurringTask.startTimeType,
      endTimeType: recurringTask.endTimeType,
      startTime: recurringTask.startTime,
      endTime: recurringTask.endTime,
      recurrenceUnit: recurringTask.recurrenceUnit,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username,
      taskOrder: recurringTask.taskOrder
    });

    return id;
  },
  updateRecurringTask: function(recurringTask) {
    throwIfUnauthorised(recurringTask.patientId);
    throwIfAppointmentNotFound(recurringTask);
    throwIfAppointmentInPast(recurringTask.recurringAppointmentId);
    throwIfInThePast(recurringTask._id, recurringTask);
    throwIfInvalidStartOrEndTimes(recurringTask);
    throwIfOutsideBoundsOfAppointment(recurringTask, recurringTask.recurringAppointmentId);

    var result = RecurringTasks.update(
      { _id: recurringTask._id },
      {
        $set: {
          patientId: recurringTask.patientId,
          recurringAppointmentId: recurringTask.recurringAppointmentId,
          name: recurringTask.name,
          description: recurringTask.description,
          carerUsername: recurringTask.carerUsername,
          recurrenceStartDay: recurringTask.recurrenceStartDay,
          recurrenceEndDay: recurringTask.recurrenceEndDay,
          startTimeType: recurringTask.startTimeType,
          endTimeType: recurringTask.endTimeType,
          startTime: recurringTask.startTime,
          endTime: recurringTask.endTime,
          recurrenceUnit: recurringTask.recurrenceUnit,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  },
  endRecurringTaskPreviousDay: function(recurringTask, currentDay) {
    throwIfUnauthorised(recurringTask.patientId);

    if (!currentDay) {
      throw new Meteor.Error('Current day must have a value');
    }

    var newEnd = moment(currentDay).toDate();
    newEnd.setDate(newEnd.getDate()-1);

    var result = RecurringTasks.update(
      { _id: recurringTask._id },
      {
        $set: {
          recurrenceEndDay: newEnd,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  }
});


  // close if (Meteor.isServer)
}
