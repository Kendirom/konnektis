/*
  lib/methods/admin-care-plan.js
*/

if (Meteor.isServer) {

function throwIfUnauthorised(patientId, roles) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), roles)) {
    throw new Meteor.Error("You aren't authorised to perform that function for the patient");
  }
}

function throwIfNotFamilyOrAdminOrCarer(patientId) {
  throwIfUnauthorised(patientId, ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])
}

Meteor.methods({
  setClientInfo: function (patientId, patientInfo, changeText) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var logResult = PatientInfoLog.insert({
        patientId: patientId,
        patientInfo: patientInfo,
        changeText: changeText,
        updatedUsername: Meteor.user().username,
        updatedTime: moment().toDate()
    });

    var result = PatientInfo.update(
      { patientId: patientId },
      {
        $set: {
          patientInfo: patientInfo,
          changeText: changeText,
          updatedUsername: Meteor.user().username,
          updatedTime: moment().toDate()
        }
      }, {upsert: true});

    return result;
  },

  setRiskAssessment: function (patientId, riskAssessment, changeText) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var logResult = RiskAssessmentsLog.insert({
        patientId: patientId,
        riskAssessment: riskAssessment,
        changeText: changeText,
        updatedUsername: Meteor.user().username,
        updatedTime: moment().toDate()
    });

    var result = RiskAssessments.update(
      { patientId: patientId },
      {
        $set: {
          riskAssessment: riskAssessment,
          changeText: changeText,
          updatedUsername: Meteor.user().username,
          updatedTime: moment().toDate()
        }
      }, {upsert: true});

    return result;
  },

  setCarePlan: function (patientId, carePlanSection, carePlanText, changeText) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var criteria = {
      patientId: patientId
    };

    criteria[carePlanSection] = carePlanText;

    var newFields = {
      updatedUsername: Meteor.user().username,
      updatedTime: moment().toDate()
    };

    newFields[carePlanSection + 'ChangeText'] = changeText;

    var logResult = CarePlansLog.insert(criteria.concat(newFields));

    var result = CarePlans.update(
      criteria,
      { $set: newFields },
      { upsert: true });

    return result;
  },

  setWholeCarePlan: function (patientId, carePlanText) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var criteria = {
      patientId: patientId
    };

    var newFields = {
      updatedUsername: Meteor.user().username,
      updatedTime: moment().toDate()
    };

    newFields['carePlan'] = carePlanText[0];
    newFields['medication'] = carePlanText[1];
    newFields['supportPlan'] = carePlanText[2];

    newFields['carePlanChanges'] = carePlanText[3];

    var insertFields = _.extend({}, criteria);
    _.extend(insertFields, newFields);

    var logResult = CarePlansLog.insert(insertFields);

    var result = CarePlans.update(
      criteria,
      { $set: newFields },
      { upsert: true });

    return result;
  },
});


  // close if (Meteor.isServer)
}
