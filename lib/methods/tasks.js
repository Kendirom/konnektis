if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])) {
    throw new Meteor.Error("You aren't authorised to create tasks for that patient");
  }
}

function throwIfAppointmentNotFound(appointmentId) {
  if (appointmentId) {
    var appointment = Appointments.findOne(appointmentId);

    if (!appointment) {
      throw new Meteor.Error("Cannot find the specified appointment");
    }
  }

  return appointment;
}

function throwIfCompleteUnauthorised(patientId, taskId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])) {
    throw new Meteor.Error("You aren't authorised to update tasks for that patient");
  }

  if(taskId) {
    var task = Tasks.findOne(taskId);

    if (!task) {
      throw new Meteor.Error("Task not found");
    }

    if(!(task.carerUsername === Meteor.user().username || !task.carerUsername)) {
      throw new Meteor.Error("You aren't authorised to update this task");
    }
  }
}

function throwIfAppointmentInPast(appointmentId, appointment) {
  var startOfToday = moment.utc().startOf('day');

  if (appointmentId) {
    if(appointment == undefined) {
      appointment = Appointments.findOne(appointmentId);
    }

    if (moment(appointment.scheduledStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create tasks for past appointments');
    }
  }
}

function throwIfInThePast(taskId, task) {
  var startOfToday = moment().startOf('day');

  var a = Tasks.findOne(taskId);
  if (a && moment(a.scheduledStartDay).isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot edit past events');
  }

  if (task && moment(task.scheduledStartDay).isBefore(startOfToday)) {
    throw new Meteor.Error('Cannot create events in the past');
  }
}

function throwIfSpansTooManyDays(task) {
  var taskMaximumLengthInHours = 23;

  var from = combineDayAndTime(task.scheduledStartDay, task.scheduledStartTime);
  var to = combineDayAndTime(task.scheduledEndDay, task.scheduledEndTime);
  if (to.diff(from, 'hours') > taskMaximumLengthInHours) {
    throw new Meteor.Error('Tasks cannot span more than 24 hours');
  }
}

function parseTimeString(timeString) {
  if (!timeString) {
    return null;
  }

  var segments = timeString.split(':');

  return {
    hours: segments[0], minutes: segments[1]
  };
}

function combineDayAndTime(day, timeString) {
  if (!day) {
    throw new Meteor.Error('Day cannot be null');
  }
  var date = moment(day);
  var time = parseTimeString(timeString);
  if (time) {
    date.hours(time.hours);
    date.minutes(time.minutes);
    date.seconds(0);
    date.milliseconds(0);
  } else {
    date.hours(0);
    date.minutes(0);
    date.seconds(0);
    date.milliseconds(0);
  }
  return date;
}

function throwIfInvalidStartOrEndTimes(task) {
  var startDate = combineDayAndTime(task.scheduledStartDay, task.scheduledStartTime);
  var endDate = combineDayAndTime(task.scheduledEndDay, task.scheduledEndTime);

  if (endDate.isBefore(startDate)) {
    throw new Meteor.Error('Start days must preceed end days');
  }
}

function throwIfOutsideBoundsOfAppointment(task, appointmentId, appointment) {
  if (appointmentId) {
    if(appointment == undefined) {
      appointment = Appointments.findOne(appointmentId);
    }

    var taskStartDate = combineDayAndTime(task.scheduledStartDay, task.scheduledStartTime);
    var taskEndDate = combineDayAndTime(task.scheduledEndDay, task.scheduledEndTime);

    var appointmentStartDate = combineDayAndTime(appointment.scheduledStartDay, appointment.scheduledStartTime);
    var appointmentEndDate = combineDayAndTime(appointment.scheduledEndDay, appointment.scheduledEndTime);

    if (taskStartDate.isBefore(appointmentStartDate) || taskEndDate.isAfter(appointmentEndDate)) {
      throw new Meteor.Error('Task must fall within the bounds of their appointment');
    }
  }
}

Meteor.methods({
  createTask: function (task) {
    throwIfUnauthorised(task.patientId);
    throwIfAppointmentNotFound(task.appointmentId);
    throwIfAppointmentInPast(task.appointmentId);
    throwIfInThePast(null, task);
    throwIfInvalidStartOrEndTimes(task);
    throwIfOutsideBoundsOfAppointment(task, task.appointmentId);
    throwIfSpansTooManyDays(task);

    if(!task.taskOrder) {
      var numTasks = Tasks.find({appointmentId: task.appointmentId}).count();
      var maxTaskOrderTask = Tasks.findOne({appointmentId: task.appointmentId}, {sort: {taskOrder: -1}, limit: 1});
      var taskOrder = numTasks + 1;

      if(maxTaskOrderTask && maxTaskOrderTask.taskOrder > numTasks) {
        taskOrder = maxTaskOrderTask.taskOrder + 1;
      }

      task.taskOrder = taskOrder;
    }

    var id = Tasks.insert({
      patientId: task.patientId,
      appointmentId: task.appointmentId,
      recurringTaskId: task.recurringTaskId,
      name: task.name,
      description: task.description,
      scheduledStartDay: task.scheduledStartDay,
      scheduledStartTime: task.scheduledStartTime,
      scheduledStartTimeType: task.scheduledStartTimeType,
      scheduledEndDay: task.scheduledEndDay,
      scheduledEndTime: task.scheduledEndTime,
      scheduledEndTimeType: task.scheduledEndTimeType,
      actualStartDate: null,
      actualEndDate: null,
      carerUsername: task.carerUsername,
      isEvent: task.isEvent,
      careStatus: null,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username,
      taskOrder: task.taskOrder
    });

    return id;
  },
  updateTask: function(task) {
    throwIfUnauthorised(task.patientId);
    var appointment = throwIfAppointmentNotFound(task.appointmentId);
    throwIfAppointmentInPast(task.appointmentId, appointment);
    throwIfInThePast(task._id, task);
    throwIfInvalidStartOrEndTimes(task);
    throwIfOutsideBoundsOfAppointment(task, task.appointmentId, appointment);
    throwIfSpansTooManyDays(task);

    var result = Tasks.update(
      { _id: task._id },
      {
        $set: {
          appointmentId: task.appointmentId,
          recurringTaskId: task.recurringTaskId,
          name: task.name,
          description: task.description,
          scheduledStartDay: task.scheduledStartDay,
          scheduledStartTime: task.scheduledStartTime,
          scheduledStartTimeType: task.scheduledStartTimeType,
          scheduledEndDay: task.scheduledEndDay,
          scheduledEndTime: task.scheduledEndTime,
          scheduledEndTimeType: task.scheduledEndTimeType,
          actualStartDate: task.actualStartDate,
          actualEndDate: task.actualEndDate,
          carerUsername: task.carerUsername,
          isEvent: task.isEvent,
          careStatus: task.careStatus,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
        }
    });

    return result;
  },
  deleteTask: function(id, patientId) {
    throwIfUnauthorised(patientId);
    throwIfInThePast(id, null);

    var a = RecurringTasks.findOne(id);

    if (a) {
      throw new Meteor.Error("You can't delete a recurring task.");
    }

    var task = Tasks.findOne(id);

    if (!task) {
      throw new Meteor.Error("Could not find the task.");
    }

    var result = Comments.remove({ parentId: id });

    result = Tasks.remove({ _id: id });

    return !result.writeError;
  },
  completeTask: function(task) {
    throwIfCompleteUnauthorised(task.patientId, task._id);
    var appointment = throwIfAppointmentNotFound(task.appointmentId);
    throwIfInThePast(task._id, task, appointment);

    var result = Tasks.update(
      { _id: task._id },
      {
        $set: {
          actualEndDate: moment().toDate(),
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
          completedUsername: Meteor.user().username,
        }
    });

    // var patient = Patients.findOne({ _id: task.patientId });

    // return result;
  },
  uncompleteTask: function(task) {
    throwIfCompleteUnauthorised(task.patientId, task._id);
    throwIfAppointmentNotFound(task.appointmentId);
    throwIfInThePast(task._id, task);

    var result = Tasks.update(
      { _id: task._id },
      {
        $set: {

          actualEndDate: null,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username,
        }
    });

    var patient = Patients.findOne({ _id: task.patientId });

    return result;
  }
});


  // close if (Meteor.isServer)
}
