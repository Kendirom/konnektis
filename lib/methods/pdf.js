// lib/methods/pdf.js

Meteor.methods({
  'generate_appointment_pdf': function(appointmentId) {
    if (Meteor.isServer) {

      var getUserDisplayName = function(username) {
        var displayUsername;
        var user = Meteor.users.findOne({username: username});

        if(user) {
          if (user.profile && (user.profile['first-name'] || user.profile['last-name'])) {
            var usernameParts = [];

            if(user.profile['first-name']) {
              usernameParts.push(user.profile['first-name']);
            }

            if(user.profile['last-name']) {
              usernameParts.push(user.profile['last-name']);
            }

            displayUsername = usernameParts.join(' ');
          }
          else {
            displayUsername = user.username;
          }
        }

        return displayUsername;
      };

      var addUserDisplayNameToComments = function(comments) {
        var updatedComments = [];
        for (var i = 0; i < comments.length; i++) {
          var comment = comments[i];
          comment.usersName = getUserDisplayName(comment.username);
          updatedComments.push(comment);
        }
        return updatedComments;
      };

      var addDisplayTimestampToComments = function(comments) {
        var updatedComments = [];
        for (var i = 0; i < comments.length; i++) {
          var comment = comments[i];
          comment.displayTimestamp = Konnektis.formatCommentDateTime(Konnektis.getDateFromUtc(comment.timestamp));
          updatedComments.push(comment);
        }
        return updatedComments;
      }

      var expandCommentsDetailsForDisplay = function(comments) {
        return addDisplayTimestampToComments(addUserDisplayNameToComments(comments));
      }


      // SETUP
      // Grab required packages
      var webshot = Meteor.npmRequire('webshot');
      var fs      = Npm.require('fs');
      var Future = Npm.require('fibers/future');

      var fut = new Future();

      var fileName = "report.pdf";

      // GENERATE HTML STRING
      var css = Assets.getText('appointment/bootstrap.min.css');
      var appointmentCss = Assets.getText('appointment/appointment.css');

      SSR.compileTemplate('layout', Assets.getText('appointment/layout.html'));

      Template.layout.helpers({
        getDocType: function() {
          return "<!DOCTYPE html>";
        }
      });

      SSR.compileTemplate('appointment', Assets.getText('appointment/appointment.html'));
      SSR.compileTemplate('remainingTask', Assets.getText('appointment/remaining_task.html'));
      SSR.compileTemplate('completedTask', Assets.getText('appointment/completed_task.html'));
      SSR.compileTemplate('comment', Assets.getText('appointment/comment.html'));

      Template.comment.helpers({
        lineBreakString: function (str) {
          if(str == undefined) {
            return str;
          }
          else {
            return new Handlebars.SafeString(str.replace(/(\r\n|\n\r|\r|\n)/g, "<br />"));
          }
        },
      });

      var appointment = Appointments.findOne(appointmentId);

      if(appointment) {
        appointment.carerDisplayName = getUserDisplayName(appointment.carerUsername);

        var patient = Patients.findOne(appointment.patientId);

        if(patient) {
          if(!patient.displayName && patient.name) {
            patient.displayName = patient.name;
          }
        }

        appointment.patient = patient;

        appointment.scheduledDateTime = Konnektis.scheduledLabelString(appointment, true, true);

        if(appointment.actualEndDate) {
          appointment.completedDateTime = Konnektis.completedLabelString(appointment, this.date, true, true);
        }

        var remainingTasks = Tasks.find({
          $and: [
            { appointmentId: appointmentId },
            { actualEndDate: null }
          ]
        }).fetch();

        appointment.remainingTasks = [];

        for (var i = 0; i < remainingTasks.length; i++) {
          var task = remainingTasks[i];
          var comments = Comments.find({ parentId: task._id }).fetch();

          task.comments = expandCommentsDetailsForDisplay(comments);

          appointment.remainingTasks.push(task);
        }


        var completedTasks = Tasks.find({
          $and: [
            { appointmentId: appointmentId },
            { actualEndDate: { $ne: null } }
          ]
        }).fetch();

        appointment.completedTasks = [];

        for (var i = 0; i < completedTasks.length; i++) {
          var task = completedTasks[i];
          var comments = Comments.find({ parentId: task._id }).fetch();

          task.comments = expandCommentsDetailsForDisplay(comments);

          appointment.completedTasks.push(task);
        }


        var comments = Comments.find({ parentId: appointmentId }).fetch();

        appointment.comments = expandCommentsDetailsForDisplay(comments);
      }


      // PREPARE DATA
      var data = {
        appointment: appointment
      }

      var html_string = SSR.render('layout', {
        css: css + appointmentCss,
        template: "appointment",
        data: data
      });

      // Setup Webshot options
      var options = {
          //renderDelay: 2000,
          "paperSize": {
              "format": "A4",
              "orientation": "portrait",
              "margin": "2cm"
          },
          siteType: 'html'
      };

      // Commence Webshot
      console.log("Commencing webshot...");
      webshot(html_string, fileName, options, function(err) {
          fs.readFile(fileName, function (err, data) {
              if (err) {
                  return console.log(err);
              }

              fs.unlinkSync(fileName);
              fut.return(data);

          });
      });

      pdfData = fut.wait();
      base64String = new Buffer(pdfData).toString('base64');

      return base64String;
    }
  },
});
