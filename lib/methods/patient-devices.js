/*
  lib/methods/patient-devices.js
*/

if (Meteor.isServer) {

Meteor.methods({
  assignPatientDevice: function (patientId, patientDeviceId) {
    var doInsert = true;
    var foundPatientDevice = PatientDevices.findOne(patientDeviceId);

    if (foundPatientDevice) {
      doInsert = false;
    }

    if (doInsert) {
      console.log('inserting');
      patientDeviceId = PatientDevices.insert({
        patientId: patientId
      });
    }
    else {
      console.log('updating');
      var result = PatientDevices.update(
        { _id: patientDeviceId },
        {
          $set: {
            patientId: patientId
          }
        });
    }

    // make an audit log record of the change

    var patientDeviceLogId = PatientDevicesLog.insert({
      patientDeviceId: patientDeviceId,
      patientId: patientId,
      createdUsername: Meteor.user().username,
      createdTime: moment().toDate()
    });

    return patientDeviceId;
  },

  clearPatientDevice: function (patientDeviceId) {
    PatientDevices.remove(patientDeviceId);

    // make an audit log record of the change

    var patientDeviceLogId = PatientDevicesLog.insert({
      patientDeviceId: patientDeviceId,
      patientId: null,
      createdUsername: Meteor.user().username,
      createdTime: moment().toDate()
    });
  },
});


  // close if (Meteor.isServer)
}
