if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])) {
    throw new Meteor.Error("You aren't authorised to create comments for that patient");
  }
}

Meteor.methods({
  createComment: function (comment) {
    throwIfUnauthorised(comment.patientId);

    var startOfToday = moment.utc().startOf('day');
    var relevantDate = moment.utc();

    if (comment.parentType === 'appointment') {
      var appointment = Appointments.findOne(comment.parentId);
      if (!appointment) {
        throw new Meteor.Error('Cannot find the appointment');
      }
      else {
        relevantDate = appointment.scheduledStartDay;
      }

      if (moment(appointment.scheduledStartDay).isBefore(startOfToday)) {
        throw new Meteor.Error('Cannot edit past events');
      }
      if (appointment.patientId !== comment.patientId) {
        throw new Meteor.Error('Not currently in the context of that patient, please switch context to perform the action');
      }
    }
    else if (comment.parentType === 'task') {
      var task = Tasks.findOne(comment.parentId);
      if (!task) {
        throw new Meteor.Error('Cannot find the task');
      }
      else {
        relevantDate = task.scheduledStartDay;
      }

      if (moment(task.scheduledStartDay).isBefore(startOfToday)) {
        throw new Meteor.Error('Cannot edit past events');
      }
      if (task.patientId !== comment.patientId) {
        throw new Meteor.Error('Not currently in the context of that patient, please switch context to perform the action');
      }
    }
    else if (comment.parentType === 'medication') {
      var medication = Medications.findOne(comment.parentId);
      if (!medication) {
        throw new Meteor.Error('Cannot find the medication');
      }
      else {
        relevantDate = medication.scheduledStartDay;
      }

      if (moment(medication.scheduledStartDay).isBefore(startOfToday)) {
        throw new Meteor.Error('Cannot edit past events');
      }
      if (medication.patientId !== comment.patientId) {
        throw new Meteor.Error('Not currently in the context of that patient, please switch context to perform the action');
      }
    }

    var timestamp = moment.utc();
    var id = Comments.insert({
      patientId: comment.patientId,
      parentType: comment.parentType,
      parentId: comment.parentId,
      timestamp: timestamp.toDate(),
      username: comment.username,
      commentText: comment.commentText,
      urgent: comment.urgent,
      relevantDate: relevantDate,
      userType: comment.userType
    });

    var patient = Patients.findOne({ _id: comment.patientId });

    if (comment.urgent === true) {
      var commentName;

      switch (comment.parentType) {
        case 'appointment':
          commentName = KonnektisNotifications.getAppointmentTypeLabel(appointment.name);
          break;
        case 'task':
          commentName = task.name + ' Task';
          break;
        case 'medication':
          commentName = medication.name + ' Medication';
          break;
      }

      KonnektisNotifications.notifyFamilyMembers(
        comment.patientId,
        Meteor.user().username,
        patient.name + ' – ' + commentName,
        Meteor.user().username + ' left an urgent comment - ' + timestamp.tz('Europe/London').format('HH:mm') + ': ' + comment.commentText);
    }

    return id;
  }
});


  // close if (Meteor.isServer)
}
