/*
  lib/methods/carerPatientLastLogin.js
*/

if (Meteor.isServer) {

function throwIfUnauthorised(patientId, roles) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), roles)) {
    throw new Meteor.Error("You aren't authorised to perform that function for the patient");
  }
}

function throwIfNotFamilyOrAdminOrCarer(patientId) {
  throwIfUnauthorised(patientId, ['admin', buildRoleString('family', patientId), buildRoleString('carer', patientId)])
}

Meteor.methods({
  setCarerPatientLastLogin: function(patientId) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var result = CarerPatientLastLogins.update({
        carerId: Meteor.user()._id,
        patientId: patientId
      }, {
        $set: {lastLoginDate: moment().toDate()}
      }, {
        upsert: true
      });

    return result;
  },

  setCarerPatientInfoConfirm: function(patientId, patientDocument) {
    throwIfNotFamilyOrAdminOrCarer(patientId);

    var carerPatientLastLogin = CarerPatientLastLogins.findOne({
      carerId: Meteor.user()._id,
      patientId: patientId
    });

    var patientDocumentLastReviewDates = null;

    if(carerPatientLastLogin) {
      patientDocumentLastReviewDates = carerPatientLastLogin.patientDocumentLastReviewDates;
    }

    if(!patientDocumentLastReviewDates) {
      patientDocumentLastReviewDates = {};
    }

    patientDocumentLastReviewDates[patientDocument] = moment().toDate();

    var result = CarerPatientLastLogins.update({
        carerId: Meteor.user()._id,
        patientId: patientId
      }, {
        $set: {patientDocumentLastReviewDates: patientDocumentLastReviewDates}
      }, {
        upsert: true
      });

    return result;
  },
});


  // close if (Meteor.isServer)
}
