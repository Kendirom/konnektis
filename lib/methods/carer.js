KonnektisCarer = {};

(function (KonnektisCarer) {
  'use strict';

  KonnektisCarer.getDisplayName = function(carerId) {
    var carer = Meteor.users.findOne(carerId);
    if(carer) {
        if(carer.profile && (carer.profile['first-name'] || carer.profile['last-name'])) {
            var nameParts = [];

            if(carer.profile['first-name']) {
                nameParts.push(carer.profile['first-name']);
            }
            if(carer.profile['last-name']) {
                nameParts.push(carer.profile['last-name']);
            }

            return nameParts.join(' ');
        }
        else {
            return carer.username;
        }
    }
    return undefined;
  };

})(KonnektisCarer);
