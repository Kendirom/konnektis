if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId)])) {
    throw new Meteor.Error("You aren't authorised to edit recurring appointments for that patient");
  }
}

function equals(a, b) {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false;
  }

  return a === b;
}

function throwIfInThePast(recurringAppointmentId, recurringAppointment) {
  var startOfToday = moment.utc().startOf('day');

  var a = RecurringAppointments.findOne(recurringAppointmentId);

  if (a) {
    if (moment(a.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot edit past events');
    }

    if (moment(a.recurrenceStartDay).isBefore(startOfToday)) {
      if (recurringAppointment.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
        || !equals(recurringAppointment.name, a.name)
        || !equals(recurringAppointment.description, a.description)
        || !equals(recurringAppointment.carerUsername, a.carerUsername)
        || !equals(recurringAppointment.startTime, a.startTime)
        || !equals(recurringAppointment.endTime, a.endTime)
        || !equals(recurringAppointment.recurrenceUnit, a.recurrenceUnit)
        || !equals(recurringAppointment.reurrenceAmount, a.reurrenceAmount)) {
        throw new Meteor.Error('Cannot edit past events');
      }
    }

    if (recurringAppointment.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
      && moment(recurringAppointment.recurrenceStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }

    if (recurringAppointment.recurrenceEndDay.toISOString() != a.recurrenceEndDay.toISOString()
      && moment(recurringAppointment.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  } else {
    if (moment(recurringAppointment.recurrenceStartDay).isBefore(startOfToday) || moment(recurringAppointment.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  }
}

function throwIfInvalidStartOrEndTimes(recurringAppointment) {
  if (recurringAppointment.recurrenceEndDay < recurringAppointment.recurrenceStartDay) {
    throw new Meteor.Error('Start days must preceed end days');
  }

  if (recurringAppointment.endTime < recurringAppointment.startTime) {
    throw new Meteor.Error('Start times must preceed end times');
  }
}

Meteor.methods({
  createRecurringAppointment: function (recurringAppointment) {
    throwIfUnauthorised(recurringAppointment.patientId);
    throwIfInThePast(null, recurringAppointment);
    throwIfInvalidStartOrEndTimes(recurringAppointment);

    var id = RecurringAppointments.insert({
      patientId: recurringAppointment.patientId,
      name: recurringAppointment.name,
      description: recurringAppointment.description,
      carerUsername: recurringAppointment.carerUsername,
      recurrenceStartDay: recurringAppointment.recurrenceStartDay,
      recurrenceEndDay: recurringAppointment.recurrenceEndDay,
      startTime: recurringAppointment.startTime,
      endTime: recurringAppointment.endTime,
      recurrenceUnit: recurringAppointment.recurrenceUnit,
      recurrenceAmount: recurringAppointment.recurrenceAmount,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username
    });

    return id;
  },
  updateRecurringAppointment: function(recurringAppointment) {
    throwIfUnauthorised(recurringAppointment.patientId);
    throwIfInThePast(recurringAppointment._id, recurringAppointment);
    throwIfInvalidStartOrEndTimes(recurringAppointment);

    var result = RecurringAppointments.update(
      { _id: recurringAppointment._id },
      {
        $set: {
          patientId: recurringAppointment.patientId,
          name: recurringAppointment.name,
          description: recurringAppointment.description,
          carerUsername: recurringAppointment.carerUsername,
          recurrenceStartDay: recurringAppointment.recurrenceStartDay,
          recurrenceEndDay: recurringAppointment.recurrenceEndDay,
          startTime: recurringAppointment.startTime,
          endTime: recurringAppointment.endTime,
          recurrenceUnit: recurringAppointment.recurrenceUnit,
          recurrenceAmount: recurringAppointment.recurrenceAmount,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  },
  endRecurringAppointmentPreviousDay: function(recurringAppointment, currentDay) {
    throwIfUnauthorised(recurringAppointment.patientId);

    if (!currentDay) {
      throw new Meteor.Error('Current day must have a value');
    }

    var newEnd = moment(currentDay).toDate();
    newEnd.setDate(newEnd.getDate()-1);

    var result = RecurringAppointments.update(
      { _id: recurringAppointment._id },
      {
        $set: {
          recurrenceEndDay: newEnd,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  }
});


  // close if (Meteor.isServer)
}
