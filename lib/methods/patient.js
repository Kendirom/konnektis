KonnektisPatient = {};

(function (KonnektisPatient) {
  'use strict';

  KonnektisPatient.getDisplayName = function(patientId) {
    var patient = Patients.findOne(patientId);
    if(patient) {
        return patient.displayName || patient.name;
    }
    return undefined;
  };

})(KonnektisPatient);
