if (Meteor.isServer) {

function throwIfUnauthorised(patientId) {
  if (! Meteor.userId() || !patientId) {
    throw new Meteor.Error("Not authorised");
  }

  if (!Roles.userIsInRole(Meteor.userId(), ['admin', buildRoleString('family', patientId)])) {
    throw new Meteor.Error("You aren't authorised to edit recurring medications for that patient");
  }
}

function throwIfAppointmentNotFound(recurringMedication) {
  if (recurringMedication.recurrenceUnit === 'appointment') {
    var appointment = RecurringAppointments.findOne(recurringMedication.recurringAppointmentId);

    if (!appointment) {
      throw new Meteor.Error("Cannot find the specified appointment");
    }
  }
}

function equals(a, b) {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false;
  }

  return a === b;
}

function throwIfInThePast(recurringMedicationId, recurringMedication) {
  var startOfToday = moment.utc().startOf('day');

  var a = RecurringMedications.findOne(recurringMedicationId);

  if (a) {
    if (moment(a.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot edit past events');
    }

    if (moment(a.recurrenceStartDay).isBefore(startOfToday)) {
      if (recurringMedication.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
        || !equals(recurringMedication.recurringAppointmentId, a.recurringAppointmentId)
        || !equals(recurringMedication.name, a.name)
        || !equals(recurringMedication.description, a.description)
        || !equals(recurringMedication.carerUsername, a.carerUsername)
        || !equals(recurringMedication.startTime, a.startTime)
        || !equals(recurringMedication.endTime, a.endTime)
        || !equals(recurringMedication.recurrenceUnit, a.recurrenceUnit)) {
        throw new Meteor.Error('Cannot edit past events');
      }
    }

    if (recurringMedication.recurrenceStartDay.toISOString() != a.recurrenceStartDay.toISOString()
      && moment(recurringMedication.recurrenceStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }

    if (recurringMedication.recurrenceEndDay.toISOString() != a.recurrenceEndDay.toISOString()
      && moment(recurringMedication.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  } else {
    if (moment(recurringMedication.recurrenceStartDay).isBefore(startOfToday) || moment(recurringMedication.recurrenceEndDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create events in the past');
    }
  }
}

function throwIfAppointmentInPast(appointmentId) {
  var startOfToday = moment.utc().startOf('day');

  if (appointmentId) {
    var appointment = RecurringAppointments.findOne(appointmentId);

    if (moment(appointment.recurrenceStartDay).isBefore(startOfToday)) {
      throw new Meteor.Error('Cannot create medications for past appointments');
    }
  }
}

function throwIfInvalidStartOrEndTimes(recurringMedication) {
  if (recurringMedication.recurrenceEndDay < recurringMedication.recurrenceStartDay) {
    throw new Meteor.Error('Start days must preceed end days');
  }

  if (recurringMedication.endTime < recurringMedication.startTime) {
    throw new Meteor.Error('Start times must preceed end times');
  }
}

function throwIfOutsideBoundsOfAppointment(medication, appointmentId) {
  if (appointmentId) {
    var appointment = RecurringAppointments.findOne(appointmentId);

    if (moment(medication.recurrenceStartDay).startOf('day').toDate() < moment(appointment.recurrenceStartDay).startOf('day').toDate()
      || moment(medication.recurrenceEndDay).startOf('day').toDate() > moment(appointment.recurrenceEndDay).startOf('day').toDate()
      || medication.startTime < appointment.startTime
      || medication.endTime > appointment.endTime) {
        throw new Meteor.Error('Medication must fall within the bounds of their appointment');
      }
  }
}

Meteor.methods({
  createRecurringMedication: function (recurringMedication) {
    throwIfUnauthorised(recurringMedication.patientId);
    throwIfAppointmentNotFound(recurringMedication);
    throwIfAppointmentInPast(recurringMedication.recurringAppointmentId);
    throwIfInThePast(null, recurringMedication);
    throwIfInvalidStartOrEndTimes(recurringMedication);
    throwIfOutsideBoundsOfAppointment(recurringMedication, recurringMedication.recurringAppointmentId);

    if(!recurringMedication.taskOrder) {
      var numMedications = RecurringMedications.find({recurringAppointmentId: recurringMedication.recurringAppointmentId}).count();
      var maxTaskOrderMedication = RecurringMedications.findOne({recurringAppointmentId: recurringMedication.recurringAppointmentId}, {sort: {taskOrder: -1}, limit: 1});
      var taskOrder = numMedications + 1;

      if(maxTaskOrderMedication && maxTaskOrderMedication.taskOrder > numMedications) {
        taskOrder = maxTaskOrderMedication.taskOrder + 1;
      }

      recurringMedication.taskOrder = taskOrder;
    }

    var id = RecurringMedications.insert({
      patientId: recurringMedication.patientId,
      recurringAppointmentId: recurringMedication.recurringAppointmentId,
      name: recurringMedication.name,
      description: recurringMedication.description,
      carerUsername: recurringMedication.carerUsername,
      recurrenceStartDay: recurringMedication.recurrenceStartDay,
      recurrenceEndDay: recurringMedication.recurrenceEndDay,
      startTimeType: recurringMedication.startTimeType,
      endTimeType: recurringMedication.endTimeType,
      startTime: recurringMedication.startTime,
      endTime: recurringMedication.endTime,
      recurrenceUnit: recurringMedication.recurrenceUnit,
      lastModifiedTime: moment().toDate(),
      createdTime: moment().toDate(),
      lastModifiedUsername: Meteor.user().username,
      createdUsername: Meteor.user().username,
      taskOrder: recurringMedication.taskOrder
    });

    return id;
  },
  updateRecurringMedication: function(recurringMedication) {
    throwIfUnauthorised(recurringMedication.patientId);
    throwIfAppointmentNotFound(recurringMedication);
    throwIfAppointmentInPast(recurringMedication.recurringAppointmentId);
    throwIfInThePast(recurringMedication._id, recurringMedication);
    throwIfInvalidStartOrEndTimes(recurringMedication);
    throwIfOutsideBoundsOfAppointment(recurringMedication, recurringMedication.recurringAppointmentId);

    var result = RecurringMedications.update(
      { _id: recurringMedication._id },
      {
        $set: {
          patientId: recurringMedication.patientId,
          recurringAppointmentId: recurringMedication.recurringAppointmentId,
          name: recurringMedication.name,
          description: recurringMedication.description,
          carerUsername: recurringMedication.carerUsername,
          recurrenceStartDay: recurringMedication.recurrenceStartDay,
          recurrenceEndDay: recurringMedication.recurrenceEndDay,
          startTimeType: recurringMedication.startTimeType,
          endTimeType: recurringMedication.endTimeType,
          startTime: recurringMedication.startTime,
          endTime: recurringMedication.endTime,
          recurrenceUnit: recurringMedication.recurrenceUnit,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  },
  endRecurringMedicationPreviousDay: function(recurringMedication, currentDay) {
    throwIfUnauthorised(recurringMedication.patientId);

    if (!currentDay) {
      throw new Meteor.Error('Current day must have a value');
    }

    var newEnd = moment(currentDay).toDate();
    newEnd.setDate(newEnd.getDate()-1);

    var result = RecurringMedications.update(
      { _id: recurringMedication._id },
      {
        $set: {
          recurrenceEndDay: newEnd,
          lastModifiedTime: moment().toDate(),
          lastModifiedUsername: Meteor.user().username
        }
    });

    return result;
  }
});


  // close if (Meteor.isServer)
}
