(function() {
  var recurringTask = {
    recurringAppointmentId: {
      type: String,
      optional: true
    },
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    endTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'appointment'],
      autoform: {
        options: [
          { label: 'Day', value: 'day' },
          { label: 'Appointment', value: 'appointment' }
        ]
      }
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    carerUsername: {
      type: String,
      optional: true
    },
  }

  Schemas.RecurringTaskForm = new SimpleSchema(recurringTask);

  recurringTask._id = {
    type: String,
    optional: true,
  };
  recurringTask.patientId = {
    type: String,
    optional: false,
  };
  recurringTask.lastModifiedTime = {
    type: Date,
    optional: false
  };
  recurringTask.createdTime = {
    type: Date,
    optional: false
  };
  recurringTask.lastModifiedUsername = {
    type: String,
    optional: false
  };
  recurringTask.createdUsername = {
    type: String,
    optional: false
  };
  recurringTask.taskOrder = {
    type: Number,
    optional: false
  };

  Schemas.RecurringTask = new SimpleSchema(recurringTask);
})();
