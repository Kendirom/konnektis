(function() {
  var carePlan = {
    _id: {
      type: String,
      optional: true,
    },
    patientId: {
      type: String,
      optional: true
    },
    updatedUsername: {
      type: String,
      optional: false
    },
    updatedTime: {
      type: Date,
      optional: false
    },
    carePlan: {
      type: String,
      optional: true
    },
    encrypted: {
        type: Boolean,
        defaultValue: false,
        optional: true
    },
    medication: {
      type: String,
      optional: true
    },
    supportPlan: {
      type: String,
      optional: true
    },
    carePlanChanges: {
      type: String,
      optional: true
    }
  }

  Schemas.CarePlan = new SimpleSchema(carePlan);
})();
