(function() {
  var recurringMedication = {
    recurringAppointmentId: {
      type: String,
      optional: true
    },
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    endTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'appointment'],
      autoform: {
        options: [
          { label: 'Day', value: 'day' },
          { label: 'Appointment', value: 'appointment' }
        ]
      }
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    carerUsername: {
      type: String,
      optional: true
    },
  }

  Schemas.RecurringMedicationForm = new SimpleSchema(recurringMedication);

  recurringMedication._id = {
    type: String,
    optional: true,
  };
  recurringMedication.patientId = {
    type: String,
    optional: false,
  };
  recurringMedication.lastModifiedTime = {
    type: Date,
    optional: false
  };
  recurringMedication.createdTime = {
    type: Date,
    optional: false
  };
  recurringMedication.lastModifiedUsername = {
    type: String,
    optional: false
  };
  recurringMedication.createdUsername = {
    type: String,
    optional: false
  };
  recurringMedication.taskOrder = {
    type: Number,
    optional: false
  };

  Schemas.RecurringMedication = new SimpleSchema(recurringMedication);
})();
