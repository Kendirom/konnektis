(function() {
  var appointmentWithRecurrence = {
    name: {
      type: String,
      optional: false,
      label: "Appointment Type",
      allowedValues: ["agency", "volunteer", "family", "home", "outside"],
      autoform: {
        options: [
          { label: "Care Agency Visit", value: "agency" },
          { label: "Volunteer Care Visit", value: "volunteer" },
          { label: "Family Care Visit", value: "family" },
          { label: "Home Appointment", value: "home" },
          { label: "Outside Appointment", value: "outside" },
        ]
      }
    },
    description: {
      type: String,
      optional: true
    },
    carerUsername: {
      type: String,
      optional: false,
      label: "Carer"
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'week'],
      autoform: {
        options: [
          { label: 'Days', value: 'day' },
          { label: 'Weeks', value: 'week' }
        ]
      }
    },
    recurrenceAmount: {
      type: Number,
      min: 0,
      optional: false
    }
  }

  Schemas.AppointmentWithRecurrenceForm = new SimpleSchema(appointmentWithRecurrence);

  var appointment = {
    name: {
      type: String,
      optional: false,
      label: "Appointment Type",
      allowedValues: ["agency", "volunteer", "family", "home", "outside"],
      autoform: {
        options: [
          { label: "Agency Care Visit", value: "agency" },
          { label: "Volunteer Care Visit", value: "volunteer" },
          { label: "Family Care Visit", value: "family" },
          { label: "Home Appointment", value: "home" },
          { label: "Outside Appointment", value: "outside" },
        ]
      }
    },
    description: {
      type: String,
      optional: true
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledStartTime: {
      type: String,
      optional: true,
      label: "Start Time"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    scheduledEndTime: {
      type: String,
      optional: true,
      label: "End Time"
    },
    carerUsername: {
      type: String,
      optional: false,
      label: "Carer"
    }
  }

  Schemas.AppointmentForm = new SimpleSchema(appointment);

  appointment._id = {
    type: String,
    optional: true
  };
  appointment.patientId = {
    type: String,
    optional: false,
  };
  appointment.recurringAppointmentId = {
    type: String,
    optional: true
  };
  appointment.actualStartDate = {
    type: Date,
    optional: true
  };
  appointment.actualEndDate = {
    type: Date,
    optional: true
  };
  appointment.careStatus = {
    type: String,
    optional: true
  };
  appointment.lastModifiedTime = {
    type: Date,
    optional: false
  };
  appointment.createdTime = {
    type: Date,
    optional: false
  };
  appointment.lastModifiedUsername = {
    type: String,
    optional: false
  };
  appointment.createdUsername = {
    type: String,
    optional: false
  };

  Schemas.Appointment = new SimpleSchema(appointment);
})();
