(function() {
  var comment = {
    commentText: {
      type: String,
      optional: false,
    },
    urgent: {
      type: Boolean,
      optional: false
    }
  }

  Schemas.CommentForm = new SimpleSchema(comment);

  comment._id = {
    type: String,
    optional: true
  };
  comment.patientId = {
    type: String,
    optional: false
  };
  comment.parentType = {
    type: String,
    optional: false
  };
  comment.parentId = {
    type: String,
    optional: false
  };
  comment.timestamp = {
    type: Date,
    optional: false
  };
  comment.relevantDate = {
    type: Date,
    optional: false
  };
  comment.username = {
    type: String,
    optional: false
  };
  comment.userType = {
    type: String,
    optional: false
  };

  Schemas.Comment = new SimpleSchema(comment);
})();
