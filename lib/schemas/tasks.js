(function() {
  var taskWithRecurrence = {
    recurringAppointmentId: {
      type: String,
      optional: true
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledStartTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledStartTime: {
      type: String,
      optional: true,
      label: "Start Time"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    scheduledEndTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledEndTime: {
      type: String,
      optional: true,
      label: "End Time"
    },
    carerUsername: {
      type: String,
      optional: true,
      label: "Carer"
    },
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    endTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'appointment'],
      autoform: {
        options: [
          { label: 'Day', value: 'day' },
          { label: 'Appointment', value: 'appointment' }
        ]
      }
    }
  }

  Schemas.TaskWithRecurrenceForm = new SimpleSchema(taskWithRecurrence);

  var task = {
    recurringTaskId: {
      type: String,
      optional: true
    },
    appointmentId: {
      type: String,
      optional: true
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledStartTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledStartTime: {
      type: String,
      optional: true,
      label: "Start Time"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    scheduledEndTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledEndTime: {
      type: String,
      optional: true,
      label: "End Time"
    },
    carerUsername: {
      type: String,
      optional: true,
      label: "Carer"
    },
    completedUsername: {
      type: String,
      optional: true,
      label: "Completed By"
    }
  }

  Schemas.TaskForm = new SimpleSchema(task);

  task._id = {
    type: String,
    optional: true,
  };
  task.patientId = {
    type: String,
    optional: false,
  };
  task.recurringTaskId = {
    type: String,
    optional: true
  };
  task.actualStartDate = {
    type: Date,
    optional: true
  };
  task.actualEndDate = {
    type: Date,
    optional: true
  };
  task.isEvent = {
    type: Boolean,
    optional: false
  },
    task.careStatus = {
      type: String,
      optional: true
    };
  task.lastModifiedTime = {
    type: Date,
    optional: false
  };
  task.createdTime = {
    type: Date,
    optional: false
  };
  task.lastModifiedUsername = {
    type: String,
    optional: false
  };
  task.createdUsername = {
    type: String,
    optional: false
  };
  task.taskOrder = {
    type: Number,
    optional: false
  };

  Schemas.Task = new SimpleSchema(task);
})();
