(function() {
  var medicationWithRecurrence = {
    recurringAppointmentId: {
      type: String,
      optional: true
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledStartTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledStartTime: {
      type: String,
      optional: true,
      label: "Start Time"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    scheduledEndTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledEndTime: {
      type: String,
      optional: true,
      label: "End Time"
    },
    carerUsername: {
      type: String,
      optional: true,
      label: "Carer"
    },
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    endTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"}
        ]
      }
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'appointment'],
      autoform: {
        options: [
          { label: 'Day', value: 'day' },
          { label: 'Appointment', value: 'appointment' }
        ]
      }
    }  }

  Schemas.MedicationWithRecurrenceForm = new SimpleSchema(medicationWithRecurrence);

  var medication = {
    recurringMedicationId: {
      type: String,
      optional: true
    },
    appointmentId: {
      type: String,
      optional: true
    },
    name: {
      type: String,
      optional: false,
      label: "Title"
    },
    description: {
      type: String,
      optional: true
    },
    scheduledStartDay: {
      type: Date,
      optional: false,
      label: "Start Day"
    },
    scheduledStartTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledStartTime: {
      type: String,
      optional: true,
      label: "Start Time"
    },
    scheduledEndDay: {
      type: Date,
      optional: false,
      label: "End Day"
    },
    scheduledEndTimeType: {
      type: String,
      optional: true,
      allowedValues: ["specific", "partOfDay"],
      autoform: {
        options: [
          {label: "At a Specific Time", value: "specific"},
          {label: "During Part of the Day", value: "partOfDay"}
        ]
      }
    },
    scheduledEndTime: {
      type: String,
      optional: true,
      label: "End Time"
    },
    carerUsername: {
      type: String,
      optional: true,
      label: "Carer"
    },
    completedUsername: {
      type: String,
      optional: true,
      label: "Completed By"
    },
    taken: {
      type: String,
      optional: true
    },
    location: {
      type: String,
      optional: true
    }
  }

  Schemas.MedicationForm = new SimpleSchema(medication);

  medication._id = {
    type: String,
    optional: true,
  };
  medication.patientId = {
    type: String,
    optional: false,
  };
  medication.recurringMedicationId = {
    type: String,
    optional: true
  };
  medication.actualStartDate = {
    type: Date,
    optional: true
  };
  medication.actualEndDate = {
    type: Date,
    optional: true
  };
  medication.isEvent = {
    type: Boolean,
    optional: false
  },
    medication.careStatus = {
      type: String,
      optional: true
    };
  medication.lastModifiedTime = {
    type: Date,
    optional: false
  };
  medication.createdTime = {
    type: Date,
    optional: false
  };
  medication.lastModifiedUsername = {
    type: String,
    optional: false
  };
  medication.createdUsername = {
    type: String,
    optional: false
  };
  medication.taskOrder = {
    type: Number,
    optional: false
  };

  Schemas.Medication = new SimpleSchema(medication);
})();
