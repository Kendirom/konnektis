(function() {
  var recurringAppointment = {
    recurrenceStartDay: {
      type: Date,
      optional: false
    },
    recurrenceEndDay: {
      type: Date,
      optional: false
    },
    startTime: {
      type: String,
      optional: false
    },
    endTime: {
      type: String,
      optional: false
    },
    recurrenceUnit: {
      type: String,
      optional: false,
      allowedValues: ['day', 'week'],
      autoform: {
        options: [
          { label: 'Days', value: 'day' },
          { label: 'Weeks', value: 'week' }
        ]
      }
    },
    recurrenceAmount: {
      type: Number,
      min: 0,
      optional: false
    },
    name: {
      type: String,
      optional: false,
      label: "Appointment Type",
      allowedValues: ["agency", "volunteer", "family", "home", "outside"],
      autoform: {
        options: [
          { label: "Agency Care Visit", value: "agency" },
          { label: "Volunteer Care Visit", value: "volunteer" },
          { label: "Family Care Visit", value: "family" },
          { label: "Home Appointment", value: "home" },
          { label: "Outside Appointment", value: "outside" },
        ]
      }
    },
    description: {
      type: String,
      optional: true
    },
    carerUsername: {
      type: String,
      optional: false
    }
  };

  Schemas.RecurringAppointmentForm = new SimpleSchema(recurringAppointment);

  recurringAppointment._id = {
    type: String,
    optional: true,
  };
  recurringAppointment.patientId = {
    type: String,
    optional: false,
  };
  recurringAppointment.lastModifiedTime = {
    type: Date,
    optional: false
  };
  recurringAppointment.createdTime = {
    type: Date,
    optional: false
  };
  recurringAppointment.lastModifiedUsername = {
    type: String,
    optional: false
  };
  recurringAppointment.createdUsername = {
    type: String,
    optional: false
  };

  Schemas.RecurringAppointment = new SimpleSchema(recurringAppointment);
})();
