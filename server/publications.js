function getPatientIdsUserIsAuthorisedToRead(user) {
  var roles = user.roles;

  if (!roles) {
    return [];
  }

  var patientIds = [];
  for (var i = 0; i < roles.length; i++) {
    var split = roles[i].split(':');
    var role = split[0];
    var patientId = split[1];
    if (role === 'family'
      || role === 'carer') {
        patientIds.push(patientId);
      }
  }

  return patientIds;
}

function getPatientIdForDevice(deviceId) {
  var patientDevice = PatientDevices.findOne({ _id: deviceId });
  if(patientDevice) {
    return patientDevice.patientId;
  }
  return false;
}

Meteor.publish("carers", function() {
  var carers = Meteor.users.find({ "roles": { $elemMatch: { $regex: /^(carer|family)\:/ } } }).fetch();
  var self = this;

  _.each(carers, function(value) {
    var patients = [];
    var roles = parseRoles(value.roles);
    for (var i = 0; i < roles.length; i++) {
      if (roles[i].role === 'carer') {
        patients.push(roles[i].patientId);
      }
    }

    var entity = { _id: value._id, username: value.username, patients: patients };

    self.added("carers", entity._id, entity);
  });

  return [];
});

Meteor.publish("patients", function() {
  if (this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return Patients.find();
      foundItems = Patients.find().fetch();
    }

    else {
      var user = Meteor.users.findOne(this.userId);

      var roles = parseRoles(user.roles);

      var patientIds = roles.map(function(value) {
        return value.patientId;
      })

      return Patients.find({ _id: { $in: patientIds } });
    }
  }

  return [];
});

Meteor.publish("medications", function(viewingPatientId, date, appointmentId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return Medications.find();
      foundItems = Medications.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        // return Medications.find({ patientId: { $in: patientIds } });
        var qry = { patientId: viewingPatientId };

        if(appointmentId) {
          qry.appointmentId = appointmentId;
        }
        else if(date) {
          qry.scheduledStartDay = date;
        }

        return Medications.find(qry);
      }
    }
  }

  return [];
});

Meteor.publish("tasks", function(viewingPatientId, date, appointmentId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return Tasks.find();
      foundItems = Tasks.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        // return Tasks.find({ patientId: { $in: patientIds } });
        var qry = { patientId: viewingPatientId };

        if(appointmentId) {
          qry.appointmentId = appointmentId;
        }
        else if(date) {
          qry.scheduledStartDay = date;
        }

        return Tasks.find(qry);
      }
    }
  }

  return [];
});

Meteor.publish("appointments", function(viewingPatientId, date, appointmentId, dateFrom, dateTo) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return Appointments.find();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        // return Appointments.find({ patientId: { $in: patientIds } });
        var qry = [{ patientId: viewingPatientId }];

        if(appointmentId) {
          qry.push({_id: appointmentId});
        }
        else if(dateFrom && dateTo) {
          qry.push({scheduledStartDay: {$gte: dateFrom}});
          qry.push({scheduledStartDay: {$lte: dateTo}});
        }
        else if(date) {
          qry.push({scheduledStartDay: date});
        }

        // return Appointments.find({$and: qry});
        var self = this;

        var appendCarer = function(appointment) {
          if(appointment.carerUsername) {
            var carer = Meteor.users.findOne({username: appointment.carerUsername});
            if(carer) {
              appointment.carerProfile = carer.profile;
            }
          }
          return appointment;
        };

        var subHandle = Appointments.find({$and: qry}).observeChanges({
          added: function (id, fields) {
            self.added("appointments", id, appendCarer(fields));
          },
          changed: function(id, fields) {
            self.changed("appointments", id, appendCarer(fields));
          },
          removed: function (id) {
            self.removed("appointments", id);
          }
        });

        self.ready();

        self.onStop(function () {
          subHandle.stop();
        });
      }
    }
  }

  // we are not admin, no patientID has been set, or the viewing patientID is not valid for this user, so return nothing
  return [];
});

Meteor.publish("appointmentSummaries", function(deviceId, date) {
  if(deviceId) {
    var patientId = getPatientIdForDevice(deviceId);

    if(patientId) {
      // return Appointments.find({ patientId: patientId, scheduledStartDay: date });

      var self = this;

      _.each(Appointments.find({ patientId: patientId, scheduledStartDay: date }).fetch(), function(appointment) {
        if(appointment.carerUsername) {
          var carer = Meteor.users.findOne({username: appointment.carerUsername});
          if(carer) {
            appointment.carerProfile = carer.profile;
          }
        }
        self.added("appointments", appointment._id, appointment);
      });
    }
  }

  return [];
});

Meteor.publish("recurringAppointmentSummaries", function(deviceId) {
  if(deviceId) {
    var patientId = getPatientIdForDevice(deviceId);

    if(patientId) {
      // return RecurringAppointments.find({ patientId: patientId });

      var self = this;

      _.each(RecurringAppointments.find({ patientId: patientId }).fetch(), function(appointment) {
        if(appointment.carerUsername) {
          var carer = Meteor.users.findOne({username: appointment.carerUsername});
          if(carer) {
            appointment.carerProfile = carer.profile;
          }
        }
        self.added("recurringAppointments", appointment._id, appointment);
      });
    }
  }

  return [];
});

Meteor.publish("recurringAppointments", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return RecurringAppointments.find();
      foundItems = RecurringAppointments.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        // return RecurringAppointments.find({ patientId: viewingPatientId });
        var self = this;

        var appendCarer = function(appointment) {
          if(appointment.carerUsername) {
            var carer = Meteor.users.findOne({username: appointment.carerUsername});
            if(carer) {
              appointment.carerProfile = carer.profile;
            }
          }
          return appointment;
        };

        var subHandle = RecurringAppointments.find({ patientId: viewingPatientId }).observeChanges({
          added: function (id, fields) {
            self.added("recurringAppointments", id, appendCarer(fields));
          },
          changed: function(id, fields) {
            self.changed("recurringAppointments", id, appendCarer(fields));
          },
          removed: function (id) {
            self.removed("recurringAppointments", id);
          }
        });

        self.ready();

        self.onStop(function () {
          subHandle.stop();
        });
      }
    }
  }

  // we are not admin, no patientID has been set, or the viewing patientID is not valid for this user, so return nothing
  return [];
});

// Meteor.publish("recurringAppointments", function(viewingPatientId) {
//   if(this.userId) {
//     if (Roles.userIsInRole(this.userId, ['admin'])) {
//       return RecurringAppointments.find();
//       foundItems = RecurringAppointments.find().fetch();
//     }

//     if(viewingPatientId) {
//       var user = Meteor.users.findOne(this.userId);
//       var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

//       if(patientIds.indexOf(viewingPatientId) >= 0) {
//         // return RecurringAppointments.find({ patientId: viewingPatientId });
//         var self = this;
//         _.each(RecurringAppointments.find({ patientId: viewingPatientId }, {reactive: true}).fetch(), function(appointment) {
//           if(appointment.carerUsername) {
//             var carer = Meteor.users.findOne({username: appointment.carerUsername});
//             if(carer) {
//               appointment.carerProfile = carer.profile;
//             }
//           }
// console.log('publish.recurringAppointments found id: ' + appointment._id);
//           self.added("recurringAppointments", appointment._id, appointment);
//         });
//       }
//     }
//   }

//   return [];
// });

Meteor.publish("recurringMedications", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return RecurringMedications.find();
      foundItems = RecurringMedications.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return RecurringMedications.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("recurringTasks", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return RecurringTasks.find();
      foundItems = RecurringTasks.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return RecurringTasks.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("comments", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return Comments.find();
      foundItems = Comments.find().fetch();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return Comments.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("carePlans", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return CarePlans.find();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return CarePlans.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("riskAssessments", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return RiskAssessments.find();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return RiskAssessments.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("patientInfo", function(viewingPatientId) {
  if(this.userId) {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
      return PatientInfo.find();
    }

    if(viewingPatientId) {
      var user = Meteor.users.findOne(this.userId);
      var patientIds = getPatientIdsUserIsAuthorisedToRead(user);

      if(patientIds.indexOf(viewingPatientId) >= 0) {
        return PatientInfo.find({ patientId: viewingPatientId });
      }
    }
  }

  return [];
});

Meteor.publish("carerPatientLastLogins", function(viewingPatientId) {
  if(this.userId) {
    if(viewingPatientId) {
      return CarerPatientLastLogins.find({
        carerId: this.userId,
        patientId: viewingPatientId
      }, {lastLogin: 1, _id:0})
    }
  }

  return [];
});

Meteor.publish("medicationTakenOptions", function() {
  return MedicationTakenOptions.find();
});

Meteor.publish("medicationLocationOptions", function() {
  return MedicationLocationOptions.find();
});

Meteor.publish("carePlanSections", function() {
  return CarePlanSections.find();
});
