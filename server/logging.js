Meteor.startup(function() {
  var options = {
    "level": "info",
    "tags": "konnektis-server",
    "subdomain": "konnektis.loggly.com",
    "inputToken":"e7a96cb4-9a0c-4361-b705-ba9c553aa29d",
    "json": true,
    "handleExceptions": true
  };

  Winston.add(Winston_Loggly, options);
});
