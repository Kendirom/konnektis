Accounts.onCreateUser(function(options, user) {
  // Security in depth
  throw new Meteor.Error('User creation is forbidden');
});
