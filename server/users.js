Meteor.methods({
    getCarersAndFamilyForPatient: function(patientId) {
        var carersAndFamily = Meteor.users.find({
            roles: { $in: [
                'carer:' + patientId,
                'family:' + patientId
                ]}
        }, { fields: {'_id': true} }).fetch();
console.log('carers and family');
console.log(carersAndFamily);
        var carerAndFamilyArr = [];

        _.each(carersAndFamily, function(carerOrFamilyMember) {
            console.log('adding carer or family: ' + carerOrFamilyMember['_id']);
            carerAndFamilyArr.push(carerOrFamilyMember['_id']);
        });
console.log('carers and family array');
console.log(carerAndFamilyArr);

console.log('unique carers and family');
console.log(_.uniq(carerAndFamilyArr));

        return _.uniq(carerAndFamilyArr);
    }
});
