Meteor.startup(function() {
  var db = Meteor.users.rawDatabase();
  var opening = false;

  db.on('error', function(err, d) {
    if (err) {
      console.log('error connecting to the database: ' + err);
      if (!opening) {
        try {
          if (d._reconnectInProgreess === false) {
            console.log('closing database connection');
            db.close();
          }
        } catch (e) {
          console.log('error closing database connection: ' + e);
        }
      }
    }
  });

  db.on('timeout', function() {
    console.log('database connection timeout');
  });

  db.on('close', function(d, a) {
    console.log('database connection closed');
    if (!opening) {
      opening = true;

      setTimeout(function() {
        console.log('re-opening database connection');

        try {
          db.open();
          console.log('database connection successfully reopened');
        } catch (e) {
          console.log('error re-opening database connection: ' + e);
        }

        opening = false;
      }, 1000);
    }
  });

  Meteor.setInterval(function() {
    try {
      Meteor.users.findOne();
    } catch (e) {
      console.log('error periodically checking the database: ' + e);
    }
  }, 20000);
});
