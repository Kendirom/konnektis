/*
  server/env.js
  Environment stuff
*/

Meteor.methods({
  getTimerSettings: function(){
    var inactivity_timeout = parseInt(Meteor.settings.public.INACTIVITY_TIMEOUT || process.env.INACTIVITY_TIMEOUT);
    var pre_inactivity_timeout = parseInt(Meteor.settings.public.PRE_INACTIVITY_TIMEOUT || process.env.PRE_INACTIVITY_TIMEOUT);
    var timeouts = {inactivity_timeout: inactivity_timeout, pre_inactivity_timeout: pre_inactivity_timeout}
    return timeouts;
  }
});
