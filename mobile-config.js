App.info({
  name: 'Konnektis',
  description: 'Konnektis facilitates advanced communication with domiciliary carers',
  version: '1.0.23'
});

App.icons({
  'android_mdpi': 'icons/konnektis-logo-mdpi.png',
  'android_hdpi': 'icons/konnektis-logo-hdpi.png',
  'android_xhdpi': 'icons/konnektis-logo-xhdpi.png'
});

App.launchScreens({
  'android_xhdpi_portrait': 'splash/konnektis-splash-720x1280.png',
  'android_xhdpi_landscape': 'splash/konnektis-splash-1280x720.png'
});

// source for analytics.js
App.accessRule('http://www.google-analytics.com/*',{external:false});
App.accessRule('http://cdn.mxpnl.com/*',{external:false});

// destination for data
App.accessRule('https://stats.g.doubleclick.net',{external:false});
App.accessRule('http://api.mixpanel.com/*',{external:false});
App.accessRule('https://enginex.kadira.io/*', {eternal:false});

// logs
App.accessRule('http://cloudfront.loggly.com/*',{external:false});
