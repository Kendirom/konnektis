Tasks = new Mongo.Collection("tasks");
Tasks.attachSchema(Schemas.Task);
RecurringTasks = new Mongo.Collection("recurringTasks");
RecurringTasks.attachSchema(Schemas.RecurringTask);
Medications = new Mongo.Collection("medications");
Medications.attachSchema(Schemas.Medication);
RecurringMedications = new Mongo.Collection("recurringMedications");
RecurringMedications.attachSchema(Schemas.RecurringMedication);
Appointments = new Mongo.Collection("appointments");
Appointments.attachSchema(Schemas.Appointment);
RecurringAppointments = new Mongo.Collection("recurringAppointments");
RecurringAppointments.attachSchema(Schemas.RecurringAppointment);
Comments = new Mongo.Collection("comments");
Comments.attachSchema(Schemas.Comment);
Patients = new Mongo.Collection("patients");
Carers = new Mongo.Collection("carers");
NotificationHistory = new Mongo.Collection("notificationHistory");

PatientDevices = new Mongo.Collection("patientDevices");
PatientDevicesLog = new Mongo.Collection("patientDevicesLog");

MedicationTakenOptions = new Mongo.Collection("medicationTakenOptions");
MedicationLocationOptions = new Mongo.Collection("medicationLocationOptions");
CarePlanSections = new Mongo.Collection("carePlanSections");

CarePlans = new Mongo.Collection("carePlans");
CarePlans.attachSchema(Schemas.CarePlan);
CarePlansLog = new Mongo.Collection("carePlansLog");

RiskAssessments = new Mongo.Collection("riskAssessments");
RiskAssessmentsLog = new Mongo.Collection("riskAssessmentsLog");

PatientInfo = new Mongo.Collection("patientInfo");
PatientInfoLog = new Mongo.Collection("patientInfoLog");

CarerPatientLastLogins = new Mongo.Collection("carerPatientLastLogins");


if (Meteor.isClient) {
    // define fields to be encrypted
    var encryptedCarePlanFields = ['carePlan', 'medication', 'supportPlan', 'carePlanChanges'];
    // init encryption on collection CarePlans
    CarePlansEncryption = new CollectionEncryption(CarePlans, encryptedCarePlanFields, {
        onFinishedDocEncryption: function (doc) {
            var patientId = doc.patientId;

            Meteor.call('getCarersAndFamilyForPatient', patientId, function(error, carerAndFamilyArr) {
                if(error) {
                    bootbox.alert('There was a problem allocating carers and family memebers with access to this care plan..');
                    return;
                }

                var subscribeUser = function(userId) {
console.log('subscribing: ' + userId);
                    if(userId !== Meteor.userId()) {
console.log('user is not the currently logged in user');
console.log('doc ID is: ' + doc._id);
                        Meteor.subscribe('principals', userId, function () {
                            CarePlansEncryption.shareDocWithUser(doc._id, userId);
                        });
                    }
                };

                _.each(carerAndFamilyArr, subscribeUser);
            });
        }
    });
}


if (Meteor.isServer) {
    // allow client-side updating/inserting of care plans - to support end-to-end encryption

    CarePlans.permit(['insert']).ifLoggedIn().apply();
    CarePlans.permit(['update']).ifLoggedIn().apply();
}
