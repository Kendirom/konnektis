Meteor.startup(function() {
  if (Meteor.isCordova) {
    Push.addListener('error', function(err) {
      console.error(err);
    });
    // Seems to be firing the event twice; workaround to avoid displaying messages twice
    var receivedMessages = [];
    Push.addListener('message', function(notification) {
      if (receivedMessages.indexOf(notification.payload.historyId) !== -1) {
        return;
      }

      receivedMessages.push(notification.payload.historyId);
      navigator.notification.alert(notification.message, function() { }, notification.payload.title, "Ok");
    });
  }

  if (Meteor.isServer) {
    if (Meteor.settings && Meteor.settings.twilio) {
      Meteor.twilioClient = Twilio(Meteor.settings.twilio.accountSid, Meteor.settings.twilio.authToken);
    }
  }

  if (Meteor.isCordova && Meteor.settings) {
    // navigator.analytics.setTrackingId(
    //   Meteor.settings.public.googleAnalyticsMobileSettings.trackingId);

    Deps.autorun(function(){
      if(!Meteor.userId()) {
        var loggedIn = SessionAmplify.get('loggedIn', true);
        if (loggedIn === true) {
          //navigator.analytics.sendEvent('userActivity', 'Signed out');
        }
      }
    });

    Package['iron:router'].Router.onRun(function() {
      var router = this;
      Tracker.afterFlush(function () {
        //navigator.analytics.sendAppView(router.route.getName());
      });
      this.next();
    });
  }

  if (Meteor.isClient) {
    Status.setTemplate('bootstrap3', {classes: 'offlinebar login'});
  }
  if(Meteor.isServer){
    Accounts.urls.resetPassword = function(token) {
      return Meteor.absoluteUrl('reset-password/' + token);
    };
  }
});
