# Konnektis Readme

## Local Debugging

To test all features, the settings.test.json file should be used when running meteor e.g.

```
meteor run --settings settings.test.json
meteor run android --settings settings.test.json
meteor run android-device --settings settings.test.json
```

The production settings are held in Modulus, as explained below. These settings are loaded by Meteor into the Meteor.settings property.

http://joshowens.me/environment-settings-and-security-with-meteor-js/

## Server app

The settings (including android app settings) are all environment variables on the app server(s). These can be entered into either the modulus web site or the cli. These include:

- NODE_ENV
- MONGO_URL
- ROOT_URL
- METEOR_SETTINGS

### Deployment Instructions

- Install modulus globally via NPM
- Login to modulus on the cli
- Run 'modulus deploy'

https://modulus.desk.com/customer/portal/articles/1701977-modulus-command-line

https://modulus.desk.com/customer/portal/articles/1647770-using-meteor-with-modulus?b_id=9670

## Android App

### Deployment Instructions

- Change the version number in ./mobile-config.js
- Import the signing key into the key store
- Run
```
rm -r .app-bundle/
meteor build .app-bundle/ --server https://app.konnektis.com
cd .app-bundle/android/
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 release-unsigned.apk konnektis-app
```
- Enter keystore password
- Enter app signing key password
- Replace the version number to match that in ./mobile-config.js and run
```
~/.meteor/android_bundle/android-sdk/build-tools/20.0.0/zipalign 4 \
release-unsigned.apk konnektis-app-production-v1.0.13.apk
```

### Install Instructions

- On the device allow installation of apps from sources other than the Play Store. Settings > Security.
- Plug device into PC
- Create a folder in the root of the device 'konnektis'
- Copy the new apk file into the folder
- On the device start a new Chrome tab and navigate to 'file:///sdcard/konnektis/' (yes 3 slashes)
- Click the new apk file and it should download
- Open the downloaded apk file
- Install

https://github.com/meteor/meteor/wiki/How-to-submit-your-Android-app-to-Play-Store
